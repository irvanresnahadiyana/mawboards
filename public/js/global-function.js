function putError(data,idDiv) {
    $.each(data,function(key, val){
        $(idDiv+key).addClass('has-error');
        $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
    });

    var errorDiv = $('.has-error:visible').first();
    var scrollPos = errorDiv.offset().top;
    $('html, body').animate({ scrollTop: errorDiv.offset().top }, 'slow');

}

function postAjax(type,url,formData,btn,divError,redirect,formId) {
    $('.form-group').removeClass('has-error');
    $('.help-block').remove();
    $.ajax({
        type:'post',
        url:url,
        data: formData,
        dataType: 'json',
        cache:false,
        contentType: false,
        processData: false,
        success:function(data) {
            generateNotif('Information', data.message, data.result);
            if (data.result == 'success') {
                document.getElementById(formId).reset();
                if (redirect) {
                    setTimeout(function(){
                        window.location.href = data.redirect;
                    },1000);
                }
            }
            btn.button('reset');
        },
        error:function(data) {
            var data = data.responseJSON;
            if (data.result === 'Error') {
                generateNotif('Error', data.msgTrans, 'error');
                $('.form-group').addClass('has-error');
            } else {   
                putError(data,divError);
            }

            btn.button('reset');
        }
    });
}

function postComment(url,formData,btn,divError,formId,elementAppend) {
    $('.form-group').removeClass('has-error');
    $('.help-block').remove();
    $.ajax({
        type:'post',
        url:url,
        data: formData,
        dataType: 'json',
        success:function(data) {
            generateNotif('Information', data.message, data.result);
            if (data.result == 'success') {
                document.getElementById(formId).reset();
                elementAppend.html(data.data);
                if (formId == 'form-add-comment') {
                    $('html, body').animate({
                        scrollTop: $("#box-top-comment").offset().top
                    }, 500);
                } else {
                    var div = $('.remodal-wrapper');
                    div.animate({
                        scrollTop: $(".comments-modal").offset().top - div.offset().top + div.scrollTop()
                    }, 500);
                }
            }
            btn.button('reset');
        },
        error:function(data) {
            var data = data.responseJSON;
            if (data.result === 'Error') {
                generateNotif('Error', data.msgTrans, 'error');
                $('.form-group').addClass('has-error');
            } else {   
                putError(data,divError);
            }

            btn.button('reset');
        }
    });
}

function wishLikeBookmark(id,type,group) {
    var url = (type == 'like' ? like_url : bookmark_url);
    $.ajax({
        url: url,
        method: "post",
        dataType: "json",
        data: {"id":id,"group":group},
        success: function(result) {
            console.log(result);
            if (result.result == 'Success') {
                if (result.data.status == 'like') {
                    $(result.data.wishID).addClass('liked');
                } else {
                    $(result.data.wishID).removeClass('liked');
                }

                $('.people-likes-'+id).text(result.data.people_like.length);
            }
        },
        error: function(result) {
            var data = result.responseJSON;
            generateNotif('Error',data.msgTrans,'error');
        }
    });
}

function loadFeed(pageNumber) {
    $('div#inifiniteLoader').show();
    $.ajax({
        url: base_url,
        type: 'GET',
        data: {page: pageNumber,callback:'json'},
        dataType: 'json',
        before: function() {
            finish = true;
        },
        success: function (data) {
            finish = data.finish;
            $('div#inifiniteLoader').hide();
            if (!finish) {
                $(data.products).insertAfter($(".ajax_scroll .is-product").last());
                $(data.wishes).insertAfter($(".ajax_scroll .is-wish").last());
                setTimeout(function(){
                    $grid.masonry( 'reloadItems');
                    $grid.masonry('layout');
                },100);
            }
        }
    });
    return false;
}

function confirmDelete(ajax_url_delete, success_url, data, name = null) {
    swal({
        title: "Are you sure ?",
        text: "Delete "+name,
        html: true,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        $.ajax({
            type: 'post',
            url: ajax_url_delete,
            data: data,
            dataType: 'json',
            success: function(data) {
                window.location = success_url;
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
}