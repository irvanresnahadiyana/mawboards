
//function error message
function putError(data,idDiv) {
    $.each(data,function(key, val){
        $(idDiv+key).addClass('has-error');
        $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
    });

    var errorDiv = $('.has-error:visible').first();
    var scrollPos = errorDiv.offset().top;
    $('html, body').animate({ scrollTop: errorDiv.offset().top }, 'slow');

}

//function store with ajax
function postWithAjax(type,url,formData,btn,divError) {
    if (type != 'delete') {
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:url,
            data: formData,
            dataType: 'json',
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                generateNotif('topCenter', data.result, '<i class="fa fa-'+data.result+'" aria-hidden="true"></i> '+data.message);
                if (data.result == 'success') {
                    setTimeout(function(){
                        window.location.href = data.redirect;
                    },1000);
                } else {
                    btn.button('reset');
                }
            },
            error:function(data) {
                var data = data.responseJSON;
                if (data.result === 'Error') {
                    generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                    $('.form-group').addClass('has-error');
                } else {   
                    putError(data,divError);
                }

                btn.button('reset');
            }
        });
    } else {
        var result = false;
        $.ajax({
            type:'post',
            url:url,
            data: formData,
            dataType: 'json',
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                generateNotif('topCenter', data.result, '<i class="fa fa-'+data.result+'" aria-hidden="true"></i> '+data.message);
                btn.button('reset');
                resultAjaxDelete(data);
                return data;
            },
            error:function(data) {
                var data = data.responseJSON;
                if (data.result === 'Error') {
                    generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                    $('.form-group').addClass('has-error');
                } else {   
                    putError(data,divError);
                }

                btn.button('reset');

                resultAjaxDelete(data);
                return data;
            }
        });
    }

}