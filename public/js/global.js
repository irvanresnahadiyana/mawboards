var pageComment = 2;
$(document).on('click','.wish-like-bookmark',function(){
    var wishId = $(this).data('id');
    var type = $(this) .data('type');
    var group = $(this) .data('group');

    if (status_user) {
        wishLikeBookmark(wishId,type,group);
    } else {
        generateNotif('Information',msgInfoLoginReg,'warning');
    }
});

$(document).on('click','.show-wish',function(e){
    e.preventDefault();
    var wishSlug = $(this).data('wishslug');
    var typeMaw = $(this).data('maw');
    if (typeMaw == 'product') {
        inst.close();
    } else {
        inst.open();
        inst.getState();
        history.pushState(null, null, '/maw/'+wishSlug);
        $.ajax({
            method:'get',
            url:urlDetailWish+'/'+wishSlug+'?callback=json&type='+typeMaw+'&for=detail_modal',
            dataType:'json',
            success:function(data) {
                if (data.result = 'success') {
                    $('#modal-wish-detail .content-detail').html(data.content_detail);
                    $('#modal-wish-detail .content-maw').html(data.content_maw);
                    $('#modal-wish-detail .content-related-maw').html(data.content_related);

                    var modalGridMaw = $('.content-related-maw .modal-grid').masonry({
                        itemSelector: '.grid-item',
                        columnWidth: '.grid-sizer',
                        percentPosition: true,
                        fitWidth: true
                    });

                    var $modalWhisboard = $('.modal-wishboard-masonry').masonry({
                        itemSelector: '.modal-wishboard-item',
                        columnWidth: '.modal-wishboard-sizer',
                        percentPosition: true,
                        fitWidth: true
                    });

                    setTimeout(function(){
                        
                        $modalWhisboard.masonry('reloadItems');
                        $modalWhisboard.masonry('layout');
                    },1000);
                }
            },
            error:function(data) {

            }
        });
    }
});

$(document).on('click','#submit-comment-modal',function(){
    if (status_user) {
        var btn = $(this).button('loading');
        var formData = $('#form-add-comment-modal').serialize();
        postComment(urlPostComment,formData,btn,"#wish-modal-",'form-add-comment-modal',$('.comments-modal'));
    } else {
        generateNotif('Information',msgInfoLoginReg,'warning');
    }
});

$(document).on('click','#modal-see-more',function(){
    var slug = $(this).data('wishslug');
    $.ajax({
        url:urlDetailWish+'/'+slug+'?comment_pagination=true&callback=json&page='+pageComment,
        method:'get',
        dataType:'json',
        success:function(data) {
            if (data.result == 'success') {
                if (!data.finish) {
                    $(data.data).insertBefore('#modal-see-more');
                } else {
                    $('#modal-see-more').hide();
                }
            } else {
                generateNotif('Information','Something went wrong','error');
            }
        }
    });
    pageComment++
});