<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductCategoriesChildsAndAddFieldIsParentAtTableProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories_child', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });

        Schema::table('product_categories', function (Blueprint $table) {
            $table->boolean('is_parent')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories_child');

        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropColumn(['is_parent']);
        });
    }
}
