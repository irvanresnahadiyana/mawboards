<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFieldProductIdAtTableProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropColumn(['product_id']);
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['wholesales_price_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->integer('product_id')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('wholesales_price_id')->nullable();
        });
    }
}
