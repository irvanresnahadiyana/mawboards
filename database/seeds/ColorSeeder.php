<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->truncate();

        $dateNow = date('Y-m-d H:i:s');

        $data = [
            [
                'name' => 'Pink',
                'hex' => '#ff1493',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Yellow',
                'hex' => '#ffff00',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Orange',
                'hex' => '#ff4500',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Lime Green',
                'hex' => '#32cd32',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Cyan',
                'hex' => '#00ffff',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Grey',
                'hex' => '#808080',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
        ];

        DB::table('colors')->insert($data);
    }
}
