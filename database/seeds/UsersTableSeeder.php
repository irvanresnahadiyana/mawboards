<?php

use Illuminate\Database\Seeder;
use App\Models\UserMerchant;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();

    	  $default_password = '12345678';


        // ROLE
        $admin = [
    			'name' => 'Administrator',
    			'slug' => 'administrator',
    		];
    		$adminRole = Sentinel::getRoleRepository()->createModel()->fill($admin)->save();

    		$merchant = [
    			'name' => 'Merchant',
    			'slug' => 'merchant',
    		];
    		$merchantRole = Sentinel::getRoleRepository()->createModel()->fill($merchant)->save();


        $user = [
    			'name' => 'User',
    			'slug' => 'user',
    		];
    		$userRole = Sentinel::getRoleRepository()->createModel()->fill($user)->save();


        // ROLE

    		$admin = [
    			'email'    => 'admin@maw.com',
    			'password' => $default_password,
                'first_name' => 'Super',
                'last_name' => 'Admin',
    		];

        $merchant = [
    			'email'    => 'merchant@maw.com',
    			'password' => $default_password,
                'first_name' => 'User',
                'last_name' => 'Merchant',
    		];

    		$users = [
    			[
    				'email'    => 'demo1@maw.com',
    				'password' => $default_password,
                    'first_name' => 'User',
                    'last_name' => 'Demo1',
    			],
    			[
    				'email'    => 'demo2@maw.com',
    				'password' => $default_password,
                    'first_name' => 'User',
                    'last_name' => 'Demo3',
    			],
    			[
    				'email'    => 'demo3@maw.com',
    				'password' => $default_password,
                    'first_name' => 'User',
                    'last_name' => 'Demo3',
    			],
    		];


        // REGISTER

    		$adminUser = Sentinel::registerAndActivate($admin);
        $merchantUser = Sentinel::registerAndActivate($merchant);

        // put merchant at table user_merchants
        UserMerchant::create([
                'user_id' => $merchantUser->id,
                'store_name' => 'Merchant Store',
                'store_address' => 'Merchant Store Address',
                'phone_number' => '1234567890',
                'country' => 'Indonesia',
                'province' => 'Jawa Barat',
                'city' => 'Bandung',
                'postal_code' => '1234'
            ]);

        $roleAdmin = Sentinel::findRoleByName('Administrator');
        $roleAdmin->users()->attach($adminUser);

        $roleMerchant = Sentinel::findRoleByName('Merchant');
        $roleMerchant->users()->attach($merchantUser);

    		foreach ($users as $user)
    		{
    			$userRegister = Sentinel::registerAndActivate($user);
          $roleUser = Sentinel::findRoleByName('User');
          $roleUser->users()->attach($userRegister);
    		}
    }
}
