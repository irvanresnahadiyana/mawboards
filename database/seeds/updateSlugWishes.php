<?php

use Illuminate\Database\Seeder;
use App\Models\Wish;

class updateSlugWishes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wishes = Wish::all();
        if (count($wishes) > 0) {
            foreach ($wishes as $key => $value) {
                $value->update(['title' => $value->title]);
            }
        }
    }
}
