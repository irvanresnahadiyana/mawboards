<?php

use Illuminate\Database\Seeder;

class WishCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wish_categories')->truncate();

        $dateNow = date('Y-m-d H:i:s');

        $data = [
            [
                'name' => 'Valentine',
                'color_id' => 1,
                'slug' => 'valentine',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'New Year / Christmas',
                'color_id' => 2,
                'slug' => 'new-year-xmas',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Holiday',
                'color_id' => 3,
                'slug' => 'holiday',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Anniversary',
                'color_id' => 4,
                'slug' => 'anniversary',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Birthday',
                'color_id' => 5,
                'slug' => 'birthday',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
            [
                'name' => 'Custom',
                'color_id' => 6,
                'slug' => 'custom',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ],
        ];

        DB::table('wish_categories')->insert($data);
    }
}
