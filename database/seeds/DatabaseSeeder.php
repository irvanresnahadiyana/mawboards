<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StaticPagesSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(WishCategorySeeder::class);
    }
}
