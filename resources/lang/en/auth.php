<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Login',
    'register' => 'Register',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'activation' => 'Account activation has been sent to your email.',
    'reactivation' => 'Error activation, Account activation has been sent again to your email.',
    'activate' => 'Activate your account',
    'reactivate' => 'Reactivate your account',
    'email_not_registered' => 'Email not registered',
    'user_already_activated' => 'User already activated',
    'user_has_been_activated' => 'User has been activated',
    'not_activated' => 'Account not activated!',
    'logged' => 'You have a logged!',
    'please_login_first' => 'Please login first',
    'reset_password' => 'Reset Password',
    'forgot_password' => 'Forgot password',
    'change_password' => 'Change Password',
    'already_have_account' => 'Already have an account',
    'login_here' => 'Login here',
    'as_wisher' => 'As Wisher',
    'as_merchant' => 'As Merchant',

];
