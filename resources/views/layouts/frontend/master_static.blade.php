<!DOCTYPE html>
<html lang="en">
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta('robots', 'noindex, nofollow') !!}
        {!! Html::meta('author', env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('product', env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('title', isset($meta['title']) ? $meta['title'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('description', isset($meta['description']) ? $meta['description'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('keywords', isset($meta['keywords']) ? $meta['keywords'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1') !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        <title>@yield('title') | {{ env('APP_NAME', 'Mawboards') }}</title>
        <!-- MAIN Style -->
        {!! Html::style('frontend/assets/css/styles.css') !!}
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="icon" type="image/png" href="{{ asset('frontend/assets/images/logo.png') }}" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ asset('frontend/assets/images/logo.png') }}" sizes="16x16" />
        @yield('header')
    </head>
    <body class="about">
        <header class="about-header">
            <div class="container">
                <section class="about-box">
                    <div class="about-logo">
                        <a href="#"><img src="{{ asset('frontend/assets/images/logo.png') }}" alt=""></a>
                    </div>
                    <div class="about-nav">
                        <ul>
                            <li class="active"><a href="#">What is Maw?</a></li>
                            <li><a href="#">Discover</a></li>
                            <li><a href="#">Sign up!</a></li>
                            <li><a href="#">Term</a></li>
                            <li><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </section>
            </div>
        </header>
         <section class="about-content" style="background-image: url('https://images.unsplash.com/photo-1483383490964-8335c18b6666?dpr=1&auto=format&fit=crop&w=1500&h=1125&q=80&cs=tinysrgb&crop=&bg=');">
            <div class="container">
                <section class="col-md-5">
                    @yield('content')
                </section>
            </div>
        </section>
    </body>
</html>
