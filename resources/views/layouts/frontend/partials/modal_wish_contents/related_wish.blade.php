<div class="row">
    <div class="modal-grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        @if(count($wish_related) > 0)
            @include('contents.frontend.wish_list', ['result' => $wish_related])
        @endif
    </div>
</div>