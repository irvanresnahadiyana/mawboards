<div class="modal-box">
    <section class="modal-wishboard">
        <div class="media">
            <section class="media-left media-middle">
                <a href="{{ route('user-wishboard-frontend', $wish_info['user']) }}">
                    <img class="media-object grid-img-file" src="{{$wish_info['user_avatar']}}" id="modal-wish-user-avatar">
                </a>
            </section>
            <section class="media-body media-middle">
                <h4 class="media-heading"><a href="{{ route('user-wishboard-frontend', $wish_info['user']) }}" id="modal-wish-user">{{$wish_info['user_name']}}</a></h4>
                <h4 class="media-heading" id="modal-wish-user-category">{{$wish_info['category']->name}} Wishboard</h4>
            </section>
        </div>
        <div class="modal-wishboard-masonry" id="modal-scrollBar">
            <section class="modal-wishboard-sizer col-xs-4"></section>
            @if(count($wish_info['data']) > 0)
                @foreach($wish_info['data'] as $wish)
                    <section class="modal-wishboard-item col-xs-4">
                        <a href="{{route('detail-wish-frontend',['slug' => $wish->slug])}}" class="show-wish" title="{{$wish->title}}" 
                            data-wishid="{{ $wish->id }}"
                            data-wishslug="{{$wish->slug}}">
                            <img src="{{get_file(@$wish->images()->first()->image_standard,'thumbnail')}}" alt="">
                        </a>
                    </section>
                @endforeach
            @endif
        </div>
        <div class="modal-wishboard-btn text-right">
            <button type="button" class="btn btn-unstyle">Follow MAW Board</button>
        </div>
    </section>
</div>
<div class="row">
    <section class="col-xs-12">
        <div class="modal-box">
            <section class="modal-box-header">
                <div class="modal-box-header-title">
                    <span>Other boards</span>
                </div>
            </section>
        </div>
    </section>
    @if(count($wish_info['user_wishes']) > 0)
        @foreach($wish_info['user_wishes'] as $userWish)
            <section class="col-xs-6">
                <div class="modal-box">
                    <section class="modal-wishboard-list">
                        <div class="modal-wishboard-list-img objectfit-container-cover">
                            <!-- <a href="{{ route('show-wishboard-frontend', $userWish->wishCategory->slug) }}" title="{{ $userWish->title }}"> -->
                            <a href="#" title="{{ $userWish->title }}">
                                <img class="objectfit-img" src="{{ get_file(@$userWish->images()->first()->image_standard, 'thumbnail') }}" alt="">
                            </a>
                        </div>
                        <div class="modal-wishboard-list-title" style="background: {{ $userWish->wishCategory->color ? $userWish->wishCategory->color->hex : 'gray' }}">
                            <span>{{ $userWish->wishCategory->name }} Wishboard</span>
                        </div>
                    </section>
                </div>
            </section>
        @endforeach
    @endif
</div>