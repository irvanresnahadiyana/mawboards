<div class="modal-box">
    <section class="modal-box-header sea">
        <div class="modal-box-header-title">
            <span id="modal-wish-title">{{ $wish->title }}</span>
        </div>
        <div class="modal-box-header-badges">
            <ul>
                <li><a href="#"><i class="fa fa-gift"></i> 0</a></li>
                <li><a href="#"><i class="fa fa-heart"></i> {{ number_format(count($wish->people_likes),0,',','.') }}</a></li>
            </ul>
        </div>
    </section>
    <section class="modal-box-img">
        <a href="#">
            <img src="{{ $wish->image }}" alt="Wishboard Image" id="modal-wish-image">
        </a>
    </section>
    <section class="modal-box-header box-top-comment">
        <div class="modal-box-header-title">
            <span id="modal-wish-name-category">{{ $wish->category_name }}</span>
        </div>
        <div class="modal-box-header-badges">
            <ul>
                <li><a href="#"><i class="fa fa-gift sea-text"></i> WAW</a></li>
                <li><a href="#"><i class="fa fa-heart sea-text"></i> Share</a></li>
            </ul>
        </div>
    </section>
    <section class="modal-box-comments comments-modal">
        @if(count($wish->people_comments) > 0)
            @foreach($wish->people_comments as $comment)
                <div class="media">
                    <section class="media-left media-middle">
                        <a href="#">
                            <img class="media-object grid-img-file" src="{{ link_to_avatar(($comment->user->images()->first() ? $comment->user->images()->first()->image_standard : null),'thumbnail') }}" alt="User Avatar">
                        </a>
                    </section>
                    <section class="media-body media-middle">
                        <h4 class="media-heading">
                            <a href="#">
                                @if($comment->user->first_name == user_info('first_name') && $comment->user->last_name == user_info('last_name'))
                                    You
                                @else
                                    {{ $comment->user->first_name.' '.$comment->user->last_name }}
                                @endif
                            </a> :
                        </h4>
                        <p>{{ $comment->comment }}</p>
                    </section>
                </div>
            @endforeach
            @if($wish->total_comments > 7)
                <button type="button" name="button" class="btn btn-unstyle less-comment" id="modal-see-more" data-wishslug="{{$wish->slug}}">see more comments...</button>
            @endif
        @else
            <p>No comments...</p>
        @endif
    </section>
    @if(user_info())
        <section class="modal-box-addComment">
            <!-- <div class="media">
                <section class="media-left media-middle">
                    <a href="#">
                        <img class="media-object grid-img-file" src="{{link_to_avatar(user_info() ? @user_info()->images->first()->image_thumb : null)}}" alt="...">
                    </a>
                </section>
                <section class="media-body media-middle">
                    <h2>You</h1>
                </section>
            </div> -->
            <form id="form-add-comment-modal">
                <div class="form-group" id="wish-modal-comment">
                    <input type="hidden" name="id" value="{{$wish->id}}">
                    <label for="add-comment" class="sr-only"></label>
                    <textarea name="comment" id="comment" class="form-control" placeholder="Add a comment..."></textarea>
                </div>
                <button type="button" id="submit-comment-modal" name="add-comment-submit" class="btn btn-2">Comment</button>
            </form>
        </section>
    @endif
</div>
