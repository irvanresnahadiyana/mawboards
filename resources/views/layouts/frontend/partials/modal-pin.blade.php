<!-- START: PINNED Modal -->
<div class="remodal remodal-login" data-remodal-id="pin">
    <form>
        <div class="remodal-box remodal-split">
            <button data-remodal-action="close" class="remodal-close"></button>
            <section class="remodal-split-box remodal-split-full">
                <h3>Title image here</h3>
            </section>
            <section class="remodal-split-box remodal-pin-img">
                <div class="pin-img">
                    <img src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="" class="objectfit-img">
                </div>
            </section>
            <section class="remodal-split-box remodal-pin-board">
                <div class="form-group form-group-1">
                    <input class="form-control form-control-1" type="text" id="pin-title"/>
                    <label class="form-label" for="pin-title">
                        <span class="form-label-content">Edit title name</span>
                    </label>
                </div>
                <div class="pin-header">
                    <label>Choose your Wishboard</label>
                </div>
                <div class="pin-body">
                    <div class="radio-wishboard" data-toggle="buttons">
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label lime">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option1" autocomplete="off" checked>
                        </label>
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label sea">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option2" autocomplete="off">
                        </label>
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label orange">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option3" autocomplete="off">
                        </label>
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label gray">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option1" autocomplete="off" checked>
                        </label>
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label pink">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option2" autocomplete="off">
                        </label>
                        <label class="btn-wishboard btn">
                            <div class="objectfit-container-cover">
                                <img class="objectfit-img" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt="">
                            </div>
                            <div class="btn-wishboard-label blue">
                                <span><i class="fa fa-check-circle-o"></i>Anniversary WIshboard</span>
                            </div>
                            <input type="radio" name="options" id="option3" autocomplete="off">
                        </label>
                    </div>
                    <button type="submit" class="btn btn-3 btn-block"><i class="fa fa-thumb-tack"></i> Pin</button>
                </div>
            </section>
        </div>
    </form>
</div>
<!-- END: PINNED Modal -->
