<div data-remodal-id="modal-wish-detail" class="modal-detail remodal" id="modal-wish-detail">
    <section class="container">
        <button data-remodal-action="close" class="remodal-close"></button>
        <div class="row">
            <section class="col-md-9">
                <div class="content-detail">
                </div>
            </section>
            <section class="col-md-3">
                <div class="content-maw">
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-md-9">
                <div class="modal-box">
                    <section class="modal-box-header">
                        <div class="modal-box-header-title">
                            <span>Related Wishboard</span>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="content-related-maw">
        </div>
    </section>
</div>
