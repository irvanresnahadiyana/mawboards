<!-- Login Modal -->
<div class="remodal remodal-login" data-remodal-id="loginModal">
    <div class="remodal-box">
        <button data-remodal-action="close" class="remodal-close"></button>
        <section class="remodal-header">
            <img src="{{ asset('frontend/assets/images/logo.png') }}" alt="Maw Logo" />
            <h3>{!! trans('general.welcome') !!}</h3>
            <p>{!! trans('general.many_wish_come_true') !!}</p>
        </section>
        <section class="remodal-body">
            <form class="remodal-form" id="form-login">
                <div id="alert-info-login-modal">
                    @if (session('status'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="form-group" id="login-email">
                    <input type="email" class="form-control" name="email" id="login-input-email" placeholder="Email">
                </div>
                <div class="form-group" id="login-password">
                    <input type="password" class="form-control" name="password" id="login-input-password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-login btn-block" id="button-login" data-loading-text="Loading...">Log in</button>
                <p>{!! trans('auth.forgot_password') !!}? <a href="#" id="to-register" data-remodal-target="forgotPassModal">Get started here</a></p>
            </form>
            <div class="remodal-login-sosmed text-center">
                <div class="remodal-login-sosmed-caption">
                    {!! trans('general.or_connect_with_socmed') !!}
                </div>
                <ul>
                    <li><a href="{{ route('auth-redirect-socialite',['provider' => 'facebook']) }}"><button type="button" class="btn btn-3"><i class="maw-facebook-o"></i></button></a></li>
                    <!-- <li><a href="{{ route('auth-redirect-socialite',['provider' => 'twitter']) }}"><button type="button" class="btn btn-3"><i class="maw-twitter-o"></i></button></a></li> -->
                    <li><a href="{{ route('auth-redirect-socialite',['provider' => 'google']) }}"><button type="button" class="btn btn-3"><i class="maw-google-plus-o"></i></button></a></li>
                    <!-- <li><button type="button" class="btn btn-3"><i class="maw-instagram-o"></i></button></li> -->
                </ul>
                <div class="remodal-login-sosmed-alternative">
                    {{ trans('general.need_account') }} <a href="#" id="to-register" data-remodal-target="registerModal">Sign up</a>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- Register Modal -->
<div class="remodal remodal-login" data-remodal-id="registerModal">
    <div class="remodal-box">
        <button data-remodal-action="close" class="remodal-close"></button>
        <section class="remodal-header">
            <img src="{{ asset('frontend/assets/images/logo.png') }}" alt="Maw Logo" />
            <h3>{!! trans('general.welcome') !!}</h3>
            <p>{!! trans('general.many_wish_come_true') !!}</p>
        </section>
        <section class="remodal-body">
            <ul class="remodal-tab" role="tablist">
                <li role="presentation" class="active"><a href="#asWisher" aria-controls="asWisher" role="tab" data-toggle="tab"><i class="fa fa-heart"></i> {!! trans('auth.as_wisher') !!}</a></li>
                <li role="presentation"><a href="#asMerchant" aria-controls="asMerchant" role="tab" data-toggle="tab"><i class="fa fa-usd"></i> {!! trans('auth.as_merchant') !!}</a></li>
            </ul>
            <div class="tab-content">
                <section role="tabpanel" class="tab-pane active fade in" id="asWisher">
                    <form class="remodal-form" id="form-register">
                        <div class="form-group" id="register-email">
                            <label class="form-label" for="register-email">
                                <span class="form-label-content">Email*
                                    <span id="help-block-email"></span>
                                </span>
                            </label>
                            <input type="email" class="form-control" name="email" id="register-input-email" placeholder="Email*"/>
                        </div>
                        <div class="form-group" id="register-password">
                            <label class="form-label" for="register-password">
                                <span class="form-label-content">Password*
                                    <span id="help-block-password"></span>
                                </span>
                            </label>
                            <input type="password" class="form-control" name="password" id="register-input-password" placeholder="Password*"/>
                        </div>
                        <div class="form-group" id="register-password_confirmation">
                            <label class="form-label" for="register-password_confirmation">
                                <span class="form-label-content">Password Confirmation*
                                    <span id="help-block-password_confirmation"></span>
                                </span>
                            </label>
                            <input type="password" class="form-control" name="password_confirmation" id="register-input-password_confirmation" placeholder="Password Confirmation*"/>
                        </div>
                        <div class="form-group" id="register-first_name">
                            <label class="form-label" for="register-first_name">
                                <span class="form-label-content">First Name*
                                    <span id="help-block-first_name"></span>
                                </span>
                            </label>
                            <input type="text" class="form-control" name="first_name" id="register-input-first_name" placeholder="First Name*"/>
                        </div>
                        <div class="form-group" id="register-last_name">
                            <label class="form-label" for="register-last_name">
                                <span class="form-label-content">Last Name*
                                    <span id="help-block-last_name"></span>
                                </span>
                            </label>
                            <input type="text" class="form-control" name="last_name" id="register-input-last_name" placeholder="Last Name*"/>
                        </div>
                        <div class="form-group" id="register-gender">
                            <div class="radio">
                                <span id="help-block-gender"></span><br>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="male"> <i class="fa fa-male fa-2x" aria-hidden="true"></i> {!! trans('general.male') !!}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="female"> <i class="fa fa-female fa-2x" aria-hidden="true"></i> {!! trans('general.female') !!}
                                </label>
                            </div>
                        </div>
                        <div class="form-group" id="register-birthday">
                            <label class="form-label" for="register-birthday">
                                <span class="form-label-content">Birthday*
                                    <span id="help-block-birthday"></span>
                                </span>
                            </label>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" name="day" placeholder="Day">
                                            <option value="">Day</option>
                                            @for ($x = 1; $x <= 31; $x++)
                                                <option value="{{ (strlen($x) == 1) ? '0'.$x : $x }}">{{ (strlen($x) == 1) ? '0'.$x : $x }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="month" placeholder="Month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="year" placeholder="Year">
                                        <option value="">Year</option>
                                        @for ($x = date('Y', strtotime('-50 years', strtotime(date('Y')))); $x <= date('Y'); $x++)
                                            <option value="{{ $x }}">{{ $x }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <!-- <input type="text" class="form-control" name="birthday" id="register-input-birthday" placeholder="Birthday*"/> -->
                        </div>
                        <div class="form-group" id="register-terms">
                            <label id="register-terms">
                                <input type="checkbox" name="terms"> I agree to the <a href="{{ route('terms') }}">Terms*</a>
                            </label><br>
                            <span id="help-block-terms"></span>
                        </div>
                        <button type="submit" class="btn btn-login btn-block" id="button-register" data-loading-text="Loading...">Register</button>
                        <p>Already have a Maw account? <a href="#" id="to-register" data-remodal-target="loginModal">Sign in</a></p>
                    </form>
                    <div class="remodal-login-sosmed text-center">
                        <div class="remodal-login-sosmed-caption">
                            {!! trans('general.or_connect_with_socmed') !!}
                        </div>
                        <ul>
                            <li><a href="{{ route('auth-redirect-socialite',['provider' => 'facebook']) }}"><button type="button" class="btn btn-3"><i class="maw-facebook-o"></i></button></a></li>
                            <!-- <li><a href="{{ route('auth-redirect-socialite',['provider' => 'twitter']) }}"><button type="button" class="btn btn-3"><i class="maw-twitter-o"></i></button></a></li> -->
                            <li><a href="{{ route('auth-redirect-socialite',['provider' => 'google']) }}"><button type="button" class="btn btn-3"><i class="maw-google-plus-o"></i></button></a></li>
                            <!-- <li><button type="button" class="btn btn-3"><i class="maw-instagram-o"></i></button></li> -->
                        </ul>
                        <div class="remodal-login-sosmed-alternative">
                            Creating an account means you're okay with Maw's <a href="{{ route('terms') }}">Terms of Service</a>. <a href="#">Privacy Policy</a>
                        </div>
                    </div>
                </section>
                <section role="tabpanel" class="tab-pane fade" id="asMerchant">
                    <form class="remodal-form" id="form-register-merchant">
                        <h4>For your login</h4>
                        <div class="form-group" id="register-merchant-email">
                            <label for="register-merchant-input-email">Email Address*</label>
                            <input type="email" class="form-control" name="email" id="register-merchant-input-email" placeholder="Email Address*">
                        </div>
                        <div class="form-group" id="register-merchant-password">
                            <label for="register-merchant-input-password">Password*</label>
                            <input type="password" class="form-control" name="password" id="register-merchant-input-password" placeholder="Password*">
                        </div>
                        <div class="form-group" id="register-merchant-password_confirmation">
                            <label for="register-merchant-input-password_confirmation">Password Confirmation*</label>
                            <input type="password" class="form-control" name="password_confirmation" id="register-merchant-input-password_confirmation" placeholder="Password Confirmation*">
                        </div>
                        <br><h4>Information your store</h4>
                        <div class="form-group" id="register-merchant-first_name">
                            <label for="register-merchant-input-first_name">First Name*</label>
                            <input type="text" class="form-control" name="first_name" id="register-merchant-input-first_name" placeholder="First Name*">
                        </div>
                        <div class="form-group" id="register-merchant-last_name">
                            <label for="register-merchant-input-last_name">Last Name*</label>
                            <input type="text" class="form-control" name="last_name" id="register-merchant-input-last_name" placeholder="Last Name*">
                        </div>
                        <div class="form-group" id="register-merchant-store_name">
                            <label for="register-merchant-input-store_name">Store Name*</label>
                            <input type="text" class="form-control" name="store_name" id="register-merchant-input-store_name" placeholder="Store Name*">
                        </div>
                        <div class="form-group" id="register-merchant-store_address">
                            <label for="register-merchant-input-store_address">Store Address*</label>
                            <textarea class="form-control" name="store_address" id="register-merchant-input-store_address" placeholder="Store Address*"></textarea>
                        </div>
                        <div class="form-group" id="register-merchant-phone_number">
                            <label for="register-merchant-input-phone_number">Phone Number*</label>
                            <input type="text" class="form-control" name="phone_number" id="register-merchant-input-phone_number" placeholder="Phone Number*">
                        </div>
                        <div class="form-group" id="register-merchant-country">
                            <label for="register-merchant-input-country">Country*</label>
                            <input type="text" class="form-control" name="country" id="register-merchant-input-country" placeholder="Country*">
                        </div>
                        <div class="form-group" id="register-merchant-province">
                            <label for="register-merchant-input-province">Province*</label>
                            <input type="text" class="form-control" name="province" id="register-merchant-input-province" placeholder="Province*">
                        </div>
                        <div class="form-group" id="register-merchant-city">
                            <label for="register-merchant-input-city">City*</label>
                            <input type="text" class="form-control" name="city" id="register-merchant-input-city" placeholder="City*">
                        </div>
                        <div class="form-group" id="register-merchant-postal_code">
                            <label for="register-merchant-input-postal_code">Postal Code*</label>
                            <input type="text" class="form-control" name="postal_code" id="register-merchant-input-postal_code" placeholder="Postal Code*">
                        </div>
                        <div class="form-group" id="register-merchant-terms">
                            <label id="register-merchant-input-terms">
                                <input type="checkbox" name="terms"> I agree to the <a href="{{ route('terms') }}">Terms Merchant*</a>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-login btn-block" id="button-register-merchant" data-loading-text="Loading...">Register</button>
                    </form>
                    <div class="remodal-login-sosmed text-center">
                        <div class="remodal-login-sosmed-alternative">
                            Creating an account means you're okay with Maw's <a href="{{ route('terms') }}">Terms of Service</a>. <a href="#">Privacy Policy</a>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </div>
</div>

<!-- Forgot Modal -->
<div class="remodal remodal-login" data-remodal-id="forgotPassModal">
    <div class="remodal-box">
        <button data-remodal-action="close" class="remodal-close"></button>
        <section class="remodal-header">
            <h3>{!! trans('auth.forgot_password') !!}</h3>
            <p>{!! trans('auth.already_have_account') !!}? <a href="#" id="to-register" data-remodal-target="loginModal">{!! trans('auth.login_here') !!}</a>.</p>
        </section>
        <section class="remodal-body">
            <form class="remodal-form" id="form-forgot">
                <div id="alert-info-forgot-modal">
                    @if (session('status'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="form-group" id="forgot-email">
                    <input type="email" class="form-control" name="email" id="forgot-input-email" placeholder="Email Address*">
                </div>
                <button type="submit" class="btn btn-login btn-block" id="button-forgot" data-loading-text="Loading...">Submit</button>
            </form>
        </section>
    </div>
</div>

<!-- Change Password Modal -->
<div class="remodal remodal-login" data-remodal-id="changePassModal">
    <div class="remodal-box">
        <button data-remodal-action="close" class="remodal-close"></button>
        <section class="remodal-header">
            <h3>{!! trans('auth.change_password') !!}</h3>
            <p>{!! trans('auth.already_have_account') !!}? <a href="#" id="to-register" data-remodal-target="loginModal">{!! trans('auth.login_here') !!}</a>.</p>
        </section>
        <section class="remodal-body">
            <form class="remodal-form" id="form-change-pass">
                <div id="alert-info-change-pass-modal">
                    @if (session('status'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                {!! Form::hidden('login', session('user-change-password.login')) !!}
                {!! Form::hidden('code', session('user-change-password.code')) !!}
                <div class="form-group" id="change-password">
                    <input type="password" class="form-control" name="password" id="change-input-password" placeholder="New Password*">
                </div>
                <div class="form-group" id="change-password_confirmation">
                    <input type="password" class="form-control" name="password_confirmation" id="change-input-password_confirmation" placeholder="Confirm New Password*">
                </div>
                <button type="submit" class="btn btn-3 btn-block" id="button-change-pass" data-loading-text="Loading...">Submit</button>
            </form>
        </section>
    </div>
</div>
