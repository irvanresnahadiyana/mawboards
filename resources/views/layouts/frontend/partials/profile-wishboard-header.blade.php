<div class="profile-wishboard-header">
    <section class="profile-pict">
        <a href="#" class="objectfit-container-cover">
            <img class="objectfit-img" src="{{ link_to_avatar(@$user ? @$user->images->first()->image_thumb : @user_info()->images->first()->image_thumb) }}" alt="">
        </a>
    </section>
    <section class="profile-wishboard-desc">
        <h1>{{ @$user ? $user->first_name.' '.$user->last_name : user_info('full_name') }}</h1>
        <div class="profile-wishboard-follow">
            <section class="profile-wishboard-follow-box">
                <span>0</span>
                <span>Follower</span>
            </section>
            <section class="profile-wishboard-follow-box">
                <span>0</span>
                <span>Following</span>
            </section>
        </div>
        <div class="profile-wishboard-tab">
            <ul>
                @if (!$user)
                    <li class="{{ Request::is('wishboard/*') ? 'active' : '' }}"><a href="{{ route('index-wishboard-frontend') }}">Wish Board</a></li>
                    <li><a href="#">Likes</a></li>
                    <li class="{{ Request::is('circle/*') ? 'active' : '' }}"><a href="{{ route('index-circle-frontend') }}">Circle</a></li>
                    @if(!Request::is('circle/show/*'))
                        <li class="{{ Request::is('friend/*') ? 'active' : '' }}"><a href="{{ route('index-friend-frontend') }}">Friend</a></li>
                    @endif
                @else
                    @if ($user->id != user_info('id'))
                        @if (!$friend_status['status'])
                            @if (user_info()->hasFriendRequestFrom($user))
                                <li><a href="#" id="accept-friend" onclick="responseFriendFunction({{ $user->id }}, {{ user_info('id') }}, 'acceptFriendRequest'); return false;" title="Accept Friend">Accept Friend</a></li>
                                <li><a href="#" id="denied-friend" onclick="responseFriendFunction({{ $user->id }}, {{ user_info('id') }}, 'denyFriendRequest'); return false;" title="Denied Friend">Denied Friend</a></li>
                                <li><a href="#" id="block-friend" onclick="responseFriendFunction({{ $user->id }}, {{ user_info('id') }}, 'blockFriend'); return false;" title="Block Friend">Block Friend</a></li>
                            @else
                                @if (user_info()->hasBlocked($user))
                                    <li><a href="#" id="unblock-friend" onclick="responseFriendFunction({{ user_info('id') }}, {{ $user->id }}, 'unblockFriend'); return false;" title="Unblock Friend">Unblock Friend</a></li>
                                @else
                                    <li><a href="#" id="invite-friend" title="Add Friend">Add Friend</a></li>
                                @endif
                            @endif
                        @else
                            @if ($friend_status['message'] != 'isFriend')
                                <li><a href="#" id="remove-friend" onclick="removeFriendFunction({{ user_info('id') }}, {{ $user->id }}, 'cancelFriendRequest'); return false;" title="Cancel friend request">Cancel friend request</a></li>
                                {{ $friend_status['message'] }}
                            @else
                                <li><a href="#" id="remove-friend" onclick="removeFriendFunction({{ user_info('id') }}, {{ $user->id }}, 'removeFriend'); return false;" title="Remove friend">Remove friend</a></li>
                            @endif
                        @endif
                    @endif
                @endif
            </ul>
        </div>
    </section>
    <section class="profile-wishboard-option">
        <ul>
            @if(Request::is('circle/show/*'))
                <li><a href="{{ route('index-circle-frontend') }}"><i class="fa fa-backward"></i></a></li>
            @else
                <li><a href="{{ route('index') }}"><i class="fa fa-home"></i></a></li>
            @endif
            <li><a href="#"><i class="maw-share"></i></a></li>
            <li><a href="#"><i class="maw-eq"></i></a></li>
            <li><a href="#"><i class="maw-more"></i></a></li>
        </ul>
    </section>
</div>
