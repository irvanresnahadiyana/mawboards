<!DOCTYPE html>
<html lang="en">
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta('robots', 'noindex, nofollow') !!}
        {!! Html::meta('author', env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('product', env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('title', isset($meta['title']) ? $meta['title'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('description', isset($meta['description']) ? $meta['description'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta('keywords', isset($meta['keywords']) ? $meta['keywords'] : env('APP_NAME', 'Mawboards')) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1') !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        <title>@yield('title') | {{ env('APP_NAME', 'Mawboards') }}</title>
        <!-- Pace -->
        {!! Html::style('bower_components/admin-lte/plugins/pace/pace.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('bower_components/components-font-awesome/css/font-awesome.min.css') !!}
        <!-- Datepicker -->
        {!! Html::style('bower_components/admin-lte/plugins/datepicker/datepicker3.css') !!}
        <!-- Sweetalert -->
        {!! Html::style('bower_components/sweetalert/dist/sweetalert.css') !!}
        <!-- MAIN Style -->
        {!! Html::style('frontend/assets/css/styles.css') !!}
        <!-- Custom Frontend css -->
        {!! Html::style('css/frontend-css.css') !!}
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="icon" type="image/png" href="{{ asset('frontend/assets/images/logo.png') }}" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ asset('frontend/assets/images/logo.png') }}" sizes="16x16" />
        @yield('header')
    </head>
    <body>
        @if (user_info())
            <div class="side-menu">
                <ul>
                    <li>
                        <div class="side-menu-quick">
                            <button class="side-menu-quick-btn" type="button" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="maw-add-o"></i></button>
                            <ul class="side-menu-quick-menu">
                                <li>
                                    <button class="side-menu-quick-drop" type="button"><i class="maw-image"></i></button>
                                </li>
                                <li>
                                    <button class="side-menu-quick-drop" type="button"><i class="maw-chain-o"></i></button>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <button type="button" id="gotop"><i class="maw-arrow-up-o"></i></button>
                    </li>
                </ul>
            </div>
        @endif
        <!-- Header -->
        @if (!Request::is('circle/*', 'wishboard/*', 'wish/*', 'friend/*', 'profile/index'))
            <header class="header">
                <section class="container">
                    <div class="header-logo">
                        <section class="header-box-logo">
                            <a href="{{ route('index') }}">
                                <img src="{{ asset('frontend/assets/images/logo.png') }}" alt="Maw Logo" />
                            </a>
                        </section>
                    </div>
                    <div class="header-box">
                        <section class="header-box-menu">
                            <ul>
                                <li><a href="{{ route('about') }}">About Us</a></li>
                                <li><a href="{{ route('terms') }}">Terms</a></li>
                                <li><a href="{{ route('faq') }}">Faq</a></li>
                                <li>
                                    <div class="dropdown dropdown-language">
                                        <a href="#" class="dropdown-toggle" type="button" role="button" id="language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Language <span class="flag-icon flag-icon-id"></span></a>
                                        <ul class="dropdown-menu"  aria-labelledby="language">
                                            <li><a href="#"><span class="flag-icon flag-icon-id pull-left"></span>Bahasa</a></li>
                                            <li><a href="#"><span class="flag-icon flag-icon-gb pull-left"></span>English</a></li>
                                            <li><a href="#"><span class="flag-icon flag-icon-us pull-left"></span>English(US)</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown dropdown-currency">
                                        <a href="#" class="dropdown-toggle" type="button" role="button" id="currency" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Currency <span class="currency currency-idr"></span></a>
                                        <ul class="dropdown-menu"  aria-labelledby="currency">
                                            <li><a href="#"><span class="flag-icon flag-icon-id pull-left"></span>IDR</a></li>
                                            <li><a href="#"><span class="flag-icon flag-icon-us pull-left"></span>USD</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <!-- <li>
                                    <div class="sb-search" id="sb-search">
                                        <form>
                                            <input type="search" class="sb-search-input" placeholder="Search..." value="" name="search" id="search">
                                            <input class="sb-search-submit" type="submit" value="">
                                            <span class="sb-icon-search"></span>
                                        </form>
                                    </div>
                                </li> -->
                            </ul>
                        </section>
                        <section class="header-box-user">
                            @if (user_info())
                                <div class="header-loggedin">
                                    <ul>
                                        <li>Welcome <span>{{ user_info('full_name') }}</span>!</li>
                                        <li>
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="maw-user"></i></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="text-uppercase">Your Lists</a></li>
                                                    <li><a href="#" class="text-uppercase">News</a></li>
                                                    <li><a href="{{ route('profile-message') }}" class="text-uppercase">Messages</a></li>
                                                    <li><a href="{{ route('index-profile-frontend') }}" class="text-uppercase">Profile</a></li>
                                                    <li><a href="{{ route('auth-logout') }}" class="text-uppercase">Logout</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="maw-bell"></i></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <button class="media">
                                                            <div class="media-left">
                                                                <a href="#2" class="objectfit-container-cover">
                                                                    <img class="media-object objectfit-img" src="https://api.adorable.io/avatars/285/abott@adorable" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <a href="#">Jeff Bridge</a>
                                                                <section class="media-wishboard">
                                                                    My Birthday Wishboard
                                                                </section>
                                                            </div>
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button class="media">
                                                            <div class="media-left">
                                                                <a href="#2" class="objectfit-container-cover">
                                                                    <img class="media-object objectfit-img" src="https://api.adorable.io/avatars/285/abott@adorable" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <a href="#">Alexis Cuarezma</a>
                                                                <section class="media-wishboard">
                                                                    My Anniversary Wishboard
                                                                </section>
                                                            </div>
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button class="media">
                                                            <div class="media-left">
                                                                <a href="#2" class="objectfit-container-cover">
                                                                    <img class="media-object objectfit-img" src="https://api.adorable.io/avatars/285/abott@adorable" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <a href="#">Selena Gomez</a>
                                                                <section class="media-wishboard">
                                                                    My Wedding Wishboard
                                                                </section>
                                                            </div>
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-remodal-target="search-modal"><i class="maw-search-1"></i></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- <div class="dropdown">
                                    @if (user_info('gender') == 'male')
                                        <button class="user-menu user-menu-male dropdown-toggle" type="button" id="user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></button>
                                    @else
                                        <button class="user-menu user-menu-female dropdown-toggle" type="button" id="user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></button>
                                    @endif
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="user-menu">
                                        <li><a href="#" class="text-uppercase">Your Lists</a></li>
                                        <li><a href="#" class="text-uppercase">News</a></li>
                                        <li><a href="{{ route('profile-message') }}" class="text-uppercase">Messages</a></li>
                                        <li><a href="{{ route('index-profile-frontend') }}" class="text-uppercase">Profile</a></li>
                                        <li><a href="{{ route('auth-logout') }}" class="text-uppercase">Logout</a></li>
                                    </ul>
                                </div> -->
                            @else
                                <section class="header-login">
                                    <ul>
                                        <li>
                                            <button type="button" class="btn btn-3" data-remodal-target="registerModal">Register</button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-3" data-remodal-target="loginModal">Login</button>
                                        </li>
                                    </ul>
                                </section>
                            @endif
                        </section>
                    </div>
                </section>
            </header>
        @endif
        <!-- End Header -->
        <!-- Content -->
        <div class="innerContent">
            <section class="container">
                @yield('content')
            </section>
        </div>
        <!-- End Content -->
        <!-- Wishes Modal -->
        @include('layouts.frontend.partials.modal-wishes')
        @if (!user_info())
            <!-- Authentication Modal -->
            @include('layouts.frontend.partials.modal-auth')
        @endif
        <!-- Pined Modal -->
        @include('layouts.frontend.partials.modal-pin')
        <!-- Search modal -->
        <div class="remodal" data-remodal-id="search-modal">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Search something ?</h1>
            <form action="{{ route('search') }}" method="post" id="form-search">
                {{ csrf_field() }}
                <div class="form-group" id="post-search">
                    <input type="text" class="form-control" name="search" id="post-input-search" placeholder="Search something ?" autofocus="">
                </div>
                <p>
                    <br><i>You can search everythings (Products, Wishes, Friends and other)</i>
                </p>
                <br>
                <button class="btn btn-success btn-lg">FIND</button>
            </form>
        </div>

        <!-- JAVASCRIPT-->
        {!! Html::script('frontend/assets/js/jquery-3.1.1.min.js') !!}
        {!! Html::script('frontend/assets/js/bootstrap.min.js') !!}
        {!! Html::script('frontend/assets/js/masonry.pkgd.min.js') !!}
        {!! Html::script('frontend/assets/js/imagesloaded.pkgd.min.js') !!}
        {!! Html::script('frontend/assets/js/perfect-scrollbar.min.js') !!}
        {!! Html::script('frontend/assets/js/remodal.min.js') !!}
        {!! Html::script('frontend/assets/js/classie.js') !!}
        {!! Html::script('frontend/assets/js/modernizr-custom.js') !!}
        <!-- Pace -->
        {!! Html::script('bower_components/admin-lte/plugins/pace/pace.min.js') !!}
        <!-- Datepicker -->
        {!! Html::script('bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js') !!}
        <!-- Sweetalert -->
        {!! Html::script('bower_components/sweetalert/dist/sweetalert.min.js') !!}
        <script type="text/javascript">
            (function() {
                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                if (!String.prototype.trim) {
                    (function() {
                        // Make sure we trim BOM and NBSP
                        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                        String.prototype.trim = function() {
                            return this.replace(rtrim, '');
                        };
                    })();
                }

                [].slice.call( document.querySelectorAll( 'input.form-control-1' ) ).forEach( function( inputEl ) {
                    // in case the input is already filled..
                    if( inputEl.value.trim() !== '' ) {
                        classie.add( inputEl.parentNode, 'input--filled' );
                    }

                    // events:
                    inputEl.addEventListener( 'focus', onInputFocus );
                    inputEl.addEventListener( 'blur', onInputBlur );
                } );

                function onInputFocus( ev ) {
                    classie.add( ev.target.parentNode, 'input--filled' );
                }

                function onInputBlur( ev ) {
                    if( ev.target.value.trim() === '' ) {
                        classie.remove( ev.target.parentNode, 'input--filled' );
                    }
                }
            })();

            if ( ! Modernizr.objectfit ) {
                $('.objectfit-container-cover').each(function () {
                    var $container = $(this),
                    imgUrl = $container.find('.objectfit-img').prop('src');
                    if (imgUrl) {
                        $container
                        .css('backgroundImage', 'url(' + imgUrl + ')')
                        .addClass('objectfit-cover');
                    }
                });
                $('.objectfit-container-contain').each(function () {
                    var $container = $(this),
                    imgUrl = $container.find('.objectfit-img').prop('src');
                    if (imgUrl) {
                        $container
                        .css('backgroundImage', 'url(' + imgUrl + ')')
                        .addClass('objectfit-contain');
                    }
                });
            }
        </script>

        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ajaxStart(function() {
                Pace.restart();
            });

            var base_url = '{{ route("index") }}';
            var bookmark_url = "{{ route('bookmark-wish-frontend') }}";
            var like_url = "{{ route('like-wish-frontend') }}";
            var msgInfoLoginReg = "{{trans('general.notification.login_or_register')}}"
            var urlDetailWish = "{{route('detail-wish-frontend')}}";
            var urlPostComment = "{{ route('store-comment-frontend') }}";
            var currentURL = window.location.href;

            var scrollBar = document.getElementById('modal-scrollBar');

            var $grid = $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                fitWidth: true
            });

            var $modalGrid = $('.modal-grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                fitWidth: true
            });

            var $modalWhisboard = $('.modal-wishboard-masonry').masonry({
                itemSelector: '.modal-wishboard-item',
                columnWidth: '.modal-wishboard-sizer',
                percentPosition: true,
                fitWidth: true
            });

            $grid.imagesLoaded().progress( function() {
                $grid.masonry('layout');
            });

            var finish = true;
            var x = 1;

            var options = {hashTracking: false, closeOnOutsideClick: false};
            var loginModal = $('[data-remodal-id=loginModal]').remodal(options);
            var registerModal = $('[data-remodal-id="registerModal"]').remodal(options);
            var forgotPassModal = $('[data-remodal-id="forgotPassModal"]').remodal(options);
            var changePassModal = $('[data-remodal-id="changePassModal"]').remodal(options);
            var inst = $('[data-remodal-id=modal-wish-detail]').remodal(options);

            function popoverDestroy() {

            }

            

            function generateNotif(title = null, text = null, type = null, reload = false) {
                $('.modal').modal('hide');
                swal({
                    title: title,
                    text: text,
                    type: type,
                    allowEscapeKey: false,
                },
                function(){
                    if (reload) {
                        location.reload();
                    }
                });
            }

            //Date picker
            $('.datepicker').datepicker({
                format: 'dd MM yyyy',
                autoclose: true
            });

            // Check session for modal show
            var status_user = "{{ user_info() }}";
            if (!status_user) {
                loginModal.open();
                // $('#loginModal').modal({
                    // backdrop: 'static',
                    // keyboard: false
                // });
            } else if (status_user == 'user-change-password') {
                changePassModal.open();
                // $('#changePassModal').modal({
                //     backdrop: 'static',
                //     keyboard: false
                // });
            } else {
                var status = "{{ session('status') }}";
                if (status) {
                    generateNotif('', status, null, false);
                }
            }

            // Remove all info if click other modal show
            // $('.btn').click(function(event) {
            //     event.preventDefault();
            //     $('.alert').remove();
            // });

            // Login
            $('#button-login').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-login-post") }}',
                    data: $('#form-login').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        window.location = data.redirect;
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            $('#alert-info-login-modal').html('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.message+'</div>');
                        }

                        $.each(data,function(key, val){
                            $('#login-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#login-input-'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });

            // Register
            $('#button-register').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                // $('.form-group').removeClass('has-error');
                // $('.checkbox').removeClass('has-error');
                // $('.help-block').remove();
                $('.help-block-field').remove();
                $('br').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-register-post") }}',
                    data: $('#form-register').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        location.reload();
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            $('.form-group').addClass('has-error');
                            $('<span class="help-block"><font color="red">'+data.message+'</font></span>').insertAfter($('.modal-title'));
                        }

                        $.each(data,function(key, val) {
                            // $('#register-'+key).addClass('has-error');
                            // $('<span class="help-block">'+val+'</span>').insertAfter($('#register-input-'+key));
                            if (key == 'day' || key == 'month' || key == 'year') {
                                $('<br><font class="help-block-field" color="#ff3333">'+val+'</font>').insertAfter($('#help-block-birthday'));
                            } else {
                                $('<font class="help-block-field" color="#ff3333">'+val+'</font>').insertAfter($('#help-block-'+key));
                            }
                        });
                        $btn.button('reset');
                    }
                });
            });

            // Merchant Register
            $('#button-register-merchant').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.checkbox').removeClass('has-error');
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-register-post-merchant") }}',
                    data: $('#form-register-merchant').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        location.reload();
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            $('.form-group').addClass('has-error');
                            $('<span class="help-block"><font color="red">'+data.message+'</font></span>').insertAfter($('.modal-title'));
                        }

                        $.each(data,function(key, val){
                            $('#register-merchant-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#register-merchant-input-'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });

            // Forgot Password
            $('#button-forgot').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.alert').remove();
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-forgot-post") }}',
                    data: $('#form-forgot').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        location.reload();
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            $('#alert-info-forgot-modal').html('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.message+'</div>');
                        }

                        $.each(data,function(key, val){
                            $('#forgot-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#forgot-input-'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });

            // Change Password
            $('#button-change-pass').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.alert').remove();
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-change-password-post") }}',
                    data: $('#form-change-pass').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        location.reload();
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            $('#alert-info-change-pass-modal').html('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.message+'</div>');
                        }

                        $.each(data,function(key, val){
                            $('#change-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#change-input-'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });
        </script>

        <!-- JS for Search -->
        <script type="text/javascript">
            /**
            * uisearch.js v1.0.0
            * http://www.codrops.com
            *
            * Licensed under the MIT license.
            * http://www.opensource.org/licenses/mit-license.php
            *
            * Copyright 2013, Codrops
            * http://www.codrops.com
            */
            ;( function( window ) {

                'use strict';

                // EventListener | @jon_neal | //github.com/jonathantneal/EventListener
                !window.addEventListener && window.Element && (function () {
                    function addToPrototype(name, method) {
                        Window.prototype[name] = HTMLDocument.prototype[name] = Element.prototype[name] = method;
                    }

                    var registry = [];

                    addToPrototype("addEventListener", function (type, listener) {
                        var target = this;

                        registry.unshift({
                            __listener: function (event) {
                                event.currentTarget = target;
                                event.pageX = event.clientX + document.documentElement.scrollLeft;
                                event.pageY = event.clientY + document.documentElement.scrollTop;
                                event.preventDefault = function () { event.returnValue = false };
                                event.relatedTarget = event.fromElement || null;
                                event.stopPropagation = function () { event.cancelBubble = true };
                                event.relatedTarget = event.fromElement || null;
                                event.target = event.srcElement || target;
                                event.timeStamp = +new Date;

                                listener.call(target, event);
                            },
                            listener: listener,
                            target: target,
                            type: type
                        });

                        this.attachEvent("on" + type, registry[0].__listener);
                    });

                    addToPrototype("removeEventListener", function (type, listener) {
                        for (var index = 0, length = registry.length; index < length; ++index) {
                            if (registry[index].target == this && registry[index].type == type && registry[index].listener == listener) {
                                return this.detachEvent("on" + type, registry.splice(index, 1)[0].__listener);
                            }
                        }
                    });

                    addToPrototype("dispatchEvent", function (eventObject) {
                        try {
                            return this.fireEvent("on" + eventObject.type, eventObject);
                        } catch (error) {
                            for (var index = 0, length = registry.length; index < length; ++index) {
                                if (registry[index].target == this && registry[index].type == eventObject.type) {
                                    registry[index].call(this, eventObject);
                                }
                            }
                        }
                    });
                })();

                // http://stackoverflow.com/a/11381730/989439
                function mobilecheck() {
                    var check = false;
                    (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
                    return check;
                }

                // http://www.jonathantneal.com/blog/polyfills-and-prototypes/
                !String.prototype.trim && (String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, '');
                });

                function UISearch( el, options ) {
                    this.el = el;
                    this.inputEl = el.querySelector( 'form > input.sb-search-input' );
                    this._initEvents();
                }

                UISearch.prototype = {
                    _initEvents : function() {
                        var self = this,
                        initSearchFn = function( ev ) {
                            ev.stopPropagation();
                            // trim its value
                            self.inputEl.value = self.inputEl.value.trim();

                            if( !classie.has( self.el, 'sb-search-open' ) ) { // open it
                                ev.preventDefault();
                                self.open();
                            }
                            else if( classie.has( self.el, 'sb-search-open' ) && /^\s*$/.test( self.inputEl.value ) ) { // close it
                                ev.preventDefault();
                                self.close();
                            }
                        }

                        this.el.addEventListener( 'click', initSearchFn );
                        this.el.addEventListener( 'touchstart', initSearchFn );
                        this.inputEl.addEventListener( 'click', function( ev ) { ev.stopPropagation(); });
                        this.inputEl.addEventListener( 'touchstart', function( ev ) { ev.stopPropagation(); } );
                    },
                    open : function() {
                        var self = this;
                        classie.add( this.el, 'sb-search-open' );
                        // focus the input
                        if( !mobilecheck() ) {
                            this.inputEl.focus();
                        }
                        // close the search input if body is clicked
                        var bodyFn = function( ev ) {
                            self.close();
                            this.removeEventListener( 'click', bodyFn );
                            this.removeEventListener( 'touchstart', bodyFn );
                        };
                        document.addEventListener( 'click', bodyFn );
                        document.addEventListener( 'touchstart', bodyFn );
                    },
                    close : function() {
                        this.inputEl.blur();
                        classie.remove( this.el, 'sb-search-open' );
                    }
                }

                // add to global namespace
                window.UISearch = UISearch;

            } )( window );
            new UISearch( document.getElementById( 'sb-search' ) );
        </script>
        <!-- Custom Frontend JS -->
        @yield('script')
        {!! Html::script('js/global.js') !!}
        {!! Html::script('js/global-function.js') !!}
    </body>
</html>
