<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ link_to_avatar($user_avatar ? $user_avatar->image_standard : 'no_image') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ user_info('full_name') }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            @if(user_info()->inRole('administrator'))
                <li class="{!! (url(route('admin-dashboard')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin-dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                </li>
                <li class="treeview{!! (Request::is('admin/user/*') OR Request::is('admin/user')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-user"></i> <span>Management User</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @if(user_info()->inRole('superadmin'))
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('admin-role')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin-role')}}"><span>Role</span></a>
                        </li>
                    </ul>
                    @endif
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('admin-user')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin-user')}}"><span>User List</span></a>
                        </li>
                        <li class="">
                            <a href="#"><span>Friend List</span></a>
                        </li>
                    </ul>
                </li>
                <li class="treeview{!! (Request::is('admin/product/*') OR Request::is('admin/product')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-cubes"></i> <span>Management Product</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('admin-product')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin-product')}}"><span>Product</span></a>
                        </li>
                        <li class="{!! (url(route('admin-product-category')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin-product-category')}}"><span>Product Category</span></a>
                        </li>
                    </ul>
                </li>
                <li class="treeview{!! (Request::is('admin/wish/*') OR Request::is('admin/wish')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-gift"></i> <span>Management Wish</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('admin-wish')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin-wish')}}"><span>Wish List</span></a>
                        </li>
                        <li class="{!! (url(route('admin-wish-category')) == Request::url() OR Request::is('admin/wish/wish-category/*')) ? 'active' : '' !!}">
                            <a href="{{route('admin-wish-category')}}"><span>Wish Category</span></a>
                        </li>
                        <li class="{!! (url(route('admin-wish-color')) == Request::url() OR Request::is('admin/wish/wish-color/*')) ? 'active' : '' !!}">
                            <a href="{{route('admin-wish-color')}}"><span>Color</span></a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>Management Master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="#"><span>Management Category Shop</span></a></li>
                      <li><a href="#"><span>Management Reseller</span></a></li>
                      <li><a href="#"><span>Management Payment</span></a></li>
                      <li><a href="#"><span>Management Brand</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>Reseller</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="#"><span>List Resller</span></a></li>
                      <li><a href="#"><span>Product Reseller</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-cog"></i> <span>{{ trans('general.settings') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('setting-frontend-static-pages') }}">Frontend Static Pages</a></li>
                    </ul>
                </li>
            @endif
            @if(user_info()->inRole('merchant'))
                <li class="{!! (url(route('merchant-dashboard')) == Request::url()) ? 'active' : '' !!}"><a href="{{route('merchant-dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>Management Master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="#"><span>Management Order</span></a></li>
                      <li><a href="#"><span>Management Invoice</span></a></li>
                    </ul>
                </li>
                <li class="treeview{!! (Request::is('merchant/product/*') OR Request::is('merchant/product')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-cubes"></i> <span>Product Management</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li class="{!! (url(route('merchant-product')) == Request::url() ? 'active' : '') !!}"><a href="{{route('merchant-product')}}"><span>Product</span></a></li>
                    </ul>
                </li>
            @endif
            <li><a href="{{ route('auth-logout') }}"><i class="fa fa-power-off"></i> <span>Sign Out</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
    <!-- Home tab content -->
    <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
            <li>
                <a href="javascript::;">
                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                    <div class="menu-info">
                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                        <p>Will be 23 on April 24th</p>
                    </div>
                </a>
            </li>
        </ul>
        <!-- /.control-sidebar-menu -->
        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
            <li>
                <a href="javascript::;">
                    <h4 class="control-sidebar-subheading">
                        Custom Template Design
                        <span class="pull-right-container">
                            <span class="label label-danger pull-right">70%</span>
                        </span>
                    </h4>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                    </div>
                </a>
            </li>
        </ul>
        <!-- /.control-sidebar-menu -->
    </div>
    <!-- /.tab-pane -->
    <!-- Stats tab content -->
    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
    <!-- /.tab-pane -->
    <!-- Settings tab content -->
    <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
            <h3 class="control-sidebar-heading">General Settings</h3>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                    Report panel usage
                    <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                    Some information about this general settings option
                </p>
            </div>
            <!-- /.form-group -->
        </form>
    </div>
    <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
