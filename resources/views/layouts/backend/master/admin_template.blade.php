<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        <title>{{ env('APP_NAME', 'MyCms Web Admin') }} | @yield('title', 'Admin')</title>
        <!-- Tell the browser to be responsive to screen width -->
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('bower_components/components-font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('bower_components/admin-lte/dist/css/skins/_all-skins.min.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/admin-lte/plugins/pace/pace.min.css') !!}
        <!-- Animate style Noty-->
        {!! Html::style('bower_components/noty/demo/animate.css') !!}
        <!-- Select-2 -->
        {!! Html::style('bower_components/admin-lte/plugins/select2/select2.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/admin-lte/dist/css/AdminLTE.min.css') !!}
        <!-- wysihtml5 -->
        {!! Html::style('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
        <!-- ICheck -->
        {!! Html::style('bower_components/admin-lte/plugins/iCheck/all.css') !!}
        <!-- Datatables -->
        {!! Html::style('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css') !!}
        <!-- Datepicker -->
        {!! Html::style('bower_components/admin-lte/plugins/datepicker/datepicker3.css') !!}
        <!-- Custom CSS -->
        {!! Html::style('css/admin-css.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('header')
    </head>
<!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
-->
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('layouts/backend/partials/header')

            <!-- Sidebar -->
            @include('layouts/backend/partials/sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('page_title', 'Admin')
                        <small>@yield('page_description', 'Admin')</small>
                    </h1>
                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Footer -->
            @include('layouts/backend/partials/footer')
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery-->
        {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap-->
        {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! Html::script('bower_components/admin-lte/dist/js/app.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/admin-lte/plugins/pace/pace.min.js') !!}
        <!-- Noty -->
        {!! Html::script('bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
        <!-- Select-2 -->
        {!! Html::script('bower_components/admin-lte/plugins/select2/select2.min.js') !!}
        <!-- wysihtml5 -->
        {!! Html::script('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
        <!-- ICheck -->
        {!! Html::script('bower_components/admin-lte/plugins/iCheck/icheck.min.js') !!}
        <!-- IFrame -->
        {!! Html::script('plugins/jquery-iframe-transport-master/jquery.iframe-transport.js') !!}
        <!-- JQuery Datatables -->
        {!! Html::script('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') !!}
        <!-- Datatables -->
        {!! Html::script('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') !!}
        <!-- Datepicker -->
        {!! Html::script('bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js') !!}
        <!-- General JS -->
        {!! Html::script('js/general.js') !!}
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ajaxStart(function() {
                Pace.restart();
            });

            var status = "{{ session('status') }}";
            if (status) {
                generateNotif('topRight', 'success', status);
            }

            function generateNotif(layout, type, text = '') {
                $.noty.closeAll();
                notif = noty({
                    layout      : layout,
                    theme       : 'relax',
                    type        : type,
                    text        : text,
                    dismissQueue: true,
                    animation   : {
                        open  : 'animated bounceInRight',
                        close : 'animated bounceOutRight',
                        easing: 'swing',
                        speed : 500
                    },
                    timeout: 5000,
                    modal: false,
                    maxVisible: 1,
                    closeWith: ['click'],
                });
            }
            $(document).ready(function(){

              $('.select2').select2({
                allowClear:true
              });

              //Date picker
              $('.datepicker').datepicker({
                format: 'dd MM yyyy',
                autoclose: true
              });

              //iCheck for checkbox and radio inputs
              $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });

              $(".textarea").wysihtml5();

              $('.wysihtml5-toolbar .bootstrap-wysihtml5-insert-link-modal [data-wysihtml5-dialog-action="cancel"]').remove();
            });

            $(document).on('keydown',".number_only",function(event) {
              // Allow: backspace, delete, tab, escape, and enter
              if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                  // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                  // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                  // let it happen, don't do anything
                  return;
              }
              else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105  )) {
                  event.preventDefault();
                }
              }
            });
            $(document).on('keyup','[type="text"], [type="email"], [type="password"]',function(){
                $(this).parent('div').parent('div').removeClass('has-error');
                $(this).next('span').remove();
            });

            $(document).on('keyup','textarea',function(){
                $(this).parent('div').parent('div').removeClass('has-error');
                $(this).next('span').remove();
            });

            $(document).on('change','select',function(){
                $(this).parent('div').parent('div').removeClass('has-error');
                $(this).next('span.help-block').remove();
            });
        </script>
        @yield('script')
    </body>
</html>
