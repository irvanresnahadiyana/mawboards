@extends('layouts/backend/master/admin_template')

@section('title', 'Product')
@section('page_title', 'Product')
@section('page_description', 'My Products')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('merchant-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{ route('merchant-product') }}"><i class="fa fa-cubes"></i> Products</a></li>
    </ol>
@endsection

@section('content')
  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-12">
      <a href="{{route('merchant-product-create')}}" class="btn btn-info">Create New Product</a>
    </div>  
  </div>
  <br>
  <!-- /.row -->

  <div id="list-products"></div>

  <!-- modal delete .start here -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalLabel">Delete Product</h4>
                </div>
                <form id="delete-product" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" class="form-control" id="id">
                        </div>
                        <p>Are you sure delete this product?</p>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" title="Cancel">Cancel</button>
                        <input type="submit" id="submit-delete" class="btn btn-primary" title="Delete" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal delete .end here -->
@endsection

@section('script')
  @include('contents.merchant.products.js.index_js')
@endsection
