@extends('layouts/backend/master/admin_template')

@section('title', 'Product')
@section('page_title', 'Create Product')
@section('page_description', 'Create New Product')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('merchant-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{ route('merchant-product') }}"><i class="fa fa-cubes"></i> Products</a></li>
        <li>Created Product</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="create-product">
                    <div class="box-body">
                        <div class="error"></div>

                        <div class="form-group" id="product-category">
                            <label class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-9">
                                <select class="form-control select2" placeholder="Category" name="category" id="category">
                                    <option></option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="has_child" id="has-child">
                        <div class="form-group" id="product-category_child">
                            <label class="col-sm-2 control-label">Category Child</label>
                            <div class="col-sm-9">
                                <select class="form-control select2" placeholder="Category Child" name="category_child" id="category_child" disabled="true">
                                    
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="product-product_name">
                            <label class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="product_name" class="form-control" placeholder="Input product name" id="product_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-9">
                                <input type="text" name="price" class="form-control number_only" placeholder="Input price">
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-sm-2 control-label">Wholesales Price</label>
                            <div class="col-sm-3">
                                <input type="text" name="wholesale_price[]" class="form-control number_only" placeholder="Price">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="quantity[]" class="form-control number_only" placeholder="Quantity">
                            </div>
                            <div class="col-sm-3">
                                <label><input type="checkbox" name="price_active[]" class="minimal"> Is Active</label>
                            </div>
                            <div class="col-sm-1" style="padding: 5px 0 0 0;">
                              <button class="btn btn-sm btn-primary add-more-wholesale-price"><i class="fa fa-plus"></i></button>
                            </div>

                        </div>
                        <div class="append-wholesale-price"></div> -->

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Description</label>
                          <div class="col-sm-9">
                            <textarea class="textarea" name="description"></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Product Image</label>
                          <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary add-more-product-image">Add More Image</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="append-product-image"></div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Stock</label>
                            <div class="col-sm-9">
                                <input type="text" name="stock" class="number_only form-control" placeholder="Stock">
                            </div>
                        </div>

                        <div class="dimension">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Dimension</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="weight" class="form-control" placeholder="Weight">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="length" class="form-control" placeholder="Length">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-2">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="width" class="form-control" placeholder="Width">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="height" class="form-control" placeholder="Height">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Is Active</label>
                            <div class="col-sm-9">
                                <input type="checkbox" name="is_active" class="minimal">
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <a href="{{ route('merchant-product') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.merchant.products.js.create_js')
@endsection
