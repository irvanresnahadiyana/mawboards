<div class="row">
  @if(count('products') > 0)
    @foreach($products as $product)
      <div class="col-md-3">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user box-product">
          <div class="ribbon-wrapper">
            @if($product->is_active == true)
              <div class="ribbon ribbon-green">Active</div>
            @else
              <div class="ribbon ribbon-red">Not Active</div>
            @endif
          </div>
          <!-- Add the bg color to the header using any of the bg-* classes -->
          @if($product->image_primary)
            <img src="{{get_file($product->image_primary->image_standard,'thumbnail')}}" class="img-responsive">
          @else
            <img src="http://130.211.52.161/tradeo-content/themes/nucleare-pro/images/no-image-box.png" class="img-responsive">
          @endif
          <div class="box-body">
            <div class="row">
              <div class="col-sm-12">
                <h3 class="widget-user-username">{{$product->product_name}}</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 border-right">
                <div class="description-block">
                  <h5 class="description-header">{{number_format($product->price,0,',','.')}}</h5>
                  <span class="description-text">PRICE</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 border-right">
                <div class="description-block">
                  <h5 class="description-header">{{number_format($product->stock,0,',','.')}}</h5>
                  <span class="description-text">STOCK</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="row">
                  <div class="col-md-6">
                    <div class="description-block">
                      <h5 class="description-header"><i class="fa fa-thumbs-o-up"></i></h5>
                      <span class="widget-user-desc">{{number_format($product->total_like,0,',','.')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="description-block">
                      <h5 class="description-header"><i class="fa fa-thumbs-o-down"></i></h5>
                      <span class="description-text">{{number_format($product->total_dislike,0,',','.')}}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-sm-6">
                <div class="description-block">
                  <h5 class="description-header"><i class="fa fa-clock-o"></i></h5>
                  <span class="widget-user-desc">{{time_elapsed_string($product->updated_at)}}</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
          </div>
          <div class="box-footer" style="padding-top: 10px;">
            <a href="{{route('merchant-product-edit', ['id' => $product->id])}}" class="btn btn-default">Edit</a>
            <button type="button" class="btn btn-danger pull-right delete-btn" data-toggle="modal" href="#modal-form" data-id="{{$product->id}}">Delete</button>
          </div>
        </div>
        <!-- /.widget-user -->
      </div>
    @endforeach
  @else
    <div class="col-md-12">
        <p>No Products Available</p>
    </div>
  @endif
</div>