<script type="text/javascript">
    $(document).ready(function(){
        getProducts();
    });

    function getProducts() {
        $.ajax({
            url:"{{route('merchant-product')}}",
            method:"get",
            dataType:"json",
            success:function(data) {
                if (data.data.count > 0) {
                    $('#list-products').html(data.data.view);
                } else {
                    $('#list-products').html('<div class="row">'
                            +'<div class="col-md-12">'
                                +'<p>Products Not Available</p>'
                            +'</div>'
                        +'</div>');
                }
            },
            error:function(data) {
                var data = data.responseJSON;
                (data.result == "Error") ? data.result = "error" : data.result = data.result;
                generateNotif('topCenter', data.result, '<i class="fa fa-'+data.result+'" aria-hidden="true"></i> '+data.message);
            }
        });
    }

    $(document).on('click','.delete-btn',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });

    $(document).on('submit','#delete-product',function(event){
        event.preventDefault();
        var btn = $('.delete-btn').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('merchant-product-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            getProducts();
        }
    }
</script>