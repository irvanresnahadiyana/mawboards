@if($result)
    @foreach($result as $wish)
        <div class="grid-item stucked col-xs-6 col-md-3 {{($wish->hex ? 'is-wish' : 'is-product')}}">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="{{route('detail-wish-frontend',['slug' => $wish->slug])}}" class="grid-img-btn show-wish"
                        data-wishid="{{ $wish->id }}"
                        data-wishuser="{{ $wish->user_name }}"
                        data-wishuseravatar="{{ link_to_avatar($wish->image_user ? $wish->image_user : 'no_image') }}"
                        data-wishtitle="{{ $wish->title }}"
                        data-wishcategory="{{ $wish->category_name }}"
                        data-wishlinkimage="{{ get_file(@$wish->image->image_standard) }}"
                        data-wishslug="{{$wish->slug}}"
                        data-maw="{{$wish->hex ? 'maw' : 'product'}}">
                        <img class="grid-img-file" src="{{ get_file(@$wish->image->image_standard,'thumbnail') }}" alt="Image">
                    </a>
                    <ul class="grid-img-action">
                        <li>
                            <button class="btn btn-default btn-sm" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button>
                            <a href="javascript:void(0);" class="btn btn-default btn-sm wish-like-bookmark {{$wish->status_like ? 'liked' : ''}}" id="wish-like-{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}" data-id="{{$wish->id}}" data-type="like"><i class="maw-heart-o"></i></a>
                        </li>
                        <!-- <li><a href="javascript:void(0);" id="wish-bookmark-{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}" class="btn btn-default btn-sm wish-like-bookmark" data-id="{{$wish->id}}" data-type="bookmark"><i class="fa fa-bookmark"></i></a></li> -->
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top" style="background-color: {{ $wish->hex ? $wish->hex : 'black' }}">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">{{ str_limit($wish->title, 26) }}</section>
                            <!-- <section class="stucked-box-desc-top-from">From {{!$wish->hex ? $wish->user_name : 'My Circle'}}</section> -->
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> <span class="people-likes-{{ $wish->id }}">{{ number_format((@$wish->people_likes ? $wish->people_likes->count() : $wish->likes->count()), 0, ',', '.') }}</span></a></li>
                                <li><a href="#"><i class="maw-maw"></i> 0</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="{{ route('user-wishboard-frontend', $wish->user) }}" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="{{ link_to_avatar($wish->image_user ? $wish->image_user : 'no_image') }}" alt="User Avatar">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading">
                                    <a href="{{ route('user-wishboard-frontend', $wish->user) }}">{{ $wish->user_name }}</a>
                                </h4>
                                <p>
                                    @if($wish->hex)
                                        {{ $wish->category_name }} wishboard
                                    @else
                                        {{ $wish->store_name }}
                                    @endif
                                </p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    @endforeach
@endif
