@extends('layouts.frontend.master')

@section('title', 'Your Circle')

@section('header')

@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <ul>
                <li class="profile-wishboard-create">
                    <a href="{{ route('create-circle-frontend') }}">
                        <div class="profile-wishboard-list profile-wishboard-create-btn">
                            <i class="maw-add-o"></i>
                        </div>
                        <div class="profile-wishboard-text profile-wishboard-create-text">
                            Create Circle
                        </div>
                    </a>
                </li>
                @if (count($circles) > 0)
                    @foreach ($circles as $circle)
                        <li>
                            <a href="{{ route('show-circle-frontend', $circle->slug) }}">
                                <div class="profile-wishboard-list">
                                    <section class="profile-wishboard-list-box">
                                        <div class="profile-wishboard-list-big objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1486334803289-1623f249dd1e?dpr=1&auto=format&fit=crop&w=1500&h=939&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1481391145929-5bcf567d5211?dpr=1&auto=format&fit=crop&w=1500&h=1875&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1473216635433-38f7100ae658?dpr=1&auto=format&fit=crop&w=1500&h=970&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/5/unsplash-kitsune-4.jpg?dpr=1&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                    </section>
                                </div>
                                <div class="profile-wishboard-text">
                                    {{ $circle->name }}
                                    <small>{{ $circle->users()->count() }} Persons</small>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>
</div>
<!-- <div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Your have {{ $circles->total() }} circle <span class="pull-right"><a href="{{ route('create-circle-frontend') }}" title="Add Circle">Add Circle</a></span></h3>
                </div>
                <ul class="list-group">
                    @if (count($circles) > 0)
                        <?php $no = 1 ?>
                        @foreach ($circles as $circle)
                            <li class="list-group-item">
                                <p class="text-capitalize">
                                    {{ $no }}. {{ $circle->name }} <a href="{{ route('show-circle-frontend', $circle->slug) }}" title="Show friend list">{{ $circle->users()->count() }} Friends</a>
                                    <span class="pull-right">
                                        <a href="{{ route('edit-circle-frontend', $circle->slug) }}" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                        <a href="#" class="delete-circle" data-id="{{ $circle->id }}" data-name="{{ $circle->name }}" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                    </span>
                                </p>
                            </li>
                            <?php $no++ ?>
                        @endforeach
                        <li class="list-group-item">{{ $circles->links() }}</li>
                    @else
                        <li class="list-group-item">You dont't have circle!</li>
                    @endif
                </ul>
            </div>
        </div>
        <section class="col-xs-6 grid-item col-md-3 rect">
            <div class="rect-box">
                <section class="rect-user">
                    <div class="rect-user-big">
                        <section class="rect-user28">Your</section>
                        <section class="rect-user52">Circle</section>
                    </div>
                </section>
            </div>
        </section>
    </section>
</div> -->
@endsection

@section('script')
<script type="text/javascript">
    $('.delete-circle').click(function(event) {
        event.preventDefault();
        var ajax_url_delete = '{{ route("destroy-circle-frontend") }}';
        var success_url = '{{ route("index-circle-frontend") }}';
        var data = {
            id: this.getAttribute('data-id')
        }
        var name = "<strong>"+this.getAttribute('data-name')+"</strong> circle";

        confirmDelete(ajax_url_delete, success_url, data, name);
    });
</script>
@endsection
