@extends('layouts.frontend.master')

@section('title', 'Add Circle')

@section('header')
<!-- Select-2 -->
{!! Html::style('bower_components/admin-lte/plugins/select2/select2.min.css') !!}
@endsection

@section('content')
<div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form id="form-circle">
                        <div class="form-group" id="circle-name">
                            <label for="circle-input-name">Name*</label>
                            <input type="text" class="form-control" name="name" id="circle-input-name" placeholder="Name*">
                        </div>
                        <!-- <div class="form-group" id="circle-type">
                            <label for="circle-input-type">Type* <small>Default: Publish</small></label><br>
                            <label class="radio-inline">
                                <input type="radio" name="type" id="circle-input-type" value="publish" checked=""> Publish
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="type" id="circle-input-type" value="private"> Private
                            </label>
                        </div> -->
                        <div class="form-group" id="circle-friends">
                            <label for="circle-input-friends">Friends <small>(You can edit later)</small></label>
                            <select class="form-control select2" name="friends[]" multiple="">
                                @foreach($friends as $friend)
                                    <option value="{{ $friend->id }}">{{ $friend->first_name }} {{ $friend->last_name }}</option>
                                @endforeach
                            </select>
                            <div id="circle-input-friends"></div>
                        </div>
                        <p class="text-left">
                            <button type="submit" class="btn btn-primary" id="button-add" data-loading-text="Loading..." title="Add">Add Circle</button>
                            <a href="{{ route('index-circle-frontend') }}" class="btn btn-danger" title="Cancel">Cancel</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <section class="col-xs-6 grid-item col-md-3 rect">
            <div class="rect-box">
                <section class="rect-user">
                    <div class="rect-user-big">
                        <section class="rect-user28">Add new</section>
                        <section class="rect-user52">Circle</section>
                    </div>
                </section>
            </div>
        </section>
        <section class="col-xs-6 grid-item col-md-3 rect">
            <section class="rect-box">
                <div class="rect-user dark">
                    <div class="rect-user-big">
                        <section class="rect-user56">{{ date('d.m.Y') }}</section>
                    </div>
                </div>
            </section>
        </section>
    </section>
</div>
@endsection

@section('script')
<!-- Select-2 -->
{!! Html::script('bower_components/admin-lte/plugins/select2/select2.min.js') !!}
<script type="text/javascript">
    $('.select2').select2({
        placeholder: "Select",
        allowClear: true
    });

    $('#button-add').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("store-circle-frontend") }}',
            data: $('#form-circle').serialize(),
            dataType: 'json',
            success:function(data) {
                window.location = "{{ route('index-circle-frontend') }}";
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('#circle-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#circle-input-'+key));
                });
                $btn.button('reset');
            }
        });
    });
</script>
@endsection
