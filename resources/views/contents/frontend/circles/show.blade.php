@extends('layouts.frontend.master')

@section('title', $circle->name.' Circle')

@section('header')
<style type="text/css">
    .profile-wishboard-list-box > div {
        height: 100%;
    }
    .profile-wishboard-body {
        margin-top: 20px;
    }
    .title-circle-detail {
        font-family: "HelveticaNeueLTPro";
        font-size: 18px;
        font-weight: bold;
        color: #333;
        margin-bottom: 10px;
    }
</style>
@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <div class="title-circle-detail">
                {{title_case($circle->name)}}
            </div>
            <ul>
                <li class="profile-wishboard-create">
                    <a href="{{ route('edit-circle-frontend', $circle->slug) }}">
                        <div class="profile-wishboard-list profile-wishboard-create-btn">
                            <i class="maw-add-o"></i>
                        </div>
                        <div class="profile-wishboard-text profile-wishboard-create-text">
                            Add Friend on Circle
                        </div>
                    </a>
                </li>
                @if (count($circle_friends) > 0)
                    @foreach ($circle_friends as $circle_friend)
                        <li>
                            <a href="{{ route('user-wishboard-frontend', $circle_friend) }}">
                                <div class="profile-wishboard-list">
                                    <section class="profile-wishboard-list-box">
                                        <div class="profile-wishboard-list-big objectfit-container-cover">
                                            <img class="objectfit-img" src="{{ link_to_avatar(@$user ? @$user->images->first()->image_thumb : @user_info()->images->first()->image_thumb) }}" alt="">
                                        </div>
                                    </section>
                                </div>
                                <div class="profile-wishboard-text">
                                    {{ $circle_friend->first_name.' '.$circle_friend->last_name }}
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.delete-circle').click(function(event) {
        event.preventDefault();
        var ajax_url_delete = '{{ route("destroy-circle-frontend") }}';
        var success_url = '{{ route("index-circle-frontend") }}';
        var data = {
            id: this.getAttribute('data-id')
        }
        var name = "<strong>"+this.getAttribute('data-name')+"</strong> circle";

        confirmDelete(ajax_url_delete, success_url, data, name);
    });
</script>
@endsection
