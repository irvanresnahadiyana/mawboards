@extends('layouts.frontend.master')

@section('title', 'My Mawboards')

@section('header')
<style type="text/css">
    input[type="file"] {
        opacity:0;
        position:absolute;
        top:0;
        left:0;
        width:90%;
        height:90%;
        cursor: pointer;
    }

    #change-avatar {
        opacity:1;
    }

    #change-avatar:hover {
        opacity:0.5;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <form id="form-avatar">
                        <input type="hidden" name="email" value="{{ user_info('email') }}">
                        <input type="hidden" name="first_name" value="{{ user_info('first_name') }}">
                        <input type="hidden" name="last_name" value="{{ user_info('last_name') }}">
                        <input type="hidden" name="gender" value="{{ user_info('gender') }}">
                        <input type="hidden" name="birthday" value="{{ user_info('birthday') }}">
                        <input type="hidden" name="live_in" value="{{ user_info('live_in') }}">
                        <input type="hidden" name="bio" value="{{ user_info('bio') }}">
                        <div class="form-group" id="change-avatar">
                            <input type="file" onchange="readURL(this);" name="avatar" id="change-avatar-input" title="Click for Change Avatar">
                            <img src="{{ link_to_avatar(@user_info()->images->first()->image_thumb) }}" class="img-responsive img-preview" alt="User Avatar">
                        </div>
                    </form>
                    <i>{{ trans('general.click_change_avatar') }}</i>
                    <button type="button" class="btn btn-default" id="update-avatar" data-loading-text="Loading..."><i class="fa fa-refresh" aria-hidden="true"></i> Save Avatar</button>
                </div>
                <div class="col-md-9">
                    <blockquote>
                        @if(user_info('bio') == null)
                            Your bio.
                        @else
                            @if(strlen(user_info('bio')) > 200)
                                {!! substr(user_info('bio'), 0, 200) !!} ..
                            @else
                                {!! user_info('bio') !!}
                            @endif
                        @endif
                    </blockquote>
                    <a href="{{ route('edit-profile-frontend') }}" type="button" class="btn btn-default" data-text="Edit Profile"><span>Edit Profile</span></a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ user_info('first_name') }}<br>{{ user_info('last_name') }}</h2>
                </div>
                <div class="col-md-8">
                    <p>{{ user_info('live_in') }}</p>
                </div>
                <div class="col-md-2">
                    <p><span><a href="#" title="Share Wishboard">Share Wishboard</a></span></p>
                </div>
                <div class="col-md-2">
                    <p><span><a href="#"><a href="#" title="Find Friends">Find Friends</a></span></p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2">
                    <b>{{ $news_total }}</b><br>News
                </div>
                <div class="col-md-2">
                    <a href="{{ route('profile-message') }}"><b>{{ $message_total }}</b><br>Message</a>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('index-wishboard-frontend') }}"><b>{{ $maw_total }}</b><br>MAW</a>
                </div>
                <div class="col-md-2">
                    <b>{{ $like_total }}</b><br>Likes
                </div>
                <div class="col-md-2">
                    <b>{{ $follower_total }}</b><br>Follower
                </div>
                <div class="col-md-2">
                    <b>{{ $following_total }}</b><br>Following
                </div>
            </div>
        </div>
    </div>
    <section class="col-xs-6 grid-item col-md-6 rect">
        <section class="rect-box">
            <div class="rect-user gray">
                <div class="rect-user-big">
                    @if ($wishes_random)
                        <img class="grid-img-file" src="{{ get_file(@$wishes_random->image->image_standard, 'thumbnail') }}" alt="Wish Image">
                    @else
                        <h3 align="center"><a href="{{ route('create-wish-frontend') }}" title="Make a wish">Make a wish</a></h3>
                    @endif
                </div>
            </div>
        </section>
    </section>
    <section class="col-xs-6 grid-item col-md-6 rect">
        <section class="rect-box">
            <div class="rect-user gray">
                <div class="rect-user-big">
                    @if (count($wishes) >= 6)
                        <img class="grid-img-file" src="{{ get_file(@$wishes_random->image->image_standard, 'thumbnail') }}" alt="Wish Image">
                    @else
                        <h3 align="center"><a href="{{ route('create-wish-frontend') }}" title="Make a wish">Make a wish</a></h3>
                    @endif
                </div>
            </div>
        </section>
    </section>
    @if (count($wishes) > 0)
        @foreach ($wishes as $wish)
            <section class="col-xs-3 grid-item col-md-2 rect">
                <section class="rect-box">
                    <div class="rect-user">
                        <div class="rect-user-big">
                            <img class="grid-img-file" src="{{ get_file(@$wish->image->image_standard, 'thumbnail') }}" alt="Wish Image">
                        </div>
                    </div>
                </section>
            </section>
        @endforeach
        @if (count($wishes) <= 6)
            @for ($i = count($wishes); $i < 6; $i++)
                <section class="col-xs-3 grid-item col-md-2 rect">
                    <section class="rect-box">
                        <div class="rect-user gray">
                            <div class="rect-user-big">
                                <h3 align="center"><a href="{{ route('create-wish-frontend') }}" title="Make a wish">Make a wish</a></h3>
                            </div>
                        </div>
                    </section>
                </section>
            @endfor
        @endif
    @else
        @for ($x = 1; $x <= 6; $x++)
            <section class="col-xs-3 grid-item col-md-2 rect">
                <section class="rect-box">
                    <div class="rect-user gray">
                        <div class="rect-user-big">
                            <h3 align="center"><a href="{{ route('create-wish-frontend') }}" title="Make a wish">Make a wish</a></h3>
                        </div>
                    </div>
                </section>
            </section>
        @endfor
    @endif
    <section class="col-md-6">
        <div class="modal-box">
            <section class="modal-box-header sea">
                <div class="modal-box-header-title">
                    <p><strong>My category Wishboards</strong><span class="pull-right"><a href="#">Edit</a></span></p>
                </div>
            </section>
        </div>
    </section>
    <section class="col-md-6">
        <div class="modal-box">
            <section class="modal-box-header red">
                <div class="modal-box-header-title">
                    <p><strong>My category Wishboards</strong><span class="pull-right"><a href="#">Edit</a></span></p>
                </div>
            </section>
        </div>
    </section>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $("#update-avatar").hide();

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            $("#update-avatar").show();
        }
    }

    $('#update-avatar').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("update-profile-frontend") }}',
            data: new FormData($("#form-avatar")[0]),
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            success:function(data) {
                location.reload();
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('<span class="help-block"><font color="red">'+val+'</font></span>').insertAfter($('#update-avatar'));
                });
                $btn.button('reset');
            }
        });
    });
</script>
@endsection
