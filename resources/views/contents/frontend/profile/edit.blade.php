@extends('layouts.frontend.master')

@section('title', 'Edit Profile')

@section('header')
<!-- wysihtml5 -->
{!! Html::style('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
<style type="text/css">
    .btn-link {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form id="form-edit">
                        <div class="form-group" id="edit-email">
                            <label for="edit-input-first_name">Email*</label>
                            <input type="email" class="form-control" name="email" id="edit-input-email" value="{{ user_info('email') }}" placeholder="Email Address*" readonly="">
                        </div>
                        <div class="form-group" id="edit-first_name">
                            <label for="edit-input-first_name">First Name*</label>
                            <input type="text" class="form-control" name="first_name" id="edit-input-first_name" value="{{ user_info('first_name') }}" placeholder="First Name*">
                        </div>
                        <div class="form-group" id="edit-last_name">
                            <label for="edit-input-first_name">Last Name*</label>
                            <input type="text" class="form-control" name="last_name" id="edit-input-last_name" value="{{ user_info('last_name') }}" placeholder="Last Name*">
                        </div>
                        <div class="form-group" id="edit-gender">
                            <label for="edit-input-first_name">Gender*</label><br>
                            @if (user_info('gender') == 'male')
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="male" checked="checked"> <i class="fa fa-male fa-2x" aria-hidden="true"></i> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="female"> <i class="fa fa-female fa-2x" aria-hidden="true"></i> Female
                                </label>
                            @else
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="male"> <i class="fa fa-male fa-2x" aria-hidden="true"></i> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="female" checked="checked"> <i class="fa fa-female fa-2x" aria-hidden="true"></i> Female
                                </label>
                            @endif
                            <div id="edit-input-gender"></div>
                        </div>
                        <div class="form-group" id="edit-birthday">
                            <label for="edit-input-first_name">Birthday*</label>
                            <div class="input-group date" id="edit-input-birthday">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right" name="birthday" value="{{ date('d F Y', strtotime(user_info('birthday'))) }}" placeholder="Birthday*">
                            </div>
                        </div>
                        <div class="form-group" id="edit-live_in">
                            <label for="edit-input-live_in">Live in*</label>
                            <input type="text" class="form-control" name="live_in" id="edit-input-live_in" value="{{ user_info('live_in') }}" placeholder="Live in*">
                        </div>
                        <div class="form-group" id="edit-bio">
                            <label for="edit-input-bio">Bio</label>
                            <textarea class="form-control wysihtml5" name="bio" placeholder="Bio">{!! user_info('bio') !!}</textarea>
                            <div id="edit-input-bio"></div>
                        </div>
                        <p class="text-left">
                            <button type="submit" class="btn btn-primary" id="button-edit" data-loading-text="Loading..." title="Update">Update</button>
                            <a href="{{ route('index-profile-frontend') }}" class="btn btn-danger" title="Cancel">Cancel</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <section class="col-xs-6 grid-item col-md-3 rect">
            <div class="rect-box">
                <section class="rect-user">
                    <div class="rect-user-big">
                        <section class="rect-user52">Edit</section>
                        <section class="rect-user24">your</section>
                        <section class="rect-user52">Profile</section>
                    </div>
                </section>
            </div>
        </section>
        <section class="col-xs-6 grid-item col-md-3 rect">
            <div class="rect-box">
                <section class="rect-user gray">
                    <div class="rect-user-big">
                        <section class="rect-user28">Change</section>
                        <section class="rect-user52">Password</section>
                        <button class="btn btn-link" data-remodal-target="changePassModal" id="btn-change-pass">
                            <section class="rect-user24">Click here!</section>
                        </button>
                    </div>
                </section>
            </div>
        </section>
    </section>
</div>
<!-- Change Password Modal -->
<div class="remodal remodal-login" data-remodal-id="changePassModal">
    <div class="remodal-box">
        <button data-remodal-action="close" class="remodal-close"></button>
        <section class="remodal-header">
            <h3>{!! trans('auth.change_password') !!}</h3>
        </section>
        <section class="remodal-body">
            <form class="remodal-form" id="form-change-pass">
                <div id="alert-info-change-pass-modal">
                    @if (session('status'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                {!! Form::hidden('login', user_info('email')) !!}
                {!! Form::hidden('code', null, ['id' => 'code']) !!}
                <div class="form-group" id="change-password">
                    <label for="change-input-password">New Password*</label>
                    <input type="password" class="form-control" name="password" id="change-input-password" placeholder="New Password*">
                </div>
                <div class="form-group" id="change-password_confirmation">
                    <label for="change-input-password_confirmation">Confirm New Password*</label>
                    <input type="password" class="form-control" name="password_confirmation" id="change-input-password_confirmation" placeholder="Confirm New Password*">
                </div>
                <button type="submit" class="btn btn-3" id="button-change-pass" data-loading-text="Loading...">Submit</button>
                <button type="button" class="btn btn-3" data-remodal-action="close">Cancel</button>
            </form>
        </section>
    </div>
</div>
@endsection

@section('script')
<!-- wysihtml5 -->
{!! Html::script('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<script type="text/javascript">
    $(".wysihtml5").wysihtml5();

    $('#button-edit').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("update-profile-frontend") }}',
            data: $('#form-edit').serialize(),
            dataType: 'json',
            success:function(data) {
                window.location = "{{ route('index-profile-frontend') }}";
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('#edit-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#edit-input-'+key));
                });
                $btn.button('reset');
            }
        });
    });

    $('#btn-change-pass').click(function(event) {
        event.preventDefault();
        $.ajax({
            type:'get',
            url:'{{ route("auth-reminder-token-get") }}',
            success:function(data) {
                $('#code').val(data.code);
            },
            error:function(data) {
                console.log(data);
            }
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GMAPS_API_KEY', 'AIzaSyAq6Vp9Wco37nB0Z_wl5rGc9EAnVaEttsI') }}&libraries=places"></script>
<script>
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('edit-input-live_in'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;

            document.getElementById('edit-input-live_in').value = place.formatted_address;
        });
    });
</script>
@endsection
