@extends('layouts.frontend.master')

@section('title', 'Shop')

@section('header')

@endsection

@section('content')
<div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-6">
            <section class="big-box">
                <div class="grid-img objectfit-container-cover">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1429081172764-c0ee67ab9afd?dpr=1&auto=format&fit=crop&w=1500&h=927&q=80&cs=tinysrgb&crop=" alt=""></a>
                </div>
                <div class="bigBadge bigBadge-green">
                    <span>Popular Wish</span>
                </div>
                <div class="big-box-desc">
                    <section class="big-box-desc-title">Karen Millen Clutch</section>
                    <section class="big-box-desc-price">Selena Gomez</section>
                </div>
            </section>
        </div>
        <div class="grid-item big col-xs-6">
            <section class="big-box">
                <div class="grid-img objectfit-container-cover">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1472461936147-645e2f311a2b?dpr=1&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" alt=""></a>
                </div>
                <div class="bigBadge bigBadge-red">
                    <span>Best Seller</span>
                </div>
                <div class="big-box-desc">
                    <section class="big-box-desc-title">Blue Metallic Leather Watch</section>
                    <section class="big-box-desc-price">Smartwatch Shop</section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1422479763403-3aa672b25e0e?dpr=1&auto=format&fit=crop&w=1500&h=1001&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top sea">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top orange">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1466804818781-6dc2644db8a2?dpr=1&auto=format&fit=crop&w=1500&h=2256&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top gray">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1467034704522-7e3bd5bf1a53?dpr=1&auto=format&fit=crop&w=1500&h=1125&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top lime">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Document Clutch</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My valentine wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1469274817686-dd4d93da3b03?dpr=1&auto=format&fit=crop&w=1500&h=2250&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top pink">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1422479763403-3aa672b25e0e?dpr=1&auto=format&fit=crop&w=1500&h=1001&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top sea">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1472073834672-adb08feda350?dpr=1&auto=format&fit=crop&w=1500&h=2247&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top orange">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1466804818781-6dc2644db8a2?dpr=1&auto=format&fit=crop&w=1500&h=2256&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top gray">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1467034704522-7e3bd5bf1a53?dpr=1&auto=format&fit=crop&w=1500&h=1125&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top lime">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Document Clutch</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My valentine wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        <div class="grid-item stucked col-xs-6 col-md-3">
            <section class="stucked-box white">
                <div class="grid-img">
                    <a href="#" class="grid-img-btn"><img class="grid-img-file" src="https://images.unsplash.com/photo-1469274817686-dd4d93da3b03?dpr=1&auto=format&fit=crop&w=1500&h=2250&q=80&cs=tinysrgb&crop=" alt=""></a>
                    <ul class="grid-img-action">
                        <li><button class="btn btn-default btn-sm"  title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button><a href="#" class="btn btn-default btn-sm"><i class="maw-heart-o"></i></a></li>
                        <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                    </ul>
                </div>
                <div class="stucked-box-desc">
                    <section class="stucked-box-desc-top pink">
                        <div class="stucked-box-desc-top-text">
                            <section class="stucked-box-desc-top-name">Paul Bistro</section>
                        </div>
                        <div class="stucked-box-desc-top-badges">
                            <ul>
                                <li><a href="#"><i class="maw-heart-o"></i> 14</a></li>
                                <li><a href="#"><i class="maw-maw"></i> 7</a></li>
                            </ul>
                        </div>
                    </section>
                    <section class="stucked-box-desc-bottom">
                        <div class="media">
                            <section class="media-left media-middle">
                                <a href="#" class="objectfit-container-cover">
                                    <img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">
                                </a>
                            </section>
                            <section class="media-body media-middle">
                                <h4 class="media-heading"><a href="#">Jhon Wick</a></h4>
                                <p>My birthday wishboard</p>
                            </section>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </section>
</div>
@endsection

@section('script')

@endsection
