@extends('layouts.frontend.master')

@section('title', $title)

@section('header')

@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <ul>
                @if (!$user)
                    <li class="profile-wishboard-create">
                        <a href="#" data-remodal-target="create-wishboard-modal">
                            <div class="profile-wishboard-list profile-wishboard-create-btn">
                                <i class="maw-add-o"></i>
                            </div>
                            <div class="profile-wishboard-text profile-wishboard-create-text">
                                Create Wish Board
                            </div>
                        </a>
                    </li>
                @endif
                @if (count($wishboards) > 0)
                    @foreach ($wishboards as $wishboard)
                        <li>
                            <a href="{{ route('show-wishboard-frontend', [$wishboard->slug]) }}">
                                <div class="profile-wishboard-list">
                                    <section class="profile-wishboard-list-box">
                                        <div class="profile-wishboard-list-big objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1486334803289-1623f249dd1e?dpr=1&auto=format&fit=crop&w=1500&h=939&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1481391145929-5bcf567d5211?dpr=1&auto=format&fit=crop&w=1500&h=1875&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1473216635433-38f7100ae658?dpr=1&auto=format&fit=crop&w=1500&h=970&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/5/unsplash-kitsune-4.jpg?dpr=1&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-color" style="background-color: {{ $wishboard->color_hex ? $wishboard->color_hex : 'white' }};"></div>
                                    </section>
                                </div>
                                <div class="profile-wishboard-text">
                                    {{ $wishboard->name }}
                                    <small>{{ $wishboard->total }} Wishes</small>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>
</div>
<div class="remodal" data-remodal-id="create-wishboard-modal">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Create Wishboard</h1>
    <form id="form-add-wishboard">
        <div class="form-group" id="post-name">
            <label for="post-input-name">Your wishboard name*</label>
            <input type="text" class="form-control" name="name" id="post-input-name" placeholder="Your wishboard name*">
        </div>
        <p>
            <br><i>You can create more wishboard</i>
            <br><i>Your wishboards enter into Custom category with Gray color</i>
        </p>
        <br>
        <button data-remodal-action="cancel" class="btn btn-danger">Cancel</button>
        <button class="btn btn-success button-post-wishboard">Create Wishboard</button>
    </form>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.button-post-wishboard').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        var btn_type = event.target.value;
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("store-wishboard-frontend") }}',
            data: new FormData($("#form-add-wishboard")[0]),
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            success:function(data) {
                window.location = "{{ route('index-wishboard-frontend') }}";
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('#post-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#post-input-'+key));
                });
                $btn.button('reset');
            }
        });
    });

    $('#invite-friend').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            type:'post',
            url:'{{ route("store-friend-frontend") }}',
            data: {
                type: 'befriend',
                sender_user_id: {{ user_info('id') }},
                recipient_user_id: {{ $user ? $user->id : 0 }},
            },
            dataType: 'json',
            success:function(data) {
                location.reload();
            },
            error:function(data) {
                console.log(data);
                $btn.button('reset');
            }
        });
    });

    function removeFriendFunction(sender_user_id, recipient_user_id, action_type) {
        var $btn = $('#remove-friend').button('loading');
        $.ajax({
            type:'post',
            url:'{{ route("destroy-friend-frontend") }}',
            data: {
                type: action_type,
                sender_user_id: sender_user_id,
                recipient_user_id: recipient_user_id,
            },
            dataType: 'json',
            success:function(data) {
                location.reload();
            },
            error:function(data) {
                console.log(data);
                $btn.button('reset');
            }
        });
    }

    function responseFriendFunction(sender_user_id, recipient_user_id, action_type) {
        if (action_type == 'acceptFriendRequest') {
            var $btn = $('#accept-friend').button('loading');
        } else if (action_type == 'denyFriendRequest') {
            var $btn = $('#denied-friend').button('loading');
        } else if (action_type == 'blockFriend') {
            var $btn = $('#block-friend').button('loading');
        } else {
            var $btn = $('#unblock-friend').button('loading');
        }

        $.ajax({
            type:'post',
            url:'{{ route("update-friend-frontend") }}',
            data: {
                type: action_type,
                sender_user_id: sender_user_id,
                recipient_user_id: recipient_user_id,
            },
            dataType: 'json',
            success:function(data) {
                location.reload();
            },
            error:function(data) {
                console.log(data);
                $btn.button('reset');
            }
        });
    }
</script>
@endsection
