@extends('layouts.frontend.master')

@section('title', 'Create Wishboard')

@section('header')
<!-- Select-2 -->
{!! Html::style('bower_components/admin-lte/plugins/select2/select2.min.css') !!}
<!-- wysihtml5 -->
{!! Html::style('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('content')
<div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-6">
            <div class="panel panel-warning">
                <div class="panel-body">
                    <form id="form-add-wish">
                        <div class="form-group" id="post-title">
                            <label for="post-input-title">Title*</label>
                            <input type="text" class="form-control" name="title" id="post-input-title" placeholder="Title*">
                        </div>
                        <p>
                            <button type="submit" class="btn btn-warning button-post-wish" value="post" data-loading-text="Loading..." title="Create Wishboard">Create Wishboard</button>
                            <a href="{{ route('wishboards.index') }}" class="btn btn-danger" title="Cancel">Cancel</a>
                        </p>
                        <p>
                            <br><i>You can create more wishboard</i>
                            <br><i>Your wishboards enter into Custom category with gray color</i>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<!-- Select-2 -->
{!! Html::script('bower_components/admin-lte/plugins/select2/select2.min.js') !!}
<!-- wysihtml5 -->
{!! Html::script('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<!-- Laravel Javascript Validation -->
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Frontend\StoreWish', '#form-add-wish'); !!}
<script type="text/javascript">
    $('.select2').select2({
        placeholder: "Choose",
        allowClear: true
    });

    $(".wysihtml5").wysihtml5();

    $('.button-post-wish').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        var btn_type = event.target.value;
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("store-wish-frontend") }}',
            data: new FormData($("#form-add-wish")[0]),
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            success:function(data) {
                // redirect by type button if just post or post and see wish
                if (btn_type == 'post') {
                    window.location = "{{ route('index-wish-frontend') }}";
                } else {
                    window.location = "{{ route('index-wish-frontend') }}";
                }
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('#post-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#post-input-'+key));
                });
                $btn.button('reset');
            }
        });
    });
</script>
@endsection
