@extends('layouts.frontend.master_static')

@section('title', $title)

@section('content')
<div class="about-content-box">
    <h3 class="about-content-title">{{ $title }}</h3>
    <p class="about-content-text">{!! $content !!}</p>
    <a href="#" class="about-content-btn btn-3">Discover</a>
</div>
@endsection
