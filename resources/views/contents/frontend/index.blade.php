@extends('layouts.frontend.master')

@section('title', 'Home')

@section('header')

@endsection

@section('content')
<div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        @if($popularWish)
            <div class="grid-item big col-xs-6">
                <section class="big-box">
                    <div class="grid-img objectfit-container-cover">
                        <a href="{{route('detail-wish-frontend',['slug' => $popularWish->slug])}}" class="grid-img-btn show-wish" data-wishid="{{ $popularWish->id }}" data-wishslug="{{$popularWish->slug}}" data-maw="{{$popularWish->hex ? 'maw' : 'product'}}">
                            <img class="grid-img-file objectfit-img" src="{{get_file(@$popularWish->images()->where('is_primary',true)->first()->image_standard)}}" alt="">
                        </a>
                    </div>
                    <div class="bigBadge bigBadge-green">
                        <span>Popular Wish</span>
                    </div>
                    <div class="big-box-desc">
                        <section class="big-box-desc-title">{{$popularWish->title}}</section>
                        <section class="big-box-desc-price">{{$popularWish->user_name}}</section>
                    </div>
                </section>
            </div>
        @else
            <div class="grid-item big col-xs-6">
                <section class="big-box">
                    <div class="grid-img objectfit-container-cover">
                        <a href="{{ route('shop') }}" class="grid-img-btn show-wish" data-maw='product'>
                            <img class="grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1429081172764-c0ee67ab9afd?dpr=1&auto=format&fit=crop&w=1500&h=927&q=80&cs=tinysrgb&crop=" alt="">
                        </a>
                    </div>
                    <div class="bigBadge bigBadge-green">
                        <span>Popular Wish</span>
                    </div>
                    <div class="big-box-desc">
                        <section class="big-box-desc-title">Karen Millen Clutch</section>
                        <section class="big-box-desc-price">Selena Gomez</section>
                    </div>
                </section>
            </div>
        @endif
        @if($popularProduct)
            <div class="grid-item big col-xs-6">
                <section class="big-box">
                    <div class="grid-img objectfit-container-cover">
                        <a href="{{ route('shop') }}" class="grid-img-btn show-wish" data-wishid="{{ $popularProduct->id }}" data-wishslug="{{ $popularProduct->slug }}" data-maw="{{ $popularProduct->hex ? 'maw' : 'product' }}">
                        <!-- <a href="{{route('detail-wish-frontend',['slug' => $popularProduct->slug])}}" class="grid-img-btn show-wish" data-wishid="{{ $popularProduct->id }}" data-wishslug="{{$popularProduct->slug}}" data-maw="{{$popularProduct->hex ? 'maw' : 'product'}}"> -->
                            <img class="grid-img-file objectfit-img" src="{{get_file(@$popularProduct->images()->where('is_primary',true)->first()->image_standard)}}" alt="">
                        </a>
                    </div>
                    <div class="bigBadge bigBadge-red">
                        <span>Best Seller</span>
                    </div>
                    <div class="big-box-desc">
                        <section class="big-box-desc-title">{{$popularProduct->title}}</section>
                        <section class="big-box-desc-price">{{currency($popularProduct->price)}}</section>
                    </div>
                </section>
            </div>
        @else
            <div class="grid-item big col-xs-6">
                <section class="big-box">
                    <div class="grid-img objectfit-container-cover">
                        <a href="{{ route('shop') }}" class="grid-img-btn"><img class="grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1472461936147-645e2f311a2b?dpr=1&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" alt=""></a>
                    </div>
                    <div class="bigBadge bigBadge-red">
                        <span>Best Seller</span>
                    </div>
                    <div class="big-box-desc">
                        <section class="big-box-desc-title">Blue Metallic Leather Watch</section>
                        <section class="big-box-desc-price">Smartwatch Shop</section>
                    </div>
                </section>
            </div>
        @endif
        <!-- @if (user_info())
            <div class="grid-item big col-xs-6">
                <section class="big-box white">
                    <div class="big-user">
                        <section class="big-user-avatar col-xs-6">
                            <div class="grid-img objectfit-container-cover">
                                <img class="grid-img-file objectfit-img" src="{{ link_to_avatar(@user_info()->images->first()->image_thumb) }}" alt="User Avatar">
                            </div>
                        </section>
                        <section class="big-user-detail col-xs-6">
                            <div class="rect-box">
                                <section class="big-user-detail-name">{{ user_info('full_name') }}</section>
                                <section class="big-user-detail-location">{{ user_info('live_in') }}</section>
                                <section>
                                    <br>
                                    <a href="{{ route('edit-profile-frontend') }}" class="btn btn-default" title="Edit Profile">Edit Profile</a>
                                </section>
                            </div>
                        </section>
                        <section class="big-user-notif col-xs-6">
                            <div class="rect-box">
                                <ul>
                                    <li><a href="#">You <span class="badge badge-baloon">14</span></a></li>
                                    <li><a href="#">News <span class="badge badge-baloon">14</span></a></li>
                                    <li><a href="{{ route('profile-message') }}">Messages <span class="badge badge-baloon">0</span></a></li>
                                </ul>
                            </div>
                        </section>
                        <section class="big-user-notif col-xs-6">
                            <div class="rect-box">
                                <a href="{{ route('index-wishboard-frontend') }}" class="btn btn-1" title="My Wishboard">My Wishboard</a>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
            @if(count($friends_birthday) > 0)
                <section class="col-xs-6 grid-item col-md-3 rect">
                    <section class="rect-box">
                        <div class="rect-user dark">
                            <div class="rect-user-big">
                                <section class="rect-user56">{{ date('d.m.Y') }}</section>
                            </div>
                        </div>
                    </section>
                </section>
                <section class="col-xs-6 grid-item col-md-3 rect">
                    <div class="rect-box">
                        <section class="rect-user">
                            <div class="rect-user-big">
                                <section class="rect-user28">This Coming</section>
                                <section class="rect-user28">Birthday</section>
                                <section class="rect-user52">{{ $friends_birthday->first_name }}</section>
                                <section class="rect-user24">{{ $friends_birthday->last_name }}</section>
                                <section class="rect-user52">{{ date("M d", strtotime($friends_birthday->birthday)) }}</section>
                            </div>
                        </section>
                    </div>
                </section>
            @else
                <section class="col-xs-6 grid-item col-md-3 rect">
                    <section class="rect-box">
                        <div class="rect-user gray">
                            <div class="rect-user-big">
                                <section class="rect-user28">Add your</section>
                                <section class="rect-user28">friend and</section>
                                <section class="rect-user52">family</section>
                                <section class="rect-user24">in your</section>
                                <section class="rect-user52">Circle</section>
                            </div>
                        </div>
                    </section>
                </section>
                <section class="col-xs-6 grid-item col-md-3 rect">
                    <div class="rect-box">
                        <section class="rect-user">
                            <div class="rect-user-big">
                                <section class="rect-user28">This Coming</section>
                                <section class="rect-user28">Birthday</section>
                                <section class="rect-user28">-</section>
                            </div>
                        </section>
                    </div>
                </section>
            @endif
        @endif -->
        <div class="ajax_scroll">
            <!-- product list -->
            @if (count($products) > 0)
                @include('contents.frontend.wish_list', ['result' => $products])
            @endif
            <!-- wish list -->
            @if (count($wishes) > 0)
                @include('contents.frontend.wish_list', ['result' => $wishes])
            @endif
        </div>
    </section>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row text-center" id="inifiniteLoader" style="position: fixed; bottom: 10px;left: 50%;opacity: 0.5;display: none;">
            <i class="fa fa-spinner fa-spin fa-2x"></i>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var count = 2;

    $(window).on('load', function () {
        $(document).ready(function ($) {
            var api = base_url+"/get-wishes";
            $('div#inifiniteLoader').hide();
        });
    });

    $(window).scroll(function () {
        if (x == 2)
            finish = false;
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            x++;
            if (finish) {
                return false;
            } else {
                loadFeed(count);
            }
            count++;
        }
    });

    $(document).ready(function() {
                $('.grid-img-action li button.btn').each(function() {

                    $('.grid-img-action li button.btn').popover({
                        animation: false,
                        container: 'body',
                        placement: function (context, source) {
                            var body = $('body').height();
                            var position = $(source).offset();
                            var val = body - position.top;

                            if (val < 500){
                                return "top";
                            }

                            return "bottom";
                        },
                        template: '<div class="popover" role="tooltip">'
                                + '<div class="arrow"></div>'
                                // + '<section class="popover-header">'
                                //     + '<div class="form-group">'
                                //         + '<input type="text" class="form-control" placeholder="Search for name or email">'
                                //         + '<span><i class="fa fa-search"></i></span>'
                                //     + '</div>'
                                // + '</section>'
                                // + '<section class="popover-body" id="popover-scroll">'
                                //     + '<button class="media" type="button">'
                                //         + '<section class="media-left media-middle">'
                                //             + '<span href="#" class="objectfit-container-cover">'
                                //                 + '<img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">'
                                //             + '</span>'
                                //         + '</section>'
                                //     + '</button>'
                                // + '</section>'
                                + '<section class="popover-footer">'
                                    + '<div class="popover-share-text">Share this Wish Board:</div>'
                                    + '<ul class="popover-share">'
                                        + '<li><button type="button" class="share-facebook"><i class="fa fa-facebook"></i></button></li>'
                                        + '<li><button type="button" class="share-twitter"><i class="fa fa-twitter"></i></button></li>'
                                        + '<li><button type="button" class="share-google"><i class="fa fa-google-plus"></i></button></li>'
                                        + '<li><button type="button" class="share-instagram"><i class="fa fa-instagram"></i></button></li>'
                                        + '<li><button type="button" class="share-instagram"><i class="fa fa-link"></i></button></li>'
                                    + '</ul>'
                                +'</section>'
                            + '</div>'
                    });

                    $(this).on('click', function (e) {

                        if ($(this).parent().parent().hasClass('active')) {
                            $(this).popover('hide');
                            $(this).parent().parent().removeClass('active');
                        } else {
                            e.stopPropagation();
                            $('.grid-img-action li button.btn').not(this).popover('hide');
                            $('.grid-img-action li button.btn').not(this).parent().parent().removeClass('active');
                            $(this).popover('show');
                            $(this).parent().parent().addClass('active');
                            var popScroll = document.getElementById('popover-scroll');
                            Ps.initialize(popScroll);
                        }
                    });

                    $('body').on('click', function(e) {
                        $('.grid-img-action li button.btn').popover('hide');
                        $('.grid-img-action').removeClass('active');
                    });
                });

                // var inst = $('[data-remodal-id=modal]').remodal();

                // $('.grid-img-btn').on('click', function(e) {
                //     e.preventDefault();
                //     e.stopPropagation();
                //     inst.open();
                // });

                // $('.modal-detail').on('click', function() {
                //     inst.close();
                // });

                // $('.modal-box').on('click', function(e) {
                //     // prevent modal close
                //     e.preventDefault();
                //     e.stopPropagation();
                // });

                $(document).on('opening', '.remodal', function () {
                    // $modalWhisboard.imagesLoaded().progress( function() {
                    //     $modalWhisboard.masonry('layout');
                    //     // Ps.initialize(scrollBar);
                    // });

                    // $modalGrid.imagesLoaded().progress( function() {
                    //     $modalGrid.masonry('layout');
                    // });

                    // $modalGrid.masonry('layout');

                    finish = true;
                });

                $(document).on('closing', '.remodal,.modal', function () {
                    x = 1;
                    if (!$(this).hasClass('remodal-login'))
                        history.pushState(null, null, currentURL);

                    // $modalGrid.masonry( 'reloadItems');
                    // $modalGrid.masonry('layout');
                });

                // $(document).on('opened', '#modal-wish-detail', function (event) {
                //     var wishSlug = buttonWish.data('wishslug');
                //     var wishid = buttonWish.data('wishid'); // Extract info from data-* attributes
                //     var wishuser = buttonWish.data('wishuser');
                //     var wishuseravatar = buttonWish.data('wishuseravatar');
                //     var wishtitle = buttonWish.data('wishtitle');
                //     var wishcategory = buttonWish.data('wishcategory');
                //     var wishlinkimage = buttonWish.data('wishlinkimage');

                //     // Update the modal's content.
                //     var modal = $(this);
                //     modal.find('#modal-wish-title').text(wishtitle);
                //     modal.find('#modal-wish-name-category').text(wishuser+' '+wishcategory+' Wishboard');
                //     modal.find('#modal-wish-image').attr("src", wishlinkimage);
                //     modal.find('#modal-wish-user').text(wishuser);
                //     modal.find('#modal-wish-user-avatar').attr("src", wishuseravatar);
                //     modal.find('#modal-wish-user-category').text(wishcategory+' Wishboard');
                //     history.pushState(null, null, '/maw/'+wishSlug);
                // });

                $('#modal-see-more').on('click', function() {
                    var a = $(this);
                    if ($(a).hasClass('less-comment')) {
                        $('#comments-toggle').slideDown(300);
                        $(a).removeClass('less-comment').addClass('more-comment').html('see less comments...');
                    } else {
                        $('#comments-toggle').slideUp(300);
                        $(a).removeClass('more-comment').addClass('less-comment').html('see more comments...');
                    };
                });

                $(window).scroll(function() {
                    var navTop = 161;

                    if ($(this).scrollTop() >= navTop) {
                        $('#gotop').css({
                            opacity: "1",
                            visibility: "visible",
                            margin: "0"
                        })
                    } else {
                        $('#gotop').css({
                            opacity: "0",
                            visibility: "hidden",
                            margin: "-45px 0 0"
                        })
                    }
                });

                $('#gotop').on('click', function() {
                    $('html, body').animate({ scrollTop: 0 }, 500); return false;
                });
            });
</script>
@endsection
