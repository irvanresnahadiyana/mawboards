@extends('layouts.frontend.master')

@section('title', $title)

@section('content')
<h1>Find "{{ $title }}"</h1>
<div class="row">
    <h2>Products <small>({{ count($products) }} Result)</small></h2>
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        @if (count($products) > 0)
            @include('contents.frontend.wish_list', ['result' => $products])
        @endif
    </section>
</div>
<div class="row">
    <h2>Wishes <small>({{ count($wishes) }} Result)</small></h2>
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        @if (count($wishes) > 0)
            @include('contents.frontend.wish_list', ['result' => $wishes])
        @endif
    </section>
</div>
<div class="row">
    <h2>Users <small>({{ count($users) }} Result)</small></h2>
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        @if ($users)
            @foreach ($users as $user)
                @if ($user->role_slug != 'administrator')
                    <div class="grid-item stucked col-xs-6 col-md-3">
                        <section class="stucked-box white">
                            <div class="stucked-box-desc">
                                <section class="stucked-box-desc-bottom">
                                    <div class="media">
                                        <section class="media-left media-middle">
                                            <a href="{{ route('user-wishboard-frontend', $user) }}" class="objectfit-container-cover">
                                                <img class="media-object grid-img-file objectfit-img" src="{{ link_to_avatar($user->getFriendAvatar($user->id) ? $user->getFriendAvatar($user->id)->image_thumb : 'no_image') }}" alt="...">
                                            </a>
                                        </section>
                                        <section class="media-body media-middle">
                                            <h4 class="media-heading"><a href="{{ route('user-wishboard-frontend', $user) }}">{{ $user->first_name.' '.$user->last_name }}</a></h4>
                                            <p>{{ $user->role_name }}</p>
                                        </section>
                                    </div>
                                </section>
                            </div>
                        </section>
                    </div>
                @endif
            @endforeach
        @endif
    </section>
</div>
@endsection
