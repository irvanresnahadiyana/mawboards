@extends('layouts.frontend.master')

@section('title', 'Friends')

@section('header')

@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <div class="row">
                <!-- Get a list of all Friendships -->
                <!-- <div class="panel panel-default">
                    <div class="panel-heading">Get a list of all Friendships</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getAllFriendships as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> -->
                <!-- Get a list of pending Friend Requests -->
                <div class="panel panel-default">
                    <div class="panel-heading">Get a list of pending Friend Requests</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getFriendRequests as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a><br>
                                        <button class="btn" onclick="acceptFriendFunction({{ $friend->sender_id }}, {{ user_info('id') }}); return false;" title="Accept this friend">Accept</button>
                                        <button class="btn" onclick="deniedFriendFunction({{ $friend->sender_id }}, {{ user_info('id') }}); return false;" title="Denied this friend">Denied</button>
                                        <button class="btn" onclick="blockFriendFunction({{ $friend->sender_id }}, {{ user_info('id') }}); return false;" title="Block this friend">Block</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Get a list of pending Friendships -->
                <div class="panel panel-default">
                    <div class="panel-heading">Get a list of pending Friendships</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getPendingFriendships as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        @if (user_info('id') != $friend->recipient_id)
                                            <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a><br>
                                            <button class="btn" onclick="removeFriendFunction({{ user_info('id') }}, {{ $friend->recipient_id }}, 'cancelFriendRequest'); return false;" title="Cancel friend request">Cancel</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Get a list of accepted Friendships -->
                <div class="panel panel-default">
                    <div class="panel-heading">Get a list of accepted Friendships</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getAcceptedFriendships as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        @if (user_info('id') != $friend->recipient_id)
                                            <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a><br>
                                            <button class="btn" onclick="removeFriendFunction({{ user_info('id') }}, {{ $friend->recipient_id }}, 'removeFriend'); return false;" title="Remove friend">Remove</button>
                                        @else
                                            <a href="{{ route('show-profile-frontend', $friend->sender_id) }}">View User Profile</a><br>
                                            <button class="btn" onclick="removeFriendFunction({{ user_info('id') }}, {{ $friend->sender_id }}, 'removeFriend'); return false;" title="Remove friend">Remove</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Get a list of denied Friendships -->
                <div class="panel panel-default">
                    <div class="panel-heading">Get a list of denied Friendships</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getDeniedFriendships as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Get a list of blocked Friendships -->
                <div class="panel panel-default">
                    <div class="panel-heading">Get a list of blocked Friendships</div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>sender_id</td>
                                <td>sender_type</td>
                                <td>recipient_id</td>
                                <td>recipient_type</td>
                                <td>status</td>
                                <td>created_at</td>
                                <td>updated_at</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getBlockedFriendships as $friend)
                                <tr>
                                    <td>{{ $friend->id }}</td>
                                    <td>{{ $friend->sender_id }}</td>
                                    <td>{{ $friend->sender_type }}</td>
                                    <td>{{ $friend->recipient_id }}</td>
                                    <td>{{ $friend->recipient_type }}</td>
                                    <td>{{ $friend->status }}</td>
                                    <td>{{ $friend->created_at }}</td>
                                    <td>{{ $friend->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('show-profile-frontend', $friend->recipient_id) }}">View User Profile</a><br>
                                        @if (user_info('id') != $friend->recipient_id)
                                            <button class="btn" onclick="unblockFriendFunction({{ user_info('id') }}, {{ $friend->recipient_id }}); return false;" title="Unblock this friend">Unblock</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </section>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        function acceptFriendFunction(sender_user_id, recipient_user_id) {
            $.ajax({
                type:'post',
                url:'{{ route("update-friend-frontend") }}',
                data: {
                    type: 'acceptFriendRequest',
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                }
            });
        }

        function deniedFriendFunction(sender_user_id, recipient_user_id) {
            $.ajax({
                type:'post',
                url:'{{ route("update-friend-frontend") }}',
                data: {
                    type: 'denyFriendRequest',
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                }
            });
        }

        function blockFriendFunction(sender_user_id, recipient_user_id) {
            $.ajax({
                type:'post',
                url:'{{ route("update-friend-frontend") }}',
                data: {
                    type: 'blockFriend',
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                }
            });
        }

        function unblockFriendFunction(sender_user_id, recipient_user_id) {
            $.ajax({
                type:'post',
                url:'{{ route("update-friend-frontend") }}',
                data: {
                    type: 'unblockFriend',
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                }
            });
        }

        function removeFriendFunction(sender_user_id, recipient_user_id, type_action) {
            $.ajax({
                type:'post',
                url:'{{ route("destroy-friend-frontend") }}',
                data: {
                    type: type_action,
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                }
            });
        }
    </script>
@endsection
