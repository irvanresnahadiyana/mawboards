@extends('layouts.frontend.master')

@section('title', 'Make a Wish')

@section('header')
<!-- Select-2 -->
{!! Html::style('bower_components/admin-lte/plugins/select2/select2.min.css') !!}
<!-- wysihtml5 -->
{!! Html::style('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <div class="profile-wishboard-inner">
                @if($wish_categories_selected)
                    <section class="profile-wishboard-color" style="background-color: {{ $wish_categories_selected->color ? $wish_categories_selected->color->hex : '#808080' }};"></section>
                    <section class="profile-wishboard-title">{{ $wish_categories_selected->name.' Wishboard' }}</section>
                @else
                    <section class="profile-wishboard-title">Make a new wish</section>
                @endif
            </div>
            <div class="row">
                <section class="grid">
                    <div class="grid-sizer col-xs-6 col-md-3"></div>
                    <div class="grid-item big col-xs-6">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <form id="form-add-wish">
                                    <div class="form-group" id="post-image">
                                        <label for="post-input-image">Upload Wish*</label> <small>{{ trans('general.avatar_validation') }}</small>
                                        <input type="file" name="image" id="post-input-image">
                                    </div>
                                    @if(!$wish_categories_selected)
                                        <div class="form-group" id="post-category">
                                            <label for="post-input-category">MAW Board*</label>
                                            <select class="form-control" name="category" id="category">
                                                <option></option>
                                                @if(count($wish_categories) > 0)
                                                    @foreach($wish_categories as $wish_category)
                                                        <option value="{{ $wish_category->id }}" title="{{ $wish_category->hex }}">{{ $wish_category->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div id="post-input-category"></div>
                                        </div>
                                    @else
                                        <input type="hidden" name="category" value="{{ $wish_categories_selected->id }}">
                                    @endif
                                    <div class="form-group" id="post-circle">
                                        <label for="post-input-circle">Circle</label>
                                        <select class="form-control select2" name="circle[]" multiple="">
                                            @if(count($circles) > 0)
                                                @foreach($circles as $circle)
                                                    <option value="{{ $circle->id }}">{{ $circle->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div id="post-input-circle"></div>
                                    </div>
                                    <div class="form-group" id="post-title">
                                        <label for="post-input-title">Title*</label>
                                        <input type="text" class="form-control" name="title" id="post-input-title" placeholder="Title*">
                                    </div>
                                    <div class="form-group" id="post-description">
                                        <label for="post-input-description">Description*</label>
                                        <textarea class="form-control wysihtml5" name="description" placeholder="Description your wish*"></textarea>
                                        <div id="post-input-description"></div>
                                    </div>
                                    <p class="text-center">
                                        <button type="submit" class="btn btn-warning btn-block button-post-wish" value="post" data-loading-text="Loading..." title="Post Wish">POST WISH</button>
                                        <span>OR</span>
                                        <button type="submit" class="btn btn-warning btn-block button-post-wish" value="post-see" data-loading-text="Loading..." title="Post & See Mawboard">POST & SEE MAWBOARD</button>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger btn-block">Cancel</a>
                                    </p>
                                    <p>
                                        <br><i>You can't create if you don't have any friends</i>
                                        <br><i>You don't have Circle, Please create a new <a href="{{ route('create-circle-frontend') }}" title="Add Circle">in here</a>!</i>
                                        <br><i>It will be automaticly post to public, if you don't choose circle</i>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <section class="col-xs-6 grid-item col-md-6 rect">
                        <section class="rect-box">
                            <div class="rect-user white">
                                <div class="rect-user" title="Preview Image">
                                    <section class="rect-user56"><i class="fa fa-camera fa-2x" aria-hidden="true"></i></section>
                                    <img src="" class="grid-img-file img-preview" alt="">
                                </div>
                            </div>
                        </section>
                    </section>
                </section>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<!-- Select-2 -->
{!! Html::script('bower_components/admin-lte/plugins/select2/select2.min.js') !!}
<!-- wysihtml5 -->
{!! Html::script('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<!-- Laravel Javascript Validation -->
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Frontend\StoreWish', '#form-add-wish'); !!}
<script type="text/javascript">
    $('.select2').select2({
        placeholder: "Choose",
        allowClear: true
    });

    $('#category').select2({
        placeholder: "Category",
        allowClear: true,
        templateResult: function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $('<span><span class="color-preview" style="background:'+state.title+';width:20px;height:15px;display:inline-block;margin-right:10px;"></span>'+state.text+'</span>');
            return $state;
        },
    });

    $(".wysihtml5").wysihtml5();

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.rect-user56').remove();
                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#post-input-image").change(function(){
        readURL(this);
    });

    $('.button-post-wish').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        var btn_type = event.target.value;
        var wish_categories_selected = "{{ $wish_categories_selected ? $wish_categories_selected : '' }}";

        $('.form-group').removeClass('has-error');
        $('.help-block').remove();

        $.ajax({
            type:'post',
            url:'{{ route("store-wish-frontend") }}',
            data: new FormData($("#form-add-wish")[0]),
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            success:function(data) {
                if (wish_categories_selected == '') {
                    // redirect by type button if just post or post and see wish
                    if (btn_type == 'post') {
                        window.location = "{{ route('index-wishboard-frontend') }}";
                    } else {
                        window.location = "{{ route('index-wishboard-frontend') }}";
                    }
                } else {
                    window.location = "{{ url()->previous() }}";
                }
            },
            error:function(data) {
                var data = data.responseJSON;
                $.each(data,function(key, val){
                    $('#post-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#post-input-'+key));
                });
                $btn.button('reset');
            }
        });
    });
</script>
@endsection
