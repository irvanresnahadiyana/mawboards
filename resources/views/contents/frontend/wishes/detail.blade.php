@extends('layouts.frontend.master')

@section('title', 'MAW | Detail Wish')

@section('header')
<link rel="canonical" href="" />
<meta property="og:url" content="{{ $wish->url }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $wish->title }}" />
<meta property="og:description" content="{{ $wish->title }}" />
<meta property="og:image" content="{{ $wish->image }}" />
@endsection

@section('content')
<div class="modal-detail wish-detail">
    <section class="container">
        <div class="row">
            <section class="col-md-9">
                @include('layouts.frontend.partials.modal_wish_contents.detail')
            </section>
            <section class="col-md-3">
                @include('layouts.frontend.partials.modal_wish_contents.maw')
            </section>
        </div>
        <div class="row">
            <section class="col-md-9">
                <div class="modal-box">
                    <section class="modal-box-header">
                        <div class="modal-box-header-title">
                            <span>Related Wishboard</span>
                        </div>
                    </section>
                </div>
            </section>
        </div>
        @include('layouts.frontend.partials.modal_wish_contents.related_wish')
    </section>
</div>
@endsection

@section('script')
    @include('contents.frontend.wishes.js.detail_js')
@endsection
