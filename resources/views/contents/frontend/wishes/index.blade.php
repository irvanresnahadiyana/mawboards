@extends('layouts.frontend.master')

@section('title', $title)

@section('header')

@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <ul>
                <li class="profile-wishboard-create">
                    <a href="#">
                        <div class="profile-wishboard-list profile-wishboard-create-btn">
                            <i class="maw-add-o"></i>
                        </div>
                        <div class="profile-wishboard-text profile-wishboard-create-text">
                            Create Wish Board
                        </div>
                    </a>
                </li>
                @if (count($wishboards) > 0)
                    @foreach ($wishboards as $wishboard)
                        <li>
                            <a href="#{{ $wishboard->slug }}">
                                <div class="profile-wishboard-list">
                                    <section class="profile-wishboard-list-box">
                                        <div class="profile-wishboard-list-big objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1486334803289-1623f249dd1e?dpr=1&auto=format&fit=crop&w=1500&h=939&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1481391145929-5bcf567d5211?dpr=1&auto=format&fit=crop&w=1500&h=1875&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/photo-1473216635433-38f7100ae658?dpr=1&auto=format&fit=crop&w=1500&h=970&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-small objectfit-container-cover">
                                            <img class="objectfit-img" src="https://images.unsplash.com/5/unsplash-kitsune-4.jpg?dpr=1&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" alt="">
                                        </div>
                                        <div class="profile-wishboard-list-color" style="background-color: {{ $wishboard->color_hex ? $wishboard->color_hex : 'white' }};"></div>
                                    </section>
                                </div>
                                <div class="profile-wishboard-text">
                                    {{ $wishboard->name }}
                                    <small>{{ $wishboard->total }} Wishes</small>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>
</div>

<!-- <div class="row">
    <section class="grid">
        <div class="grid-sizer col-xs-6 col-md-3"></div>
        <div class="grid-item big col-xs-12 col-md-6">
            <section class="big-box white">
                <div class="big-user">
                    <section class="big-user-avatar col-xs-6">
                        <div class="grid-img">
                            <img class="grid-img-file" src="{{ link_to_avatar($user_avatar ? $user_avatar->image_thumb : 'no-preview-available.png') }}" alt="User Avatar">
                        </div>
                    </section>
                    <section class="big-user-detail col-xs-6">
                        <div class="rect-box">
                            <section class="rect-user24">{{ $user ? $user->first_name.' '.$user->last_name : user_info('full_name') }}</section>
                            <section class="big-user-detail-location">{{ $user ? $user->live_in : user_info('live_in') }}</section>
                            @if(!$user)
                                <section>
                                    <br>
                                    <a href="{{ route('edit-profile-frontend') }}" type="button" class="btn btn-default" title="Edit Profile">Edit Profile</a>
                                </section>
                            @endif
                        </div>
                    </section>
                    @if ($user)
                        <section class="big-user-notif col-xs-6">
                            <div class="rect-box">
                            </div>
                        </section>
                        <section class="big-user-notif col-xs-6">
                            <div class="rect-box">
                                @if ($user->id != user_info('id'))
                                    @if (!$friend_status['status'])
                                        {{ $friend_status['message'] }}
                                        <button type="button" class="btn btn-1" id="button-invite-friend" title="Invite Friend" >Invite</button>
                                    @else
                                        @if ($friend_status['message'] != 'isFriend')
                                            {{ $friend_status['message'] }}
                                            <button type="button" class="btn btn-1" id="button-remove-friend" onclick="removeFriendFunction({{ user_info('id') }}, {{ $user->id }}, 'cancelFriendRequest'); return false;" title="Cancel friend request"><small>Cancel Request?</small></button>
                                        @else
                                            <button type="button" class="btn btn-1" id="button-remove-friend" onclick="removeFriendFunction({{ user_info('id') }}, {{ $user->id }}, 'removeFriend'); return false;" title="Remove friend"><small>Remove Friend</small></button>
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </section>
                    @else
                        <section class="big-user-notif col-xs-12">
                            <div class="rect-box">
                                <ul>
                                    <li><a href="#">You <span class="badge badge-baloon">14</span></a></li>
                                    <li><a href="#">News <span class="badge badge-baloon">14</span></a></li>
                                    <li><a href="#">Messages <span class="badge badge-baloon">104</span></a></li>
                                    <li><a href="{{ route('index-friend-frontend', user_info('id')) }}">Friends <span class="badge badge-baloon">{{ $user ? $user->getFriendsCount() : user_info()->getFriendsCount() }}</span></a></li>
                                    <li><a href="{{ route('index-circle-frontend') }}">Circle <span class="badge badge-baloon">{{ $circle_total }}</span></a></li>
                                </ul>
                            </div>
                        </section>
                    @endif
                </div>
            </section>
        </div>
        <section class="col-xs-12 grid-item col-md-6 rect">
            <div class="rect-box sea">
                <section class="rect-user">
                    <div class="rect-user-big">
                        <section class="rect-user52"><font color="white">{{ $user ? $user->first_name : 'My' }}</font></section>
                        <section class="rect-user52"><font color="white">Mawboard</font></section>
                        <section class="rect-user24">
                            <font color="white">
                                {{ count($wishes) }} <a href="{{ route('index-wish-frontend') }}" title="MAW List">MAW</a>
                                <span class="pull-right">
                                    {{ $user ? $user->getFriendsCount() : user_info()->getFriendsCount() }}
                                    <a href="{{ route('index-friend-frontend', $user ? $user->id : user_info('id')) }}" title="Friend List">Friends</a>
                                </span>
                            </font>
                        </section>
                    </div>
                </section>
            </div>
        </section>
        @if(count($wishes) > 0)
            @foreach($wishes as $wish)
                <div class="grid-item stucked col-xs-6 col-md-3">
                    <section class="stucked-box white">
                        <div class="grid-img">
                            <a href="{{route('detail-wish-frontend',['slug' => $wish->slug])}}" class="grid-img-btn show-wish"
                                data-wishid="{{ $wish->id }}"
                                data-wishuser="{{ $wish->user_name }}"
                                data-wishuseravatar="{{ link_to_avatar($wish->avatar ? $wish->avatar : 'no_image') }}"
                                data-wishtitle="{{ $wish->title }}"
                                data-wishcategory="{{ $wish->category_name }}"
                                data-wishlinkimage="{{ get_file(@$wish->image->image_standard) }}"
                                data-wishslug="{{$wish->slug}}"
                                data-maw="{{$wish->hex ? 'maw' : 'product'}}">
                                <img class="grid-img-file" src="{{ link_to_avatar($wish->wish_image_thumb) }}" alt="Image">
                            </a>
                            <ul class="grid-img-action">
                                <li><a href="javascript:void(0);" id="wish-share-{{$wish->id}}" class="btn btn-default btn-sm wish-share-bookmark" data-id="{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}"><i class="fa fa-paper-plane"></i></a></li>
                                <li><a href="javascript:void(0);" id="wish-like-{{$wish->id}}" class="btn btn-default btn-sm wish-like-bookmark {{$wish->status_like ? 'liked' : ''}}" data-id="{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}" data-type="like"><i class="fa fa-heart"></i></a></li>
                                <li><a href="javascript:void(0);" id="wish-bookmark-{{$wish->id}}" class="btn btn-default btn-sm wish-like-bookmark" data-id="{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}" data-type="bookmark"><i class="fa fa-thumb-tack"></i></a></li>
                            </ul>
                        </div>
                        <div class="stucked-box-desc">
                            <section class="stucked-box-desc-top" style="background-color: {{ $wish->hex ? $wish->hex : 'black' }}">
                                <div class="stucked-box-desc-top-text">
                                    <section class="stucked-box-desc-top-name">{{ str_limit($wish->title,26) }}</section>
                                    <section class="stucked-box-desc-top-from">From {{!$wish->hex ? $wish->user_name : 'My Circle'}}</section>
                                </div>
                                <div class="stucked-box-desc-top-badges">
                                    <ul>
                                        <li><i class="fa fa-gift"></i> 14</li>
                                        <li><i class="fa fa-heart"></i> <span class="people-likes-{{$wish->id}}">{{number_format($wish->people_likes->count(),0,',','.')}}</span></li>
                                    </ul>
                                </div>
                            </section>
                            <section class="rect-box-desc-bottom">
                                <div class="media">
                                    <section class="media-left media-middle">
                                        <a href="#">
                                            <img class="media-object grid-img-file" src="{{ link_to_avatar($wish->avatar ? $wish->avatar : 'no_image') }}" alt="User Avatar">
                                        </a>
                                    </section>
                                    <section class="media-body media-middle">
                                        <h4 class="media-heading"><a href="#">{{ $wish->first_name }} {{ $wish->last_name }}</a></h4>
                                        <p>My {{ $wish->wish_category_name }} wishboard</p>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            @endforeach
            <section class="col-xs-6 grid-item col-md-3 rect">
                <section class="rect-box">
                    <div class="rect-user gray">
                        <div class="rect-user-big">
                            <section class="rect-user28"><a href="{{ route('create-wish-frontend') }}">Make a wish</a></section>
                        </div>
                    </div>
                </section>
            </section>
        @else
            <section class="col-xs-6 grid-item col-md-6 rect">
                <section class="rect-box">
                    <div class="rect-user gray">
                        <div class="rect-user-big">
                            <section class="rect-user56"><a href="{{ route('create-wish-frontend') }}">Make a wish</a></section>
                        </div>
                    </div>
                </section>
            </section>
        @endif
    </section>
</div> -->
@endsection

@section('script')
@if ($user)
    <script type="text/javascript">
        $('#button-invite-friend').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $.ajax({
                type:'post',
                url:'{{ route("store-friend-frontend") }}',
                data: {
                    type: 'befriend',
                    sender_user_id: {{ user_info('id') }},
                    recipient_user_id: {{ $user->id }},
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                    $btn.button('reset');
                }
            });
        });

        function removeFriendFunction(sender_user_id, recipient_user_id, type_action) {
            var $btn = $('#button-remove-friend').button('loading');
            $.ajax({
                type:'post',
                url:'{{ route("destroy-friend-frontend") }}',
                data: {
                    type: type_action,
                    sender_user_id: sender_user_id,
                    recipient_user_id: recipient_user_id,
                },
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    console.log(data);
                    $btn.button('reset');
                }
            });
        }
    </script>
@endif
@endsection
