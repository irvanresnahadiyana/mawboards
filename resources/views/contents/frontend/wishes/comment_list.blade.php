@if(count($comments) > 0)
    @foreach($comments as $comment)
        <div class="media">
            <section class="media-left media-middle">
                <a href="#">
                    <img class="media-object grid-img-file" src="{{link_to_avatar(($comment->user->images()->first() ? $comment->user->images()->first()->image_standard : null),'thumbnail')}}" alt="...">
                </a>
            </section>
            <section class="media-body media-middle">
                <h4 class="media-heading"><a href="#">{{$comment->user->first_name.' '.$comment->user->last_name}}</a> :</h4>
                <p>{{$comment->comment}}</p>
            </section>
        </div>
    @endforeach
    @if($total_comments > 7)
        <button type="button" name="button" class="btn btn-unstyle less-comment" id="modal-see-more" data-wishslug="{{$wish->slug}}">see more comments...</button>
    @endif
@else
    <p>No comments...</p>
@endif