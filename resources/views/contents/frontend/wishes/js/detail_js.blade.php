<script type="text/javascript">
    $('#form-add-comment').submit(function(e){
        e.preventDefault();
        if (status_user) {
            var btn = $('#submit-comment').button('loading');
            var formData = $(this).serialize();
            // postComment(urlPostComment,formData,btn,"#wish-modal-",'form-add-comment-modal',$('.comments-modal'));
            postComment("{{ route('store-comment-frontend') }}",formData,btn,"#wish-",'form-add-comment',$('.comments-lists'));
        } else {
            generateNotif('Information',msgInfoLoginReg,'warning');
        }
    });
</script>