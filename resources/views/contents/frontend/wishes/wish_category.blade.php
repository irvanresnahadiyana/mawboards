@extends('layouts.frontend.master')

@section('title', $wish_category->name.' wishboard')

@section('header')

@endsection

@section('content')
<div class="profile">
    <section class="profile-wishboard">
        @include('layouts.frontend.partials.profile-wishboard-header')
        <div class="profile-wishboard-body">
            <div class="profile-wishboard-inner">
                <section class="profile-wishboard-color" style="background-color: {{ $wish_category->color ? $wish_category->color->hex : '#808080' }};"></section>
                <section class="profile-wishboard-title">
                    {{ $wish_category->name.' Wishboard' }}
                    <small>
                        ({{ count($wishes) }} wishes)
                        <a href="{{ route('create-wish-wishboard-frontend', $wish_category->slug) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Make a new wish</a>
                    </small>
                </section>
            </div>
            @if (count($wishes) > 0)
                <div class="row">
                    <section class="grid">
                        <div class="grid-sizer col-xs-6 col-md-3"></div>
                        @foreach ($wishes as $wish)
                            <div class="grid-item stucked col-xs-6 col-md-3">
                                <section class="stucked-box white">
                                    <div class="grid-img">
                                        <a href="{{ route('detail-wish-frontend', ['slug' => $wish->slug]) }}" class="grid-img-btn show-wish"
                                            data-wishid="{{ $wish->id }}"
                                            data-wishuser="{{ $wish->user_id }}"
                                            data-wishuseravatar="{{ get_file(@$wish->avatar) }}"
                                            data-wishtitle="{{ $wish->title }}"
                                            data-wishcategory="{{ $wish_category->name }}"
                                            data-wishlinkimage="{{ get_file(@$wish->image->image_standard) }}"
                                            data-wishslug="{{ $wish->slug }}"
                                            data-maw="maw">
                                            <img class="grid-img-file" src="{{ get_file(@$wish->image->image_standard, 'thumbnail') }}" alt="Image">
                                        </a>
                                        <ul class="grid-img-action">
                                            <li>
                                                <button class="btn btn-default btn-sm" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="maw-share"></i></button>
                                                <a href="javascript:void(0);" class="btn btn-default btn-sm wish-like-bookmark {{$wish->status_like ? 'liked' : ''}}" id="wish-like-{{$wish->id}}" data-group="{{($wish->hex ? 'App\Models\Wish' : 'App\Models\Product')}}" data-id="{{$wish->id}}" data-type="like"><i class="maw-heart-o"></i></a>
                                            </li>
                                            <li><a href="#" class="btn btn-default btn-sm" role="button" data-remodal-target="pin"><i class="maw-maw"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="stucked-box-desc">
                                        <section class="stucked-box-desc-top" style="background-color: {{ $wish_category->color ? $wish_category->color->hex : '#808080' }};">
                                            <div class="stucked-box-desc-top-text">
                                                <section class="stucked-box-desc-top-name">{{ str_limit($wish->title, 26) }}</section>
                                            </div>
                                            <div class="stucked-box-desc-top-badges">
                                                <ul>
                                                    <li><a href="#"><i class="maw-heart-o"></i> <span class="people-likes-{{ $wish->id }}">{{ number_format((@$wish->people_likes ? $wish->people_likes->count() : $wish->likes->count()), 0, ',', '.') }}</span></a></li>
                                                    <li><a href="#"><i class="maw-maw"></i> 0</a></li>
                                                </ul>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                        @endforeach
                    </section>
                </div>
            @else
                <br><br><br>
                <h1 class="text-center" style="color: gray;">You don't have wishes on this wishboard</h1>
                <p class="text-center">
                    <a href="{{ route('create-wish-wishboard-frontend', $wish_category->slug) }}">Make a wish</i></a>
                </p>
            @endif
        </div>
    </section>
</div>
@endsection

@section('script')
<script type="text/javascript">
    
    $(document).ready(function() {
                $('.grid-img-action li button.btn').each(function() {

                    $('.grid-img-action li button.btn').popover({
                        animation: false,
                        container: 'body',
                        placement: function (context, source) {
                            var body = $('body').height();
                            var position = $(source).offset();
                            var val = body - position.top;

                            if (val < 500){
                                return "top";
                            }

                            return "bottom";
                        },
                        template: '<div class="popover" role="tooltip">'
                                + '<div class="arrow"></div>'
                                + '<section class="popover-header">'
                                    + '<div class="form-group">'
                                        + '<input type="text" class="form-control" placeholder="Search for name or email">'
                                        + '<span><i class="fa fa-search"></i></span>'
                                    + '</div>'
                                + '</section>'
                                + '<section class="popover-body" id="popover-scroll">'
                                    + '@foreach($friends as $friend)'
                                    + '<button class="media" type="button">'
                                        + '<section class="media-left media-middle">'
                                            + '<span href="#" class="objectfit-container-cover">'
                                                + '<img class="media-object grid-img-file objectfit-img" src="https://images.unsplash.com/photo-1468218457742-ee484fe2fe4c?dpr=1&auto=format&fit=crop&w=1500&h=997&q=80&cs=tinysrgb&crop=" alt="...">'
                                            + '</span>'
                                        + '</section>'
                                        + '<section class="media-body media-middle">'
                                            + '<h4 class="media-heading" style="margin: 0;">{{ $friend->first_name }} {{ $friend->last_name }}</h4>'
                                        + '</section>'
                                    + '</button>'
                                    + '@endforeach '
                                + '</section>'
                                + '<section class="popover-footer">'
                                    + '<div class="popover-share-text">Share this Wish Board:</div>'
                                    + '<ul class="popover-share">'
                                        + '<li><button type="button" class="share-facebook"><i class="fa fa-facebook"></i></button></li>'
                                        + '<li><button type="button" class="share-twitter"><i class="fa fa-twitter"></i></button></li>'
                                        + '<li><button type="button" class="share-google"><i class="fa fa-google-plus"></i></button></li>'
                                        + '<li><button type="button" class="share-instagram"><i class="fa fa-instagram"></i></button></li>'
                                        + '<li><button type="button" class="share-instagram"><i class="fa fa-link"></i></button></li>'
                                    + '</ul>'
                                +'</section>'
                            + '</div>'
                    });

                    $(this).on('click', function (e) {

                        if ($(this).parent().parent().hasClass('active')) {
                            $(this).popover('hide');
                            $(this).parent().parent().removeClass('active');
                        } else {
                            e.stopPropagation();
                            $('.grid-img-action li button.btn').not(this).popover('hide');
                            $('.grid-img-action li button.btn').not(this).parent().parent().removeClass('active');
                            $(this).popover('show');
                            $(this).parent().parent().addClass('active');
                            var popScroll = document.getElementById('popover-scroll');
                            Ps.initialize(popScroll);
                        }
                    });

                    $('body').on('click', function(e) {
                        $('.grid-img-action li button.btn').popover('hide');
                        $('.grid-img-action').removeClass('active');
                    });
                });

                // var inst = $('[data-remodal-id=modal]').remodal();

                // $('.grid-img-btn').on('click', function(e) {
                //     e.preventDefault();
                //     e.stopPropagation();
                //     inst.open();
                // });

                // $('.modal-detail').on('click', function() {
                //     inst.close();
                // });

                // $('.modal-box').on('click', function(e) {
                //     // prevent modal close
                //     e.preventDefault();
                //     e.stopPropagation();
                // });

                $(document).on('opening', '.remodal', function () {
                    // $modalWhisboard.imagesLoaded().progress( function() {
                    //     $modalWhisboard.masonry('layout');
                    //     // Ps.initialize(scrollBar);
                    // });

                    // $modalGrid.imagesLoaded().progress( function() {
                    //     $modalGrid.masonry('layout');
                    // });

                    // $modalGrid.masonry('layout');

                    finish = true;
                });

                $(document).on('closing', '.remodal,.modal', function () {
                    x = 1;
                    if (!$(this).hasClass('remodal-login'))
                        history.pushState(null, null, currentURL);

                    // $modalGrid.masonry( 'reloadItems');
                    // $modalGrid.masonry('layout');
                });

                // $(document).on('opened', '#modal-wish-detail', function (event) {
                //     var wishSlug = buttonWish.data('wishslug');
                //     var wishid = buttonWish.data('wishid'); // Extract info from data-* attributes
                //     var wishuser = buttonWish.data('wishuser');
                //     var wishuseravatar = buttonWish.data('wishuseravatar');
                //     var wishtitle = buttonWish.data('wishtitle');
                //     var wishcategory = buttonWish.data('wishcategory');
                //     var wishlinkimage = buttonWish.data('wishlinkimage');

                //     // Update the modal's content.
                //     var modal = $(this);
                //     modal.find('#modal-wish-title').text(wishtitle);
                //     modal.find('#modal-wish-name-category').text(wishuser+' '+wishcategory+' Wishboard');
                //     modal.find('#modal-wish-image').attr("src", wishlinkimage);
                //     modal.find('#modal-wish-user').text(wishuser);
                //     modal.find('#modal-wish-user-avatar').attr("src", wishuseravatar);
                //     modal.find('#modal-wish-user-category').text(wishcategory+' Wishboard');
                //     history.pushState(null, null, '/maw/'+wishSlug);
                // });

                $('#modal-see-more').on('click', function() {
                    var a = $(this);
                    if ($(a).hasClass('less-comment')) {
                        $('#comments-toggle').slideDown(300);
                        $(a).removeClass('less-comment').addClass('more-comment').html('see less comments...');
                    } else {
                        $('#comments-toggle').slideUp(300);
                        $(a).removeClass('more-comment').addClass('less-comment').html('see more comments...');
                    };
                });

                $(window).scroll(function() {
                    var navTop = 161;

                    if ($(this).scrollTop() >= navTop) {
                        $('#gotop').css({
                            opacity: "1",
                            visibility: "visible",
                            margin: "0"
                        })
                    } else {
                        $('#gotop').css({
                            opacity: "0",
                            visibility: "hidden",
                            margin: "-45px 0 0"
                        })
                    }
                });

                $('#gotop').on('click', function() {
                    $('html, body').animate({ scrollTop: 0 }, 500); return false;
                });
            });
</script>
@endsection
