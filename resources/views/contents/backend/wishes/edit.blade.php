@extends('layouts/backend/master/admin_template')

@section('title', 'Edit Wish')
@section('page_title', 'Edit Wish')
@section('page_description', 'Edit Wish')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{route('admin-wish')}}">Wish</a></li>
        <li>Edit Wish</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="edit-wish">
                    <div class="box-body">
                        <div class="error"></div>

                        <input type="hidden" name="id" value="{{$wishDetail->id}}">
                        <div class="form-group" id="wish-user">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-7">
                                <input type="text" value="{{$wishDetail->user->first_name.' '.$wishDetail->user->last_name}}" class="form-control" disabled="true">
                            </div>
                        </div>

                        <div class="form-group" id="wish-title">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-7">
                                <input type="text" name="title" class="form-control" placeholder="Input title" id="title" value="{{$wishDetail->title}}">
                            </div>
                        </div>

                        <div class="form-group" id="wish-category">
                            <label class="col-sm-3 control-label">Category</label>
                            <div class="col-sm-7">
                                <select class="form-control select2" data-placeholder="Choose category" name="category" id="category">
                                    <option></option>
                                    @if(count($wishCategories) > 0)
                                        @foreach($wishCategories as $wishCategory)
                                            <option value="{{$wishCategory->id}}" {{$wishCategory->id == $wishDetail->wish_category_id ? 'selected' : ''}}>{{$wishCategory->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="wish-description">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-7">
                                <textarea class="textarea" name="description" id="description">{{$wishDetail->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group" id="wish-image">
                            <label class="col-sm-3 control-label">Image</label>
                            <div class="col-sm-3">
                                @if($imageWish)
                                    <img src="{{get_file($imageWish->image_thumb)}}" class="img-responsive wish-image">
                                @else
                                    <img src="https://www.justpro.co/img/no-image.png" class="img-responsive wish-image">
                                @endif
                                <input type="file" name="image" class="input-file" id="image" accept="image/*">
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-wish') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.wishes.js.edit_js')
@endsection
