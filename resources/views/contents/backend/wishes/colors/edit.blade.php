@extends('layouts/backend/master/admin_template')

@section('title', 'Edit Wish Color')
@section('page_title', 'Edit Wish Color')
@section('page_description', 'Edit Wish Color')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{route('admin-wish-color')}}">Wish Color</a></li>
        <li>Edit Wish Color</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="edit-wish-color">
                    <div class="box-body">
                        <div class="error"></div>

                        <input type="hidden" name="id" value="{{$wishColor->id}}">
                        <div class="form-group" id="wish-color-name">
                            <label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" class="form-control" placeholder="Input color name" id="name" value="{{$wishColor->name}}">
                            </div>
                        </div>

                        <div class="form-group" id="wish-color-hex">
                            <label class="col-sm-4 control-label">Color</label>
                            <div class="col-sm-4">
                                <input type="color" name="hex" class="form-control" id="hex" value="{{$wishColor->hex}}">
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-wish-color') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.wishes.colors.js.edit_js')
@endsection
