<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#wish-color-table').DataTable();
        table.destroy();

        $('#wish-color-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-wish-color-datatable") }}',
            columns: [
                {data: 'name', name: 'colors.name'},
                {data: 'hex', name: 'colors.hex'},
                {data: 'created_at', name: 'colors.created_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('submit','#delete-wish-color',function(event){
        event.preventDefault();
        var btn = $('#submit-delete').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('admin-wish-color-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>