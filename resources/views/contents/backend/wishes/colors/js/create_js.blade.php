<script type="text/javascript">
    $(document).ready(function(){
    });

    $(document).on('submit','#create-wish-color',function(event){
        event.preventDefault();
        var btn = $('#submit-save').button('loading');
        var formData = new FormData(this);

        postWithAjax('store',"{{ route('admin-wish-color-store') }}",formData,btn,"#wish-color-");
    });

</script>