<script type="text/javascript">
    $(document).ready(function(){
    });

    $(document).on('submit','#edit-wish-color',function(event){
        event.preventDefault();
        var btn = $('#submit-save').button('loading');
        var formData = new FormData(this);

        postWithAjax('store',"{{ route('admin-wish-color-update') }}",formData,btn,"#wish-color-");
    });

</script>