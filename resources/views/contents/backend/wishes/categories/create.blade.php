@extends('layouts/backend/master/admin_template')

@section('title', 'Create Wish Category')
@section('page_title', 'Create Wish Category')
@section('page_description', 'Create Wish Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{route('admin-wish-category')}}">Wish Category</a></li>
        <li>Create Wish Category</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="create-wish-category">
                    <div class="box-body">
                        <div class="error"></div>

                        <div class="form-group" id="wish-category-name">
                            <label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" class="form-control" placeholder="Input category name" id="name">
                            </div>
                        </div>

                        <div class="form-group" id="wish-category-color_id">
                            <label class="col-sm-4 control-label">Color</label>
                            <div class="col-sm-4">
                                <select class="form-control select2" data-placeholder="Color" name="color_id" id="color_id">
                                    <option></option>
                                    @if(count($colors) > 0)
                                        @foreach($colors as $color)
                                            <option value="{{$color->id}}" title="{{$color->hex}}">{{$color->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-wish-category') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.wishes.categories.js.create_js')
@endsection
