<script type="text/javascript">
    $(document).ready(function(){
        $('#color_id').select2({
            templateResult: function formatState (state) {
                if (!state.id) { return state.text; }
                var $state = $('<span><span class="color-preview" style="background:'+state.title+';width:20px;height:15px;display:inline-block;margin-right:10px;"></span>'+state.text+'</span>');
                return $state;
            },
        });
    });

    $(document).on('submit','#edit-wish-category',function(event){
        event.preventDefault();
        var btn = $('#submit-save').button('loading');
        var formData = new FormData(this);

        postWithAjax('store',"{{ route('admin-wish-category-update') }}",formData,btn,"#wish-category-");
    });
</script>