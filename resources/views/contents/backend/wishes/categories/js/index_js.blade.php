<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#wish-category-table').DataTable();
        table.destroy();

        $('#wish-category-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-wish-category-datatable") }}',
            columns: [
                {data: 'name', name: 'wish_categories.name'},
                {data: 'color_name', name: 'colors.name'},
                {data: 'slug', name: 'wish_categories.slug'},
                {data: 'created_at', name: 'wish_categories.created_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('submit','#delete-wish-category',function(event){
        event.preventDefault();
        var btn = $('#submit-delete').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('admin-wish-category-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>