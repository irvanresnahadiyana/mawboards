@extends('layouts/backend/master/admin_template')

@section('title', 'Create Wish')
@section('page_title', 'Create Wish')
@section('page_description', 'Create Wish')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{route('admin-wish')}}">Wish</a></li>
        <li>Create Wish</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="create-wish">
                    <div class="box-body">
                        <div class="error"></div>

                        <div class="form-group" id="wish-user">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-7">
                                <select class="form-control select2" data-placeholder="Choose user" name="user" id="user">
                                    <option></option>
                                    @if(count($users) > 0)
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="wish-title">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-7">
                                <input type="text" name="title" class="form-control" placeholder="Input title" id="title">
                            </div>
                        </div>

                        <div class="form-group" id="wish-category">
                            <label class="col-sm-3 control-label">Category</label>
                            <div class="col-sm-7">
                                <select class="form-control select2" data-placeholder="Choose category" name="category" id="category">
                                    <option></option>
                                    @if(count($wishCategories) > 0)
                                        @foreach($wishCategories as $wishCategory)
                                            <option value="{{$wishCategory->id}}">{{$wishCategory->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="wish-description">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-7">
                                <textarea class="textarea" name="description" id="description"></textarea>
                            </div>
                        </div>

                        <div class="form-group" id="wish-image">
                            <label class="col-sm-3 control-label">Image</label>
                            <div class="col-sm-3">
                                <img src="https://www.justpro.co/img/no-image.png" class="img-responsive wish-image">
                                <input type="file" name="image" class="input-file" id="image" accept="image/*">
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-wish') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.wishes.js.create_js')
@endsection
