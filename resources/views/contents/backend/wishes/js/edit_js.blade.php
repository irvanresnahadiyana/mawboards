<script type="text/javascript">
    $(document).ready(function(){
        $(".input-file").hide();
    });

    $(document).on('submit','#edit-wish',function(event){
        event.preventDefault();
        var btn = $('#submit-save').button('loading');
        var formData = new FormData(this);

        postWithAjax('update',"{{ route('admin-wish-update') }}",formData,btn,"#wish-");
    });

    $(document).on('click','.wish-image',function(){
        $('.input-file').click();
    });

    $(document).on('change',".input-file",function(){
        var image = $('.wish-image');

        readURLAvatar(this,image);
    });

    function readURLAvatar(input,image) {
        if (input.files && input.files[0]) {

          var reader = new FileReader();

          reader.onload = function (e) {
            image.attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
        }
    }
</script>