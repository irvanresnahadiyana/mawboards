<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#wish-table').DataTable();
        table.destroy();

        $('#wish-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-wish-datatable") }}',
            columns: [
                {data: 'first_name', name: 'users.first_name'},
                {data: 'last_name', name: 'users.last_name'},
                {data: 'title', name: 'wishes.title'},
                {data: 'category_name', name: 'wish_categories.name'},
                {data: 'created_at', name: 'wishes.created_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('submit','#delete-wish',function(event){
        event.preventDefault();
        var btn = $('#submit-delete').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('admin-wish-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>