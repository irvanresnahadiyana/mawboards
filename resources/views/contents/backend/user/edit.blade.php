@extends('layouts/backend/master/admin_template')

@section('title', 'Edit New User')
@section('page_title', 'Edit New User')
@section('page_description', 'Edit New User')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{route('admin-user')}}">User</a></li>
        <li>Edit User</li>
    </ol>
@endsection

@section('content')
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="form-update">
                  <div class="box-body">
                    <input type="hidden" name="id" value="{{$userDetail->id}}">
                    <div class="form-group" id="update-email">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{$userDetail->email}}" disabled="true">
                        </div>
                    </div>
                    <div class="form-group" id="update-first_name">
                        <label class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Input your first name" value="{{$userDetail->first_name}}">
                        </div>
                    </div>
                    <div class="form-group" id="update-last_name">
                        <label class="col-sm-2 control-label">last Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Input your last name" value="{{$userDetail->last_name}}">
                        </div>
                    </div>
                    <div class="form-group" id="update-password">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group" id="update-password_confirmation">
                        <label class="col-sm-2 control-label">Password Confimation</label>
                        <div class="col-sm-9">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Password Confirmation">
                        </div>
                    </div>
                    <div class="form-group" id="update-gender">
                        <label class="col-sm-2 control-label">Gender</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" name="gender" class="minimal" {{$userDetail->gender == 'male' ? 'checked' : ''}} value="male"> Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" class="minimal" {{$userDetail->gender == 'female' ? 'checked' : ''}} value="female"> Female
                            </label>
                            <div id="gender"></div>
                        </div>
                    </div>
                    <div class="form-group" id="update-birthday">
                        <label class="col-sm-2 control-label">Birthday</label>
                        <div class="col-sm-9">
                            <input type="text" name="birthday" id="birthday" class="form-control datepicker" placeholder="Input your birthday" readonly="true" value="{{date('d F Y',strtotime($userDetail->birthday))}}">
                        </div>
                    </div>
                  </div>
                    <div class="box-footer">
                        <a href="{{ route('admin-user') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="button-update">Register</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('contents.backend.user.js.edit_js')
@endsection
