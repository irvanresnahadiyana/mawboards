<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#user-table').DataTable();
        table.destroy();

        $('#user-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-user-datatable") }}',
            columns: [
                {data: 'email', name: 'users.email'},
                {data: 'first_name', name: 'users.first_name'},
                {data: 'last_name', name: 'users.last_name'},
                {data: 'role_name', name: 'roles.name'},
                {data: 'created_at', name: 'users.created_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('submit','#delete-user',function(event){
        event.preventDefault();
        var btn = $('#submit-delete').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('admin-user-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>
