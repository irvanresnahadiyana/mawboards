<script type="text/javascript">
    $(document).on('submit','#form-update',function(event){
        event.preventDefault();
        var btn = $('#button-update').button('loading');
        var formData = new FormData(this);

        postWithAjax('store',"{{ route('admin-user-update') }}",formData,btn,"#update-");
    });
</script>