<script type="text/javascript">
$(document).on('submit','#form-register',function(event){
    event.preventDefault();
    var btn = $('#button-register').button('loading');
    var formData = new FormData(this);

    postWithAjax('store',"{{ route('admin-user-register') }}",formData,btn,"#register-");
});
// $('#button-register').click(function(event) {
//     event.preventDefault();
//     var $btn = $(this).button('loading');
//     $('.form-group').removeClass('has-error');
//     $('.help-block').remove();
//     $.ajax({
//         type:'post',
//         url:'{{ route("admin-user-register") }}',
//         data: $('#form-register').serialize(),
//         dataType: 'json',
//         success:function(data) {
//             generateNotif('topCenter', data.result, data.message);
//             if (data.result == 'success') {
//                 window.location = '{{ route("admin-user") }}';
//             }
//             $btn.button('reset');
//         },
//         error:function(data) {
//             var data = data.responseJSON;
//             if (data.result) {
//                 generateNotif('topCenter', 'error', data.message);
//                 $('.form-group').addClass('has-error');
//             }

//             $.each(data,function(key, val){
//                 $('#register-'+key).addClass('has-error');
//                 $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
//             });
//             $btn.button('reset');
//         }
//     });
// });
</script>
