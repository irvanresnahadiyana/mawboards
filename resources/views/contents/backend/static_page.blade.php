@extends('layouts/backend/master/admin_template')

@section('title', 'Static Pages')
@section('page_title', 'Static Pages')
@section('page_description', 'Edit')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="#"><i class="fa fa-cog"></i> {{ trans('general.settings') }}</a></li>
        <li><a href="#"><i class="fa fa-file-o"></i> {{ trans('general.static_pages') }}</a></li>
    </ol>
@endsection

@section('content')
    @foreach($static_pages as $page)
        <form id="form-{{ $page->slug }}">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $page->title }}</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body pad">
                    <div class="form-group" id="update-content-{{ $page->slug }}">
                        <input type="hidden" name="id" value="{{ $page->id }}">
                        <textarea class="textarea" name="content" id="content-{{ $page->slug }}" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                            {{ $page->content }}
                        </textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" id="button-{{ $page->slug }}">{{ trans('general.button.update') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endforeach
@endsection

@section('script')
    <script type="text/javascript">
        $('#button-about-us').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.ajax({
                type:'post',
                url:'{{ route("update-setting-frontend-static-pages") }}',
                data: $('#form-about-us').serialize(),
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    var data = data.responseJSON;
                    $.each(data,function(key, val){
                        $('#update-content-about-us').addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#content-about-us'));
                    });
                    $btn.button('reset');
                }
            });
        });

        $('#button-terms').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.ajax({
                type:'post',
                url:'{{ route("update-setting-frontend-static-pages") }}',
                data: $('#form-terms').serialize(),
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    var data = data.responseJSON;
                    $.each(data,function(key, val){
                        $('#update-content-terms').addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#content-terms'));
                    });
                    $btn.button('reset');
                }
            });
        });

        $('#button-faq').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.ajax({
                type:'post',
                url:'{{ route("update-setting-frontend-static-pages") }}',
                data: $('#form-faq').serialize(),
                dataType: 'json',
                success:function(data) {
                    location.reload();
                },
                error:function(data) {
                    var data = data.responseJSON;
                    $.each(data,function(key, val){
                        $('#update-content-faq').addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#content-faq'));
                    });
                    $btn.button('reset');
                }
            });
        });
    </script>
@endsection
