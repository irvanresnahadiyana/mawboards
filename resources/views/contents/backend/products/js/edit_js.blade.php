<script type="text/javascript">
    var input_id = 0;
    var total_images = 0;
    var y = 0;
    $(document).ready(function(){
        $('#category.select2').select2({
            placeholder:'Category',
            allowClear:true
        });
        $('#category_child.select2').select2({
            placeholder:'Category Child',
            allowClear:true
        });

        var max_fields            = 5; //maximum input box allowed
        var add_button_price      = $('.add-more-wholesale-price');
        var add_button_image      = $('.add-more-product-image');

        var total_wholesales_price = "{{count($product->wholesales_prices)}}";
        total_images = "{{count($product->images)}}";

        // add more for saprotan .start here
        var x = ((total_wholesales_price == 0) ? 1 : total_wholesales_price); //initlal text box count

        $(add_button_price).click(function(e) { //on add input button click
            e.preventDefault();
            if(x < max_fields) { //max input box allowed
                x++; //text box increment
                $(".append-wholesale-price").append('<div class="form-group row"><div class="col-sm-2"></div><div class="col-sm-3">'
                                +'<input type="text" name="wholesale_price[]" class="form-control wholesale-price-number" placeholder="Price">'
                            +'</div>'
                            +'<div class="col-sm-3">'
                                +'<input type="text" name="quantity[]" class="form-control wholesale-price-number" placeholder="Quantity">'
                            +'</div>'
                            +'<div class="col-sm-3">'
                                +'<label><input type="checkbox" name="is_active['+(x-1)+']" class="minimal"> Is Active</label>'
                            +'</div>'
                            +'<div class="col-sm-1" style="padding: 5px 0 0 0;">'
                              +'<button class="btn btn-sm btn-danger remove"><i class="fa fa-trash"></i></button>'
                            +'</div></div>'); //add
            } else
            if(x = max_fields) {
                generateNotif('topRight', 'error', 'Maximum 5 allowed');
            }
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });
            $('.wholesale-price-number').addClass('number_only');
        });

        $(".append-wholesale-price").on("click",".remove", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
        });


        y = (total_images == 0 ? 0 : total_images); //initlal text box count

        $(add_button_image).click(function(e) { //on add input button click
            e.preventDefault();
            // if(y < max_fields) { //max input box allowed
                y++; //text box increment
                $(".append-product-image").append('<div class="col-md-3">'
                                  +'<div class="box box-widget widget-user box-product img-wrap">'
                                    +'<span class="close">&times;</span>'
                                    +'<img src="https://www.justpro.co/img/no-image.png" class="img-responsive take-image image-file-'+y+'" data-id="'+y+'">'
                                    +'<input type="file" name="product_image[]" class="input-file file-input-'+y+'" accept="image/*">'
                                    +'<div class="box-body">'
                                      +'<div class="row">'
                                        +'<div class="col-sm-12">'
                                            +'<center><label><input type="radio" name="product_image_active_temp" value="'+(y-1)+'" '+((y==1) ? "checked" : "")+'  class="minimal"> Set Primary</label></center>'
                                        +'</div>'
                                      +'</div>'
                                    +'</div>'
                                  +'</div>'
                                +'</div>'); //add
            // } else {}
            // if(y = max_fields) {
            //     generateNotif('topRight', 'error', 'Maximum 5 allowed');
            // }
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });
        });

        $(".append-product-image").on("click",".close", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); y--;
        });

        $('.img-wrap [type="file"]').show();
    });

    $(document).on('click','.take-image',function(){
        var id = $(this).data('id');
        input_id = id;
        $('.file-input-'+id).click();
    });

    function readURLAvatar(input,image) {
        if (input.files && input.files[0]) {

          var reader = new FileReader();

          reader.onload = function (e) {
            image.attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change',".input-file",function(){
        var image = $('.image-file-'+input_id);

        readURLAvatar(this,image);
    });

    $('#update-product').submit(function(event){
        event.preventDefault();
        var radioButtons = $("#update-product input:radio[name='product_image_active_temp']");
        var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));
        if (selectedIndex == -1) {
            generateNotif('topCenter', 'warning', '<i class="fa fa-error" aria-hidden="true"></i> Please set one image as primary');
        } else {
            $('#product-image-active').val(selectedIndex);

            var btn = $('#submit-update').button('loading');
            var formData = new FormData(this);

            postWithAjax('update','{{ route("admin-product-update") }}',formData,btn,'#product-');
        }

    });

    $(document).on('click','.delete-image',function(){
        var id = $(this).data('id');
        var element = $(this);
        $.ajax({
            url: "{{route('admin-product-delete-image')}}",
            method: "post",
            data: {"id":id},
            dataType: "json",
            success:function(data){
                if (data.result) {
                    element.parent('div').parent('div').remove();
                }
            }
        });
    });

    $(document).on('change','#category',function(){
        $('#category_child').parent('div').parent('div').removeClass('has-error');
        $('#category_child').next('span.help-block').remove();
        $('#category_child').empty();
        var id = $(this).val();
        $.ajax({
            url: "{{route('admin-product-category-get-child')}}",
            method: "get",
            data: {"slug":id},
            dataType: "json",
            success: function(data) {
                if (data.result == 'success') {
                    $('#has-child').val('yes');
                    var htmlChild = '<option></option>';
                    $('#category_child').attr('disabled',false);
                    $('#category_child').attr('required');
                    data.data.forEach(function(value){
                        htmlChild += '<option value="'+value.id+'">'+value.name+'</option>';
                    });
                    $('#category_child').html(htmlChild);
                } else {
                    $('#has-child').val('false');
                    $('#category_child').attr('disabled',true);
                    $('#category_child').removeAttr('required');
                }
            },
            error: function(data) {
                $('#has-child').val('false');
                $('#category_child').attr('disabled',false);
                $('#category_child').removeAttr('required');
                var data = data.responseJSON;
                if (data.result === 'Error') {
                    generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                } else {
                    generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> Oops! Somethings Wrong');
                }
            }
        });
    });
</script>
