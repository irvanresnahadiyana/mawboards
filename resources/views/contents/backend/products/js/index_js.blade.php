<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#product-table').DataTable();
        table.destroy();

        $('#product-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-product-datatable") }}',
            order: [ 5, 'ASC' ],
            columns: [
                {data: 'full_name', name: 'full_name'},
                {data: 'name', name: 'products.name'},
                {data: 'slug', name: 'products.slug'},
                {data: 'product_category_name', name: 'product_categories.name'},
                {data: 'is_publish', name: 'products.is_publish'},
                {data: 'updated_at', name: 'product_categories.updated_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('click','.refresh-datatable',function(){
        loadData();
    });

    $(document).on('click','.actPublish',function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        var status = $(this).data('status');
        
        $('#submit-publish').removeData('id');
        $('#submit-publish').removeData('type');

        $('#submit-publish').attr('data-id',id);
        
        if (status) {
            $('.modal-title-publish').html('Unpublish Product');
            $('.text-publish-product').html('Unpublish product <i>'+name+'</i> ?');
            $('#submit-publish').html('Unpublish');
            $('#submit-publish').attr('data-type','unpublish');
        } else {
            $('.modal-title-publish').html('Publish Product');
            $('.text-publish-product').html('Publish product <i>'+name+'</i> ?');
            $('#submit-publish').html('Publish');
            $('#submit-publish').attr('data-type','publish');
        } 
    });

    $(document).on('click','#submit-publish',function(){
        var type = $(this).data('type');
        var id = $(this).data('id');

        $.ajax({
            url: "{{route('admin-product-publish-unpublish')}}",
            method: "post",
            dataType: "json",
            data: {"id":id,"type":type},
            beforSend:function(data){
                $('#submit-publish').attr('disabled','disabled');
            },
            complete:function(data){
                $('#submit-publish').removeAttr('disabled');
                $('#modal-form-publish').modal('toggle');
            },
            success:function(data){
                generateNotif('topCenter', data.result, '<i class="fa fa-'+data.result+'" aria-hidden="true"></i> '+data.message);
                loadData();
            },
            error:function(data){
                var data = data.responseJSON;
                generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
            }
        });
    });

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });

    $(document).on('submit','#delete-product',function(event){
        event.preventDefault();
        var btn = $('.actDelete').button('loading');
        var formData = new FormData(this);

        var destroy = postWithAjax("delete","{{ route('admin-product-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }
</script>