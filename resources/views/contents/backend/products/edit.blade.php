@extends('layouts/backend/master/admin_template')

@section('title', 'Product')
@section('page_title', 'Edit Product')
@section('page_description', 'Edit Product')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{ route('admin-product') }}"><i class="fa fa-cubes"></i> Products</a></li>
        <li>Edit Product</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="update-product">
                    <div class="box-body">
                        <div class="error"></div>

                        <input type="hidden" name="id_product" value="{{$product->id}}">

                        <div class="form-group" id="product-category">
                            <label class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-9">
                                <select class="form-control select2" placeholder="Category" name="category" id="category">
                                    <option></option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{($product->product_category_id == $category->id) ? 'selected' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="has_child" id="has-child" value="{{$product->product_category_child_id ? 'yes' : 'no'}}">
                        <div class="form-group" id="product-category_child">
                            <label class="col-sm-2 control-label">Category Child</label>
                            <div class="col-sm-9">
                                <select class="form-control select2" placeholder="Category Child" name="category_child" id="category_child" {{count($categories_childs) > 0 ? '' : 'disabled'}}>
                                    <option></option>
                                    @if(count($categories_childs) > 0)
                                        @foreach($categories_childs as $category_child)
                                            <option value="{{$category_child->id}}" {{($product->product_category_child_id == $category_child->id) ? 'selected' : ''}}>{{$category_child->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="product-product_name">
                            <label class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="product_name" class="form-control" placeholder="Input product name" id="product_name" value="{{$product->name}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-9">
                                <input type="text" name="price" class="form-control number_only" placeholder="Input price" value="{{$product->price}}">
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-sm-2 control-label">Wholesales Price</label>
                            @if(count($product->wholesales_prices) > 0)
                                @foreach($product->wholesales_prices as $key => $wholesalesPrice)
                                    @if($key == 0)
                                        <div class="col-sm-3">
                                            <input type="text" name="wholesale_price[]" class="form-control number_only" placeholder="Price" value="{{$wholesalesPrice->price}}">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="quantity[]" class="form-control number_only" placeholder="Quantity" value="{{$wholesalesPrice->quantity}}">
                                        </div>
                                        <div class="col-sm-3">
                                            <label><input type="checkbox" name="price_active[{{$key}}]" class="minimal"{{($wholesalesPrice->is_active == true) ? 'checked' : ''}}> Is Active</label>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                <div class="col-sm-3">
                                    <input type="text" name="wholesale_price[]" class="form-control number_only" placeholder="Price">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="quantity[]" class="form-control number_only" placeholder="Quantity">
                                </div>
                                <div class="col-sm-3">
                                    <label><input type="checkbox" name="price_active[]" class="minimal"> Is Active</label>
                                </div>
                            @endif
                            <div class="col-sm-1" style="padding: 5px 0 0 0;">
                              <button class="btn btn-sm btn-primary add-more-wholesale-price"><i class="fa fa-plus"></i></button>
                            </div>

                        </div>
                        <div class="append-wholesale-price">
                            @if(count($product->wholesales_prices) > 0)
                                @foreach($product->wholesales_prices as $key => $wholesalesPrice)
                                    @if($key > 0)
                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-3">
                                                <input type="text" name="wholesale_price[]" class="form-control number_only" placeholder="Price" value="{{$wholesalesPrice->price}}">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="quantity[]" class="form-control number_only" placeholder="Quantity" value="{{$wholesalesPrice->quantity}}">
                                            </div>
                                            <div class="col-sm-3">
                                                <label><input type="checkbox" name="price_active[{{$key}}]" class="minimal"{{($wholesalesPrice->is_active == true) ? 'checked' : ''}}> Is Active</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div> -->

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Description</label>
                          <div class="col-sm-9">
                            <textarea class="textarea" name="description">{{$product->description}}</textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Product Image</label>
                          <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary add-more-product-image">Add More Image</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="append-product-image">
                                    @if(count($product->images) > 0)
                                        @foreach($product->images as $key => $image)
                                            <div class="col-md-3">
                                                <div class="box box-widget widget-user box-product img-wrap">
                                                    <span class="delete-image" data-id="{{$image->id}}">&times;</span>
                                                    @if($image->image_standard)
                                                        <img src="{{get_file($image->image_standard,'thumbnail')}}" class="img-responsive take-image image-file-{{$key+1}}" data-id="{{$key+1}}">
                                                    @else
                                                        <img src="https://www.justpro.co/img/no-image.png" class="img-responsive take-image image-file-{{$key+1}}" data-id="{{$key+1}}">
                                                    @endif
                                                    <input type="file" name="product_image[]" class="input-file file-input-{{$key+1}}" accept="image/*">
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <center>
                                                                    <label><input type="radio" name="product_image_active_temp" value="{{$key}}" {{($image->is_primary) ? 'checked' : ''}}  class="minimal"> Set Primary</label>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="product_image_old[]" value="{{$image->image_standard}}">
                                                <input type="hidden" name="product_image_old_id[]" value="{{$image->id}}">
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Stock</label>
                            <div class="col-sm-9">
                                <input type="text" name="stock" class="number_only form-control" placeholder="Stock" value="{{$product->stock}}">
                            </div>
                        </div>

                        <div class="dimension">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Dimension</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="weight" class="form-control" placeholder="Weight" value="{{$product->weight}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="length" class="form-control" placeholder="Length" value="{{$product->length}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-2">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="width" class="form-control" placeholder="Width" value="{{$product->width}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="height" class="form-control" placeholder="Height" value="{{$product->height}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Is Active</label>
                            <div class="col-sm-9">
                                <input type="checkbox" name="is_active" class="minimal" {{($product->is_active == true) ? 'checked' : ''}}>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="product_image_active" id="product-image-active">

                    <div class="box-footer">
                        <a href="{{ route('admin-product') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-update" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.products.js.edit_js')
@endsection
