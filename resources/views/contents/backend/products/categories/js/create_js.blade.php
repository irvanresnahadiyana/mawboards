<script type="text/javascript">
    $(document).ready(function(){
    });

    $(document).on('click','.add-child',function(e) { //on add input button click
        e.preventDefault();
            $(".append-child-more").append('<div class="form-group">'
                +'<div class="col-sm-4"></div>'
                    +'<div class="col-sm-4">'
                        +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name">'
                    +'</div>'
                    +'<div class="col-sm-2">'
                        +'<button type="button" class="btn btn-danger btn-sm remove-child"><i class="fa fa-trash"></i></button>'
                    +'</div>'
                +'</div>'); //add
    });

    $(".append-child-more").on("click",".remove-child", function(e){ //user click on remove
        e.preventDefault(); $(this).parent('div').parent('div').remove();;
    });

    $('#parent-category').on('ifChecked', function(event){
        $('.append-child').empty();
        $('.append-child-more').empty();
        $('.append-child').html('<div class="form-group">'
            +'<label class="col-sm-4 control-label">Child</label>'
                +'<div class="col-sm-4">'
                    +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name" required>'
                +'</div>'
                +'<div class="col-sm-2">'
                    +'<button type="button" class="btn btn-primary btn-sm add-child"><i class="fa fa-plus"></i></button>'
                +'</div>'
            +'</div>');
    });

    $('#parent-category').on('ifUnchecked', function(event){
        $('.append-child').empty();
        $('.append-child-more').empty();
    });

    $(document).on('submit','#create-product-category',function(event){
        event.preventDefault();
        var btn = $('#submit-save').button('loading');
        var formData = new FormData(this);

        postWithAjax('store',"{{ route('admin-product-category-store') }}",formData,btn,"#product-category-");
    });
</script>
