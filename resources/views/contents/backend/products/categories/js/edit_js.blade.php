<script type="text/javascript">
    var childCount = 0;
    var childs = [];
    $(document).ready(function(){
        if ($('#childs').val() != undefined) {
            childs = JSON.parse($('#childs').val());
            childCount = childs.length;
        }
    });

    $(document).on('click','.add-child',function(e) { //on add input button click
        e.preventDefault();
            $(".append-child-more").append('<div class="form-group">'
                +'<div class="col-sm-4"></div>'
                    +'<div class="col-sm-4">'
                        +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name">'
                    +'</div>'
                    +'<div class="col-sm-2">'
                        +'<button type="button" class="btn btn-danger btn-sm remove-child"><i class="fa fa-trash"></i></button>'
                    +'</div>'
                +'</div>'); //add
    });

    $(".append-child-more").on("click",".remove-child", function(e){ //user click on remove
        e.preventDefault(); $(this).parent('div').parent('div').remove();;
    });

    $('#parent-category').on('ifChecked', function(event){
        $('.append-child-more').empty();
        $('.append-child').empty();
        if (childCount == 0) {
            $('.append-child').html('<div class="form-group">'
                +'<label class="col-sm-4 control-label">Child</label>'
                    +'<div class="col-sm-4">'
                        +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name" required>'
                    +'</div>'
                    +'<div class="col-sm-2">'
                        +'<button type="button" class="btn btn-primary btn-sm add-child"><i class="fa fa-plus"></i></button>'
                    +'</div>'
                +'</div>');
        } else {
            var childContent = '';

            childs.forEach(function(data,key){
                if (key == 0) {
                    childContent += '<div class="form-group">'
                        +'<label class="col-sm-4 control-label">Child</label>'
                            +'<div class="col-sm-4">'
                                +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name" value="'+data.name+'">'
                            +'</div>'
                            +'<div class="col-sm-2">'
                                +'<button type="button" data-id="'+data.id+'" class="btn btn-danger btn-sm delete-child"><i class="fa fa-trash"></i></button>'
                            +'</div>'
                        +'</div>';
                } else {
                    childContent += '<div class="form-group">'
                        +'<div class="col-sm-4"></div>'
                            +'<div class="col-sm-4">'
                                +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name" value="'+data.name+'">'
                            +'</div>'
                            +'<div class="col-sm-2">'
                                +'<button type="button" data-id="'+data.id+'" class="btn btn-danger btn-sm delete-child"><i class="fa fa-trash"></i></button>'
                            +'</div>'
                        +'</div>';
                }
            });

            childContent += '<div class="form-group">'
                +'<div class="col-sm-4"></div>'
                    +'<div class="col-sm-4">'
                        +'<input type="text" name="child[]" class="form-control" placeholder="Input child category name">'
                    +'</div>'
                    +'<div class="col-sm-2">'
                        +'<button type="button" class="btn btn-primary btn-sm add-child"><i class="fa fa-plus"></i></button>'
                    +'</div>'
                +'</div>';

            $('.append-child').html(childContent)
        }
    });

    $('#parent-category').on('ifUnchecked', function(event){
        $('.append-child').empty();
        $('.append-child-more').empty();
    });

    $(document).on('submit','#edit-product-category',function(event){
        event.preventDefault();
        var btn = $('#submit-update').button('loading');
        var formData = new FormData(this);

        postWithAjax("edit","{{ route('admin-product-category-update') }}",formData,btn,"#product-category-");
    });

    $(document).on('click','.delete-child',function(){
        var id = $(this).data('id');
        var element = $(this);
        $.ajax({
            url: "{{route('admin-product-category-child-delete')}}",
            method: "post",
            data:{"id":id},
            dataType: "json",
            success: function(data){
                if (data.result == 'success') {
                    var is_child = element.parent('div').prev().prev()[0].firstChild.innerHTML == 'Child' ? true : false;
                    element.parent('div').parent('div').remove();
                    childs = data.data;
                    childCount = data.total;
                    if (data.total == 0) {
                        $('.add-child').parent('div').prev().prev().html('<label class="control-label col-sm-4">Child</label>');
                        $('.add-child').parent('div').prev().html('<input type="text" name="child[]" class="form-control" placeholder="Input child category name" required>')
                    } else {
                        if (is_child) {
                            $('.delete-child').parent('div').prev().prev().first().html('<label class="control-label col-sm-4">Child</label>');
                        }
                    }
                }
            }
        });
    });
</script>
