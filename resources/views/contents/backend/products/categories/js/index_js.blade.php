<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#product-category-table').DataTable();
        table.destroy();

        $('#product-category-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-product-category-datatable") }}',
            columns: [
                {data: 'parent', name: 'product_categories.name', className: 'child-category'},
                {data: 'is_parent', name: 'product_categories.is_parent', className: 'child-category'},
                {data: 'parent_slug', name: 'product_categories.slug', searchable: false, orderable: false, className: 'child-category'},
                {data: 'created_at', name: 'product_categories.created_at', className: 'child-category'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }

    $(document).on('click', '#product-category-table tbody tr td.child-category', function () {
        var table = $('#product-category-table').DataTable();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var slug = tr[0]['cells'][2]['innerText'];
        var is_parent = tr[0]['cells'][1]['innerText'];
        
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            if (is_parent == 'true') {
                $.ajax({
                    url: "{{route('admin-product-category-get-child')}}",
                    method: "get",
                    dataType: "json",
                    data: {'slug':slug},
                    success: function(data) {
                        if (data.result == 'success') {
                            row.child( childCategoryTemplate(data.data) ).show();
                            tr.addClass('shown');
                        }
                    }
                });
            }
        }
    });

    function childCategoryTemplate ( d ) {
        var table = '<table class="table">'+
            '<thead>'+
                '<tr>'+
                    '<td><label>Child Name</label></td>'+
                    '<td><label>Child Slug</label></td>'+
                '</tr>'+
            '</thead>'+
            '<tbody>';
            d.forEach(function(val){
                table += '<tr>'+
                        '<td>'+val.name+'</td>'+
                        '<td>'+val.slug+'</td>'+
                    '</tr>';
            });
                
            table += '</tbody>'+
            '</table>';
        return table;
    }

    $(document).on('submit','#delete-product-category',function(event){
        event.preventDefault();
        var btn = $('#submit-delete').button('loading');
        var formData = new FormData(this);
        // console.log(formData);

        var destroy = postWithAjax("delete","{{ route('admin-product-category-delete') }}",formData,btn,"");
    });

    function resultAjaxDelete(data) {
        $('#modal-form').modal('toggle');
        if (data.result == 'success') {
            loadData();
        }
    }

    $(document).on('click','.actDelete',function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });

</script>
