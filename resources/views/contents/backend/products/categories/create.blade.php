@extends('layouts/backend/master/admin_template')

@section('title', 'Create Product Category')
@section('page_title', 'Create Product Category')
@section('page_description', 'Create New Product Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{ route('admin-product-category') }}">Product Category</a></li>
        <li>Create Product Category</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="create-product-category">
                    <div class="box-body">
                        <div class="error"></div>

                        <div class="form-group" id="product-category-is_parent">
                            <label class="col-sm-4 control-label">Is Parent</label>
                            <div class="col-sm-4">
                                <input type="checkbox" name="is_parent" class="minimal brew" id="parent-category">
                            </div>
                        </div>

                        <div class="form-group" id="product-category-name">
                            <label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" class="form-control" placeholder="Input category name" id="name">
                            </div>
                        </div>

                        <div class="append-child"></div>
                        <div class="append-child-more"></div>
                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-product-category') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.products.categories.js.create_js')
@endsection
