@extends('layouts/backend/master/admin_template')

@section('title', 'Edit Product Category')
@section('page_title', 'Edit Product Category')
@section('page_description', 'Edit Product Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="{{ route('admin-product-category') }}">Product Category</a></li>
        <li>Edit Product Category</li>
    </ol>
@endsection

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="edit-product-category">
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{@$detail->id}}">
                        <div class="error"></div>

                        <div class="form-group" id="product-category-is_parent">
                            <label class="col-sm-4 control-label">Is Parent</label>
                            <div class="col-sm-4">
                                <input type="checkbox" name="is_parent" class="minimal brew" id="parent-category" {{$detail->is_parent ? 'checked' : ''}}>
                            </div>
                        </div>

                        <div class="form-group" id="product-category-name">
                            <label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" class="form-control" placeholder="Input category name" id="name" value="{{@$detail->name}}">
                            </div>
                        </div>

                        <div class="append-child">
                            @if($detail->childs->count() > 0)
                                <input type="hidden" value="{{$detail->childs->toJson()}}" id="childs">
                                @foreach($detail->childs as $key => $child)
                                    @if ($key == 0)
                                        <div class="form-group">
                                            <div class="abcd"><label class="col-sm-4 control-label">Child</label></div>
                                            <div class="col-sm-4">
                                                <input type="text" name="child[]" class="form-control" placeholder="Input child category name" value="{{@$child->name}}">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" data-id="{{$child->id}}" class="btn btn-danger btn-sm delete-child"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <div class="abcd"><div class="col-sm-4"></div></div>
                                            <div class="col-sm-4">
                                                <input type="text" name="child[]" class="form-control" placeholder="Input child category name" value="{{@$child->name}}">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" data-id="{{$child->id}}" class="btn btn-danger btn-sm delete-child"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="form-group">
                                    <div class="abcd"><div class="col-sm-4"></div></div>
                                    <div class="col-sm-4">
                                        <input type="text" name="child[]" class="form-control" placeholder="Input child category name">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-primary btn-sm add-child"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            @else
                                @if($detail->is_parent)
                                    <div class="form-group">
                                        <div class="abcd"><label class="col-sm-4 control-label">Child</label></div>
                                        <div class="col-sm-4">
                                            <input type="text" name="child[]" class="form-control" placeholder="Input child category name" required>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary btn-sm add-child"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                        <div class="append-child-more"></div>
                    </div>

                    <div class="box-footer">
                        <a href="{{ route('admin-product-category') }}" class="btn btn-default">Cancel</a>
                        <button class="btn btn-primary pull-right" id="submit-update" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @include('contents.backend.products.categories.js.edit_js')
@endsection
