@extends('layouts/backend/master/admin_template')

@section('title', 'Product')
@section('page_title', 'Product')
@section('page_description', 'Products')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li>Product</li>
    </ol>
@endsection

@section('content')
    <!-- Info boxes -->
  <div class="row">
    <div class="col-md-12">
      <a href="{{route('admin-product-create')}}" class="btn btn-info">Create New Product</a>
      <button class="btn btn-default btn-small refresh-datatable" title="Refresh"><i class="fa fa-refresh"></i></button>
    </div>
  </div>
  <br>
  <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <table id="product-table" class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Category</th>
                        <th>Published</th>
                        <th>Updated at</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- modal delete .start here -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalLabel">Delete Product</h4>
                </div>
                <form id="delete-product" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" class="form-control" id="id">
                        </div>
                        <p>Are you sure delete this product?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" title="Cancel">Cancel</button>
                        <input type="submit" id="submit-delete" class="btn btn-primary" title="Delete" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal delete .end here -->

    <!-- modal publish .start here -->
    <div class="modal fade" id="modal-form-publish" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title-publish" id="ModalLabel">Publish Product</h4>
                </div>
                <form id="delete-product" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" class="form-control" id="id">
                        </div>
                        <p class="text-publish-product">Are you sure delete this product?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" title="Cancel">Cancel</button>
                        <button type="button" id="submit-publish" class="btn btn-primary" title="Publish">Publish</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal publish .end here -->
@endsection

@section('script')
    @include('contents.backend.products.js.index_js')
@endsection
