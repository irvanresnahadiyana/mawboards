@extends('layouts/backend/master/admin_template')

@section('title', 'Role')
@section('page_title', 'Role')
@section('page_description', 'Role')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li>Product</li>
    </ol>
@endsection

@section('content')
    <!-- Info boxes -->
  <div class="row">
    <div class="col-md-12">
      <a href="{{route('admin-role-create')}}" class="btn btn-info">Create New Role</a>
    </div>
  </div>
  <br>
  <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <table id="role-table" class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th>Slug</th>
                        <th>Updated at</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- modal delete .start here -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalLabel">Delete Role</h4>
                </div>
                <form id="delete-role" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" class="form-control" id="id">
                        </div>
                        <p>Are you sure delete this product?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" title="Cancel">Cancel</button>
                        <input type="submit" id="submit-delete" class="btn btn-primary" title="Delete" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal delete .end here -->
@endsection

@section('script')
    @include('contents.backend.role.js.index_js')
@endsection
