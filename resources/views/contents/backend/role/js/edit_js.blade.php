<script type="text/javascript">
    var input_id = 0;
    $(document).ready(function(){
        var max_fields            = 5; //maximum input box allowed
        var add_button_price      = $('.add-more-wholesale-price');
        var add_button_image      = $('.add-more-product-image');

        var total_wholesales_price = "{{count($product->wholesales_prices)}}";
        var total_images = "{{count($product->images)}}";

        // add more for saprotan .start here
        var x = ((total_wholesales_price == 0) ? 1 : total_wholesales_price); //initlal text box count

        $(add_button_price).click(function(e) { //on add input button click
            e.preventDefault();
            if(x < max_fields) { //max input box allowed
                x++; //text box increment
                $(".append-wholesale-price").append('<div class="form-group row"><div class="col-sm-2"></div><div class="col-sm-3">'
                                +'<input type="text" name="wholesale_price[]" class="form-control wholesale-price-number" placeholder="Price">'
                            +'</div>'
                            +'<div class="col-sm-3">'
                                +'<input type="text" name="quantity[]" class="form-control wholesale-price-number" placeholder="Quantity">'
                            +'</div>'
                            +'<div class="col-sm-3">'
                                +'<label><input type="checkbox" name="is_active['+(x-1)+']" class="minimal"> Is Active</label>'
                            +'</div>'
                            +'<div class="col-sm-1" style="padding: 5px 0 0 0;">'
                              +'<button class="btn btn-sm btn-danger remove"><i class="fa fa-trash"></i></button>'
                            +'</div></div>'); //add
            } else
            if(x = max_fields) {
                generateNotif('topRight', 'error', 'Maximum 5 allowed');
            }
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });
            $('.wholesale-price-number').addClass('number_only');
        });

        $(".append-wholesale-price").on("click",".remove", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
        });


        var y = (total_images == 0 ? 0 : total_images); //initlal text box count
        $(add_button_image).click(function(e) { //on add input button click
            e.preventDefault();
            // if(y < max_fields) { //max input box allowed
                y++; //text box increment
                $(".append-product-image").append('<div class="col-md-3">'
                                  +'<div class="box box-widget widget-user box-product img-wrap">'
                                    +'<span class="close">&times;</span>'
                                    +'<img src="https://www.justpro.co/img/no-image.png" class="img-responsive take-image image-file-'+y+'" data-id="'+y+'">'
                                    +'<input type="file" name="product_image[]" class="input-file file-input-'+y+'" accept="image/*">'
                                    +'<div class="box-body">'
                                      +'<div class="row">'
                                        +'<div class="col-sm-12">'
                                            +'<center><label><input type="radio" name="product_image_active" value="'+(y-1)+'" '+((y==1 && total_images==0) ? "checked" : "")+'  class="minimal"> Active</label></center>'
                                        +'</div>'
                                      +'</div>'
                                    +'</div>'
                                  +'</div>'
                                +'</div>'); //add
            // } else {}
            // if(y = max_fields) {
            //     generateNotif('topRight', 'error', 'Maximum 5 allowed');
            // }
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });
        });

        $(".append-product-image").on("click",".close", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); y--;
        });

        $('.img-wrap [type="file"]').show();
    });

    $(document).on('click','.take-image',function(){
        var id = $(this).data('id');
        input_id = id;
        $('.file-input-'+id).click();
    });

    function readURLAvatar(input,image) {
        if (input.files && input.files[0]) {

          var reader = new FileReader();

          reader.onload = function (e) {
            image.attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change',".input-file",function(){
        var image = $('.image-file-'+input_id);

        readURLAvatar(this,image);
    });

    $('#update-product').submit(function(event){
        event.preventDefault();
        var btn = $('#submit-update').button('loading');
        var formData = new FormData(this);

        postWithAjax('update','{{ route("admin-product-update") }}',formData,btn,'#product-');

    });
</script>
