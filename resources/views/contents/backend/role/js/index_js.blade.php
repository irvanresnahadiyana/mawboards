<script type="text/javascript">
    $(document).ready(function(){
        loadData();
    });

    function loadData(){
        var table = $('#role-table').DataTable();
        table.destroy();

        $('#role-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin-role-datatable") }}',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'slug', name: 'slug'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', class: 'center-align', searchable: false, orderable: false}
            ]
        });
    }
</script>
