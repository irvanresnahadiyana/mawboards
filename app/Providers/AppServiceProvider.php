<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootBackendHeaderDataComposer();
        $this->bootBackendSidebarDataComposer();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function bootBackendHeaderDataComposer()
    {
        view()->composer('layouts.backend.partials.header', function ($view) {
            $data['user_avatar'] = User::getDataAvatar();

            $view->with($data);
        });
    }

    private function bootBackendSidebarDataComposer()
    {
        view()->composer('layouts.backend.partials.sidebar', function ($view) {
            $data['user_avatar'] = User::getDataAvatar();

            $view->with($data);
        });
    }
}
