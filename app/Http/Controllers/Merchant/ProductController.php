<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\StoreProduct;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\GalleryImage;

use Exception;
use DB;
use Sentinel;

class ProductController extends Controller
{
    public function __construct(Product $product, ProductCategory $productCategory, GalleryImage $galleryImage)
    {
        $this->product = $product;
        $this->productCategory = $productCategory;
        $this->galleryImage = $galleryImage;
    }

    public function index(Request $request)
    {
    	try {
            if (!$request->ajax()) {
    		  return view()->make('contents.merchant.products.index');
            } else {
                $user = Sentinel::getUser();
                $products = $this->product->getProductsByUserId($user->id);
                $result['result'] = 'success';
                $result['message'] = 'Success';
                $result['data']['count'] = count($products);
                $result['data']['view'] = view()->make('contents.merchant.products.list_product',
                        [
                            'products' => $products
                        ]
                    )->render();
                return json_encode($result);
            }
    	} catch (Exception $e) {
            return errorException($e);
    	}
    }

    public function create()
    {
        try {
            $categories = $this->productCategory->all();
            return view()->make('contents.merchant.products.create',
                [
                    'categories' => $categories
                ]
            );
        } catch (Exception $e) {

        }
    }

    public function getProducts()
    {
        try {

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Store Product]
     * @param  StoreProduct $request [description]
     * @return [type]                [description]
     */
    public function storeProduct(StoreProduct $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $productStore = $this->product->storeProduct($request);

            $result['result'] = 'success';
            $result['message'] = 'data has been saved';
            $result['redirect'] = route('merchant-product');
            return response()->json($result,200);

        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * [Update Product]
     * @param  StoreProduct $request [description]
     * @return [type]                [description]
     */
    public function updateProduct(StoreProduct $request)
    {
        try {
            // print_r($request->all()); die();
            $productUpdate = $this->product->updateProduct($request);

            $result['result'] = 'success';
            $result['message'] = 'data has been updated';
            $result['redirect'] = route('merchant-product');
            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Delete Product]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function deleteProduct(Request $request)
    {
        try {

            $productDetail = $this->product->find($request->id);

            if ($productDetail) {
                if ($productDetail->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Product has been deleted';
                    $result['redirect'] = route('merchant-product');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Product has been failed';
                    $result['redirect'] = route('merchant-product');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Product not found or has been deleted';
                $result['redirect'] = route('merchant-product');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Edit Product]
     * @param  $id
     * @return [type]                [description]
     */
    public function edit($id)
    {
        try {

            $productDetail = $this->product->find($id);
            if ($productDetail) {
                $categories = $this->productCategory->all();
                $categoryDetail = $this->productCategory->find($productDetail->product_category_id);
                if ($categoryDetail->childs->count() > 0) {
                    $categories_childs = $categoryDetail->childs;
                } else {
                    $categories_childs = [];
                }
                return view()->make('contents.merchant.products.edit',
                        [
                            'product' => $productDetail,
                            'categories' => $categories,
                            'categories_childs' => $categories_childs
                        ]
                    );

            } else {
                return response()->view('errors.503');
            }

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Delete Image Product]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function deleteImageProduct(Request $request)
    {
        DB::beginTransaction();
        try {
            $deleteImage = $this->galleryImage->deleteImageById($request->input('id'));
            DB::commit();
            return response()->json(['result'=>$deleteImage],200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }
}
