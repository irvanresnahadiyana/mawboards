<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use DB;

use App\Models\ProductCategory;
use App\Models\ProductCategoryChild;

class ProductCategoryController extends Controller
{
    public function __construct(ProductCategory $productCategory, ProductCategoryChild $childCategory)
    {
        $this->productCategory = $productCategory;
        $this->child = $childCategory;
    }

    public function getCategoryChild(Request $request)
    {
        try {
            // if (is_numeric($request->get('slug'))) {
                $detailCategory = $this->productCategory->find($request->get('id'));
            // } else {
            //     $detailCategory = $this->productCategory->findBySlug($request->get('slug'));
            // }

            if ($detailCategory) {
                // if ($request->ajax()) {
                    if ($detailCategory->childs->count() > 0) {
                        $result['result'] = 'success';
                        $result['message'] = 'Data available';
                        $result['data'] = $detailCategory->childs;
                    } else {
                        $result['result'] = 'warning';
                        $result['message'] = 'Data not available';
                        $result['data'] = [];
                    }
                // } else {
                //     $result['result'] = 'success';
                //     $result['message'] = 'Data available';
                //     $result['data'] = $detailCategory->childs;
                // }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category not available';
                $result['data'] = [];
            }

            // if ($request->ajax()) {
                return response()->json($result,200);
            // } else {
            //     return $result;
            // }
        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
