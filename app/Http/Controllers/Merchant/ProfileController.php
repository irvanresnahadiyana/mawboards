<?php

namespace App\Http\Controllers\Merchant;

use DB;
use Exception;
use Sentinel;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateProfile;

class ProfileController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index() {
        $data['user_avatar'] = User::getDataAvatar();

        return view('contents.backend.profile')->with($data);
    }

    public function updateProfile(UpdateProfile $request) {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->user->updateUser($request);

            session()->flash('status', trans("general.notification.update_success"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("general.notification.update_success"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }
}
