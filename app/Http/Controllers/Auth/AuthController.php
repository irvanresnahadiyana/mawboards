<?php

namespace App\Http\Controllers\Auth;

use DB;
use Exception;
use Activation;
use Sentinel;
use Mail;
use Socialite;
use Reminder;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Login;
use App\Http\Requests\Auth\Register;
use App\Http\Requests\Auth\RegisterMerchant;
use App\Http\Requests\Auth\ForgotPassword;
use App\Http\Requests\Auth\ChangePassword;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * [login description]
     * @return [type] [description]
     */
    public function login()
    {
        return view('contents.backend.auth.login');
    }

    /**
     * [loginPost description]
     * @param  LoginRequest $request [description]
     * @return [type]                [description]
     */
    public function loginPost(Login $request)
    {
        $find_user = Sentinel::findByCredentials($request->only('email'));
        if (! $find_user) {
            return response()->json([
                'result' => 'Error',
                'message' => trans('passwords.user'),
            ], 400);
        }

        try {
            $remember = (bool)$request->remember;
            if (!$user = Sentinel::authenticate($request->all(), $remember)) {
                return response()->json([
                    'result' => 'Error',
                    'message' => trans('auth.failed'),
                ], 400);
            }

            // session()->flash('status', 'Login success!');

            if ($user->inRole('administrator')) {
                $redirect = route("admin-dashboard");
            } elseif ($user->inRole('merchant')) {
                $redirect = route("merchant-dashboard");
            } else {
                $request->session()->put('user-status', 'user-login');
                $redirect = route("index");
            }

            return response()->json([
                'result' => 'Success',
                'message' => 'Login success!',
                'redirect' => $redirect,
            ], 200);
        } catch (ThrottlingException $e) {
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
            ], 400);
        } catch (NotActivatedException $e) {
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * [socialiteRedirect description]
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function socialiteRedirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * [socialiteLogin description]
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function socialiteLogin($provider)
    {
        $user = Socialite::driver($provider)->user();
        $login = $user->email;

        $find_user = Sentinel::findByCredentials(['login' => $login]);
        if (!$find_user) {
            $credentials = [
                'email' => $user->email,
                'password' => 12345678,
            ];

            $find_user = Sentinel::registerAndActivate($credentials);
        }

        if (Activation::completed($find_user)) {
            Sentinel::login($find_user);

            session()->flash('status', 'Login success!');

            return redirect()->route('index');
        }

        session()->flash('status', trans("auth.not_activated"));

        return redirect('/');
    }

    /**
     * [logout description]
     * @return [type] [description]
     */
    public function logout(Request $request)
    {
        session()->flash('status', 'Logout success!');
        $request->session()->forget('user-status');
        Sentinel::logout();

        return redirect('/');
    }

    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        return view('contents.backend.auth.register');
    }

    /**
     * [registerPost description]
     * @param  RegisterRequest $request [description]
     * @return [type]                   [description]
     */
    public function registerPost(Register $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->user->registerUser($request);

            session()->flash('status', trans("auth.activation"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("auth.activation"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    public function registerMerchantPost(RegisterMerchant $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->user->registerUser($request);

            session()->flash('status', trans("auth.activation"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("auth.activation"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * [activate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function activate(Request $request)
    {
        $user = Sentinel::findByCredentials(['login' => $request->login]);

        if (!$user) {
            session()->flash('status', trans("auth.email_not_registered"));

            return redirect('/');
        }

        if (Activation::completed($user)) {
            session()->flash('status', trans("auth.user_already_activated"));

            return redirect('/');
        }

        Activation::removeExpired();

        if (Activation::complete($user, $request->code)) {
            session()->flash('status', trans("auth.user_has_been_activated"));

            // Login
            // Sentinel::login($user);
            // $request->session()->put('user-status', 'user-login');
        } else {
            $activation = (Activation::exists($user)) ? Activation::exists($user) : Activation::create($user);

            // Send email reactivation
            $email_content = '<a href="'.route('auth-activate', ['login' => $user->email, 'code' => $activation->code ]).'">Click here</a> to activate your account.';
            Mail::send('emails.general', ['title' => trans("auth.activate"), 'content' => $email_content], function ($message) use($user)
            {
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->to($user->email);
                $message->subject(trans("auth.reactivate"));
            });

            session()->flash('status', trans("auth.reactivation"));
        }

        return redirect('/');
    }

    public function forgotPost(ForgotPassword $request)
    {
        $find_user = Sentinel::findByCredentials(['login' => $request->email]);

        if (!$find_user) {
            session()->flash('status', trans("auth.email_not_registered"));

            return response()->json([
                'result' => 'Error',
                'message' => trans('auth.email_not_registered'),
            ], 400);
        }

        Reminder::removeExpired();

        $reminder = (Reminder::exists($find_user)) ? Reminder::exists($find_user) : Reminder::create($find_user);

        // Send email activation
        $email_content = '<a href="'.route('auth-change-password', ['login' => $find_user->email, 'code' => $reminder->code ]).'">Click here</a> to change your Password.';
        Mail::send('emails.general', ['title' => trans("auth.forgot_password"), 'content' => $email_content], function ($message) use ($find_user) {
            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $message->to($find_user->email);
            $message->subject(trans("auth.forgot_password"));
        });

        session()->flash('status', trans("general.check_inbox_email"));

        return response()->json([
            'code' => 200,
            'status' => 'Success',
        ], 200);
    }

    public function reminderTokenGet()
    {
        Reminder::removeExpired();

        $user = user_info();
        $reminder = (Reminder::exists($user)) ? Reminder::exists($user) : Reminder::create($user);

        return $reminder;
    }

    public function changePassword(Request $request)
    {
        session()->flash('user-status', 'user-change-password');
        session()->flash('user-change-password.login', $request->login);
        session()->flash('user-change-password.code', $request->code);

        return redirect('/');
    }

    public function changePasswordPost(ChangePassword $request)
    {
        $user = Sentinel::findByCredentials(['login' => $request->login]);

        if (Reminder::complete($user, $request->code, $request->password)) {
            session()->flash('status', trans("passwords.change"));
        } else {
            session()->flash('status', trans("passwords.token"));
        }

        return response()->json([
            'code' => 200,
            'status' => 'Success',
        ], 200);
    }
}
