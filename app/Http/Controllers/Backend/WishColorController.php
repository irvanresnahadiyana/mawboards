<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\StoreWishColor;

use DB;
use Exception;

use App\Models\Color;

class WishColorController extends Controller
{
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    public function index()
    {
        return view()->make('contents.backend.wishes.colors.index');
    }

    public function datatable()
    {
        $data = $this->color->datatables();
        return datatables($data)
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-wish-color-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }

    public function create()
    {
        return view()->make('contents.backend.wishes.colors.create');
    }

    public function store(StoreWishColor $request)
    {
        DB::beginTransaction();
        try {
            $storeWishColor = $this->color->create($request->all());
            if ($storeWishColor) {
                DB::commit();
                $result['result'] = 'success';
                $result['message'] = 'Wish color has been created';
                $result['redirect'] = route('admin-wish-color');
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Wish color has been failed';
                $result['redirect'] = route('admin-wish-color-create');
            }
            return response()->json($result,200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    public function edit($id)
    {
        try {
            $wishColor = $this->color->find($id);
            if ($wishColor) {
                $colors = $this->color->all();
                return view()->make('contents.backend.wishes.colors.edit')->with([
                        'wishColor' => $wishColor,
                    ]);
            } else {
                return redirect()->route('admin-wish-color');
            }
        } catch (Exception $e) {
            return redirect()->route('admin-wish-color');
        }
    }

    public function update(StorewishColor $request)
    {
        try {
            $wishColorGet = $this->color->find($request->id);
            if ($wishColorGet) {
                $updateWishColor = $wishColorGet->update($request->except('id'));
                if ($updateWishColor) {
                    $result['result'] = 'success';
                    $result['message'] = 'Wish color has been updated';
                    $result['redirect'] = route('admin-wish-color');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Wish color has been failed';
                    $result['redirect'] = route('admin-wish-color-edit',array('id' => $request->id));
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Something went wrong!';
                $result['redirect'] = route('admin-wish-color');
            }
            return response()->json($result,200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $colorGet = $this->color->find($request->id);

            if ($colorGet) {
                if ($colorGet->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Wish color has been deleted';
                    $result['redirect'] = route('admin-wish-color');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Wish color has been failed';
                    $result['redirect'] = route('admin-wish-color');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Wish color not found or has been deleted';
                $result['redirect'] = route('admin-wish-color');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
