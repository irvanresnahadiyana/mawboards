<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use DB;

use App\Models\User;
use Mail;
use URL;
use Sentinel;
use Activation;
use App\Http\Requests\Admin\Register;
class UserController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        try {
            return view()->make('contents.backend.user.index');
        } catch (Exception $e) {

        }
    }

    public function create()
    {
        return view('contents.backend.user.create');
    }

    public function registerPost(Register $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->except('password_confirmation');
            $data['birthday'] = date('Y-m-d',strtotime($data['birthday']));

            DB::commit();
            $user = Sentinel::registerAndActivate($data);

            if ($user) {
                //register as User/Member
                $role = Sentinel::findRoleBySlug('user');

                // attach credential as User/Member
                $role->users()->attach($user);
                // Activation::removeExpired();
                // $activation = (Activation::exists($user)) ? Activation::exists($user) : Activation::create($user);

                // Send email activation
                // $email_content = '<a href="'.route('auth-activate', ['login' => $user->email, 'code' => $activation->code ]).'">Click here</a> to activate your account.';
                // Mail::send('emails.general', ['title' => trans("auth.activate"), 'content' => $email_content], function ($message) use($user)
                // {
                //     $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                //     $message->to($user->email);
                //     $message->subject(trans("auth.activate"));
                // });

                // session()->flash('status', trans("auth.activation"));

                $result['result'] = 'success';
                $result['message'] = 'User has been created';

                // return response()->json([
                //     'result' => 'success',
                //     'message' => 'User has been created',
                // ], 200);
            } else {
                $result['result'] = 'error';
                $result['message'] = 'User failed created';
            }

            $result['redirect'] = route('admin-user');

            return response()->json($result,200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    public function datatable()
    {
        $data = $this->user->datatables();
        return datatables($data)
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-user-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }

    public function edit($id)
    {
        try {
            $userDetail = $this->user->find($id);
            if ($userDetail) {
                return view()->make('contents.backend.user.edit')->with([
                        'userDetail' => $userDetail,
                    ]);
            } else {
                return redirect()->route('admin-user');
            }
        } catch (Exception $e) {
            return redirect()->route('admin-user');
        }
    }

    public function update(Register $request)
    {
        try {
            $userDetail = Sentinel::findById($request->id);
            if ($userDetail) {
                if ($request->password != '') {
                    $updateUser = Sentinel::update($userDetail, $request->except(['id','password_confirmation','email']));
                } else {
                    $updateUser = Sentinel::update($userDetail, $request->except(['id','password','password_confirmation','email']));
                }
                if ($updateUser) {
                    $result['result'] = 'success';
                    $result['message'] = 'User has been updated';
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Something went wrong!';
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'User not found or has been deleted';
            }

            $result['redirect'] = route('admin-user');

            return response()->json($result,200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $userGet = Sentinel::findById($request->id);

            if ($userGet) {
                if ($userGet->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'User has been deleted';
                    $result['redirect'] = route('admin-user');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Something went wrong!';
                    $result['redirect'] = route('admin-user');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'User not found or has been deleted';
                $result['redirect'] = route('admin-user');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
