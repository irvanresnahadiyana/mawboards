<?php

namespace App\Http\Controllers\Backend;

use App\Models\StaticPage;
use App\Http\Requests\Admin\UpdateStaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    public function __construct(StaticPage $model)
    {
        parent::__construct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['static_pages'] = $this->model->orderBy('id')->get();

        return view('contents.backend.static_page')->with($data);
    }

    public function updateData(UpdateStaticPage $request)
    {
        $this->model->updateData($request->all(), $request->id);

        session()->flash('status', trans("general.notification.update_success"));

        return response()->json([
            'result' => 'Success',
            'message' => trans("general.notification.update_success"),
        ], 200);
    }
}
