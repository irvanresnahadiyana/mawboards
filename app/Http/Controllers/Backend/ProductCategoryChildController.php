<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;

use App\Models\ProductCategory;
use App\Models\ProductCategoryChild;

class ProductCategoryChildController extends Controller
{
    public function __construct(ProductCategory $productCategory, ProductCategoryChild $childCategory)
    {
        $this->productCategory = $productCategory;
        $this->child = $childCategory;
    }

    public function deleteChildCategory(Request $request)
    {
        try {
            $delete = $this->child->find($request->id);

            $productCategoryId = $delete->product_category_id;

            $delete->delete();

            if ($delete) {
                $result['result'] = 'success';
                $result['message'] = 'Child category has been deleted';
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Child category failed deleted';
            }

            $result['total'] = $this->productCategory->find($productCategoryId)->childs()->count();
            $result['data'] = $this->productCategory->find($productCategoryId)->childs;

            return response()->json($result,200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
