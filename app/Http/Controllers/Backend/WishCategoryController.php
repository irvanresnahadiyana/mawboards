<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\StoreWishCategory;

use Exception;
use DB;

use App\Models\WishCategory;
use App\Models\Color;

class WishCategoryController extends Controller
{
    public function __construct(WishCategory $wishCategory, Color $color)
    {
        $this->wishCategory = $wishCategory;
        $this->color = $color;
    }

    public function index()
    {
        return view()->make('contents.backend.wishes.categories.index');
    }

    public function datatable()
    {
        $data = $this->wishCategory->datatables();
        return datatables($data)
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-wish-category-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }

    public function create()
    {
        $colorIdTaken = $this->wishCategory->pluck('color_id');
        $colors = $this->color->whereNotIn('id',$colorIdTaken)->get();
        return view()->make('contents.backend.wishes.categories.create')->with([
                'colors' => $colors,
            ]);
    }

    public function store(StoreWishCategory $request)
    {
        DB::beginTransaction();
        try {
            $storeWishCategory = $this->wishCategory->create($request->all());
            if ($storeWishCategory) {
                DB::commit();
                $result['result'] = 'success';
                $result['message'] = 'Category has been created';
                $result['redirect'] = route('admin-wish-category');
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category has been failed';
                $result['redirect'] = route('admin-wish-category-create');
            }
            return response()->json($result,200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    public function edit($id)
    {
        try {
            $wishCategory = $this->wishCategory->find($id);
            if ($wishCategory) {
                $colors = $this->color->all();
                return view()->make('contents.backend.wishes.categories.edit')->with([
                        'wishCategory' => $wishCategory,
                        'colors' => $colors,
                    ]);
            } else {
                return redirect()->route('admin-wish-category');
            }
        } catch (Exception $e) {
            return redirect()->route('admin-wish-category');
        }
    }

    public function update(StoreWishCategory $request)
    {
        try {
            $wishCategoryGet = $this->wishCategory->find($request->id);
            if ($wishCategoryGet) {
                $updateWishCategory = $wishCategoryGet->update($request->except('id'));
                if ($updateWishCategory) {
                    $result['result'] = 'success';
                    $result['message'] = 'Category has been updated';
                    $result['redirect'] = route('admin-wish-category');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Category has been failed';
                    $result['redirect'] = route('admin-wish-category-edit',array('id' => $request->id));
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Something went wrong!';
                $result['redirect'] = route('admin-wish-category');
            }
            return response()->json($result,200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $wishCategoryGet = $this->wishCategory->find($request->id);

            if ($wishCategoryGet) {
                if ($wishCategoryGet->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Category has been deleted';
                    $result['redirect'] = route('admin-wish-category');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Category has been failed';
                    $result['redirect'] = route('admin-wish-category');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category not found or has been deleted';
                $result['redirect'] = route('admin-wish-category');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
