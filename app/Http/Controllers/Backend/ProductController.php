<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProduct;

use Exception;
use DB;
use Sentinel;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\GalleryImage;

class ProductController extends Controller
{
    public function __construct(Product $product, ProductCategory $productCategory, GalleryImage $galleryImage)
    {
        $this->product = $product;
        $this->productCategory = $productCategory;
        $this->galleryImage = $galleryImage;
    }

    public function index()
    {
        try {
            return view()->make('contents.backend.products.index');
        } catch (Exception $e) {
            return response()->view('errors.503');
        }
    }

    public function datatables()
    {
        $data = $this->product->datatable();
        return datatables($data)
            ->addColumn('action', function ($data) {
                if (!$data->is_publish) {
                    $action = '<button class="btn btn-primary btn-xs actPublish" title="Publish" data-id="'.$data->id.'" data-status="'.$data->is_publish.'" data-name="'.$data->name.'" data-button="publish" data-toggle="modal" href="#modal-form-publish"><i class="fa fa-feed fa-fw"></i></button>';
                } else {
                    $action = '<button class="btn btn-primary btn-xs actPublish" title="Unpublish" data-id="'.$data->id.'" data-status="'.$data->is_publish.'" data-name="'.$data->name.'" data-button="unpublish" data-toggle="modal" href="#modal-form-publish"><i class="fa fa-feed fa-fw"></i></button>';
                }
                $action .= '&nbsp;<a href="'.route('admin-product-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';

                return $action;
            })->make(true);

    }

    public function create()
    {
        try {
            $categories = $this->productCategory->all();
            return view()->make('contents.backend.products.create',
                [
                    'categories' => $categories
                ]
            );
        } catch (Exception $e) {

        }
    }

    public function edit($id)
    {
        try {

            $productDetail = $this->product->find($id);

            if ($productDetail) {
                $categories = $this->productCategory->all();
                $categoryDetail = $this->productCategory->find($productDetail->product_category_id);
                if ($categoryDetail->childs->count() > 0) {
                    $categories_childs = $categoryDetail->childs;
                } else {
                    $categories_childs = [];
                }
                return view()->make('contents.backend.products.edit',
                        [
                            'product' => $productDetail,
                            'categories' => $categories,
                            'categories_childs' => $categories_childs
                        ]
                    );

            } else {
                return response()->view('errors.503');
            }

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function storeProduct(StoreProduct $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $productStore = $this->product->storeProduct($request);

            $result['result'] = 'success';
            $result['message'] = 'data has been saved';
            $result['redirect'] = route('admin-product');
            return response()->json($result,200);

        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * [Update Product]
     * @param  StoreProduct $request [description]
     * @return [type]                [description]
     */
     public function updateProduct(StoreProduct $request)
     {
         try {
             $productUpdate = $this->product->updateProduct($request);

             $result['result'] = 'success';
             $result['message'] = 'data has been updated';
             $result['redirect'] = route('admin-product');
             return response()->json($result,200);

         } catch (Exception $e) {
             return errorException($e);
         }
     }

     /**
     * [Delete Product]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function deleteProduct(Request $request)
    {
        try {

            $productDetail = $this->product->find($request->id);

            if ($productDetail) {
                if ($productDetail->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Product has been deleted';
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Product has been failed';
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Product not found or has been deleted';
                $result['redirect'] = route('merchant-product');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Action Publish Unpublish]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function actionPublishUnpublish(Request $request)
    {
        DB::beginTransaction();
        try {
            $productDetail = $this->product->find($request->get('id'));
            if ($productDetail) {
                $result['result'] = 'success';
                if ($request->get('type') == 'publish') {
                    $productDetail->is_publish = true;
                    $result['message'] = 'Product has been published';
                } else {
                    $productDetail->is_publish = false;
                    $result['message'] = 'Product has been unpublished';
                }
                $productDetail->save();
                DB::commit();
            } else {
                $result['result'] = 'failed';
                $result['message'] = 'Product not available';
            }

            return response()->json($result,200);
            
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * [Delete Image Product]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function deleteImageProduct(Request $request)
    {
        DB::beginTransaction();
        try {
            $deleteImage = $this->galleryImage->deleteImageById($request->input('id'));
            DB::commit();
            return response()->json(['result'=>$deleteImage],200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

}
