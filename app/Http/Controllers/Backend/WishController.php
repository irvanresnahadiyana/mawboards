<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\StoreWish;

use Exception;
use DB;

use App\Models\WishCategory;
use App\Models\Color;
use App\Models\Wish;
use App\Models\User;

class WishController extends Controller
{
    public function __construct(WishCategory $wishCategory, Color $color, Wish $wish,User $user)
    {
        $this->wishCategory = $wishCategory;
        $this->color = $color;
        $this->wish = $wish;
        $this->user = $user;
    }

    public function index()
    {
        return view()->make('contents.backend.wishes.index');
    }

    public function datatables()
    {
        $data = $this->wish->datatable();
        return datatables($data)
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-wish-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }

    public function create()
    {
        $wishCategories = $this->wishCategory->all();
        $users = $this->user->getUsers();

        return view()->make('contents.backend.wishes.create')->with([
                'wishCategories' => $wishCategories,
                'users' => $users,
            ]);
    }

    public function store(StoreWish $request)
    {
        DB::beginTransaction();
        try {

            DB::commit();
            $storeWish = $this->wish->storeWish($request,'backend');
            if ($storeWish) {
                $result['result'] = 'success';
                $result['message'] = 'Wish has been created';
                $result['redirect'] = route('admin-wish');
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Something went wrong!';
                $result['redirect'] = route('admin-wish-create');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    public function edit($id)
    {
        try {
            $wishDetail = $this->wish->find($id);
            if ($wishDetail) {
                $imageWish = $wishDetail->images->where('is_primary',true)->first();
                $wishCategories = $this->wishCategory->all();
                return view()->make('contents.backend.wishes.edit')->with([
                        'wishDetail' => $wishDetail,
                        'wishCategories' => $wishCategories,
                        'imageWish' => $imageWish,
                    ]);
            } else {
                return redirect()->route('admin-wish');
            }
        } catch (Exception $e) {
            return redirect()->route('admin-wish');
        }
    }

    public function update(StoreWish $request)
    {
        try {
            
            $updateWish = $this->wish->updateWish($request,'backend');
            if ($updateWish) {
                $result['result'] = 'success';
                $result['message'] = 'Wish has been updated';
                $result['redirect'] = route('admin-wish');
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Something went wrong!';
                $result['redirect'] = route('admin-wish-edit',array('id' => $request->id));
            }

            return response()->json($result,200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $wishGet = $this->wish->find($request->id);

            if ($wishGet) {
                if ($wishGet->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Wish has been deleted';
                    $result['redirect'] = route('admin-wish');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Something went wrong!';
                    $result['redirect'] = route('admin-wish');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Wish not found or has been deleted';
                $result['redirect'] = route('admin-wish');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
