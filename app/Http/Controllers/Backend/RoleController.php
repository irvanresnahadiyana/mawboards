<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRole;

use Exception;
use DB;

use App\Models\Role;

class RoleController extends Controller
{
    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function index()
    {
        try {
            return view()->make('contents.backend.role.index');
        } catch (Exception $e) {

        }
    }

    public function datatable()
    {
        $data = $this->role->datatables();
        return datatables($data)
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-role-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }
}
