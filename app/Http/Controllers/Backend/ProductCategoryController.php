<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\StoreProductCategory;

use Exception;
use DB;

use App\Models\ProductCategory;
use App\Models\ProductCategoryChild;

class ProductCategoryController extends Controller
{
    public function __construct(ProductCategory $productCategory, ProductCategoryChild $childCategory)
    {
        $this->productCategory = $productCategory;
        $this->child = $childCategory;
    }

    public function index()
    {
        try {
            return view()->make('contents.backend.products.categories.index');
        } catch (Exception $e) {

        }
    }

    public function datatable()
    {
        $data = $this->productCategory->datatables();
        return datatables($data)
            // ->editColumn('slug', function ($data) {
            //     if ($data->is_parent) {
            //         return $data->child_slug;
            //     } else {
            //         return $data->parent_slug;
            //     }
            // })
            ->addColumn('action', function ($data) {
                return '<a href="'.route('admin-product-category-edit',["id" => $data->id]).'" data-name="'.$data->name.'" class="btn btn-warning btn-xs edit-data" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>
                    &nbsp;<button class="btn btn-danger btn-xs actDelete" title="Delete" data-id="'.$data->id.'" data-name="'.$data->name.'" data-button="delete" data-toggle="modal" href="#modal-form"><i class="fa fa-trash-o fa-fw"></i></button>';
            })->make(true);
    }

    public function create()
    {
        return view()->make('contents.backend.products.categories.create');
    }

    /**
     * [Store Product Caategory]
     * @param  StoreProductCategory $request [description]
     * @return [type]                [description]
     */
    public function store(StoreProductCategory $request)
    {
        DB::beginTransaction();
        try {

            $is_parent = @$request->input('is_parent') ? true : false;

            DB::commit();
            $store = $this->productCategory->create([
                    'name' => $request->name,
                    'is_parent' => $is_parent,
                ]);
            if ($store) {
                if ($is_parent) {
                    foreach($request->input('child') as $child) {
                        if (!empty($child)) {
                            $this->child->create([
                                'product_category_id' => $store->id,
                                'name' => $child,
                            ]);
                        }
                    }
                }
                $result['result'] = 'success';
                $result['message'] = 'Category has been saved';
                $result['redirect'] = route('admin-product-category');
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category has been failed';
                $result['redirect'] = route('admin-product-category-create');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * [Update Product Caategory]
     * @param  StoreProductCategory $request [description]
     * @return [type]                [description]
     */
    public static function update(StoreProductCategory $request)
    {
        try {
            $is_parent = @$request->input('is_parent') ? true : false;
            $category = ProductCategory::find($request->id);

            if ($category) {
                $category->name = $request->name;
                $category->is_parent = $is_parent;
                if ($category->save()) {
                    $category->childs()->delete();
                    if ($is_parent) {
                        foreach($request->input('child') as $child) {
                            if (!empty($child)) {
                                ProductCategoryChild::create([
                                    'product_category_id' => $request->id,
                                    'name' => $child,
                                ]);
                            }
                        }
                    }
                    $result['result'] = 'success';
                    $result['message'] = 'Category has been updated';
                    $result['redirect'] = route('admin-product-category');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Category has been failed';
                    $result['redirect'] = route('admin-product-category-edit',['id' => $request->id]);
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category not found or has been deleted';
                $result['redirect'] = route('admin-product-category');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Delete Product Caategory]
     * @param  Request $request [description]
     * @return [type]                [description]
     */
    public function destroy(Request $request)
    {
        try {

            $category = $this->productCategory->find($request->id);

            if ($category) {
                if ($category->delete()) {
                    $result['result'] = 'success';
                    $result['message'] = 'Category has been deleted';
                    $result['redirect'] = route('admin-product-category');
                } else {
                    $result['result'] = 'error';
                    $result['message'] = 'Category has been failed';
                    $result['redirect'] = route('admin-product-category');
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category not found or has been deleted';
                $result['redirect'] = route('admin-product-category');
            }

            return response()->json($result,200);

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * [Edit Product Caategory]
     * @param  $id
     */
    public function edit($id)
    {
        try {
            $detail = $this->productCategory->getDetail($id);
            if ($detail) {
                return view()->make('contents.backend.products.categories.edit',
                    [
                        'detail' => $detail
                    ]
                );
            } else {
                return redirect()->route('admin-product-category');
            }

        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function getCategoryChild(Request $request)
    {
        try {
            if (is_numeric($request->get('slug'))) {
                $detailCategory = $this->productCategory->find($request->get('slug'));
            } else {
                $detailCategory = $this->productCategory->findBySlug($request->get('slug'));
            }

            if ($detailCategory) {
                if ($request->ajax()) {
                    if ($detailCategory->childs->count() > 0) {
                        $result['result'] = 'success';
                        $result['message'] = 'Data available';
                        $result['data'] = $detailCategory->childs;
                    } else {
                        $result['result'] = 'warning';
                        $result['message'] = 'Data not available';
                        $result['data'] = [];
                    }
                } else {
                    $result['result'] = 'success';
                    $result['message'] = 'Data available';
                    $result['data'] = $detailCategory->childs;
                }
            } else {
                $result['result'] = 'error';
                $result['message'] = 'Category not available';
                $result['data'] = [];
            }

            if ($request->ajax()) {
                return response()->json($result,200);
            } else {
                return $result;
            }
        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
