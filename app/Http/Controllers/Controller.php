<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Database\Eloquent\Model;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $model;

    public function __construct(Model $model = null)
    {
        $this->model = $model;
    }

    public function processGetRelated($data)
    {
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $avatar = $value->user->getDataAvatar();
                $value->avatar = link_to_avatar(@$avatar->image_thumb);

                $value->image = $value->images()->where('is_primary',true)->first();
                if (user_info()) {
                    $chkLike = $value->likes()->where('user_id',user_info('id'))->first();
                    $value->status_like = $chkLike ? $chkLike->status : false;
                }
                $value->people_likes = $value->likes()->where('status',true)->get();
                $value->type_wish = $value->hex ? 'wish' : 'product';
            }
            return $data;
        } else {
            return [];
        }
    }
}
