<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Wish;
use App\Models\WishCategory;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WishboardController extends Controller
{
    public function __construct(Wish $wish, WishCategory $wish_category, User $user)
    {
        $this->wish = $wish;
        $this->wish_category = $wish_category;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hash_id = null, $json = false)
    {
        if (!$hash_id) {
            $data['user'] = null;
            $data['title'] = 'My Wishboard';
            $data['wishboards'] = $this->wish_category->userWishboards(user_info('id'));
            $data['friend_status'] = ['status' => false, 'message' => ''];
        } else {
            $user = $this->user->find($this->user->originalId($hash_id));

            if ($user) {
                $data['user'] = $user;
                $data['title'] = $user->first_name.' '.$user->last_name.' Wishboard';
                $data['wishboards'] = $this->wish_category->userWishboards($user->id);

                $sender = user_info();
                $recipient = $data['user'];
                // check status between sender and recipient
                $check_friend_status = $sender->isFriendWith($recipient);
                if (!$check_friend_status) { // if not friend
                    // check sender has sent friend request to recipient
                    $check_sender_sent_request = $sender->hasSentFriendRequestTo($recipient);
                    if (!$check_sender_sent_request) { // if sender not sent request
                        $data['friend_status'] = ['status' => false, 'message' => ''];
                    } else {
                        $data['friend_status'] = ['status' => true, 'message' => 'Waiting Response'];
                    }
                } else {
                    $data['friend_status'] = ['status' => true, 'message' => 'isFriend'];
                }
            } else {
                session()->flash('status', trans("general.notification.data_not_found"));

                if ($json) {
                    return false;
                } else{
                    return redirect()->route('index');
                }
            }
        }

        if ($json) {
            return $data;
        }

        return view('contents.frontend.wishboards.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $this->wish_category->storeWishCategory($request);

        session()->flash('status', trans("general.notification.save_success"));

        return response()->json([
            'result' => 'Success',
            'message' => trans("general.notification.save_success"),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WishCategory  $wishCategory
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user_id = user_info('id');
        $data['user'] = false;
        $wish_category = $this->wish_category->findBySlug($slug);
        $wishes = $wish_category->wishes()->where('user_id', $user_id)->get();
        if (user_info()) {
            $data['friends'] = user_info()->getFriends();
            $data['friends_birthday'] = $data['friends']->where('birthday', '!=', null)->sortBy('birthday')->first();
        }
        $data['wish_category'] = $wish_category;
        $data['wishes'] = $this->processGetRelated($wishes);

        return view('contents.frontend.wishes.wish_category', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WishCategory  $wishCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(WishCategory $wishCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WishCategory  $wishCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WishCategory $wishCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WishCategory  $wishCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WishCategory $wishCategory)
    {
        //
    }
}
