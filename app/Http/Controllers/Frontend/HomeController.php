<?php

namespace App\Http\Controllers\Frontend;

use Validator;
use App\Models\User;
use App\Models\StaticPage;
use App\Models\Wish;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct(Wish $wish, User $user, Product $product)
    {
        $this->wish = $wish;
        $this->user = $user;
        $this->product = $product;
    }

    public function index(Request $request) {
        $page = @$request->page ? $request->page : 1;
        $callback = @$request->callback ? $request->callback : 'html';

        $products = $this->product->productForIndex()->orderBy('created_at', 'desc')->forPage($page, 10)->get();
        $wishes = $this->wish->wishForIndex()->orderBy('created_at', 'desc')->forPage($page, 10)->get();

        $data['products'] = $this->processGetRelated($products);
        $data['wishes'] = $this->processGetRelated($wishes);

        if (user_info()) {
            $data['friends'] = user_info()->getFriends();
            $data['friends_birthday'] = $data['friends']->where('birthday', '!=', null)->sortBy('birthday')->first();
        }

        if (!$data['products'] && !$data['wishes']) {
            $data['finish'] = true;
        } else {
            $data['finish'] = false;
        }
        
        if ($callback == 'html') {
            $data['popularWish'] = $this->wish->popularWish()->first();
            $data['popularProduct'] = $this->product->popularProduct()->first();

            return view('contents.frontend.index')->with($data);
        } else {
            $data['products'] = view('contents.frontend.wish_list')->with(['result' => $data['products']])->render();
            $data['wishes'] = view('contents.frontend.wish_list')->with(['result' => $data['wishes']])->render();

            return json_encode($data);
        }
    }

    public function getAllWishes() {
        return $this->wish->all();
    }

    public function aboutUs() {
        $get_data = StaticPage::where('slug', 'about-us')->first();

        $data['title'] = $get_data->title;
        $data['content'] = $get_data->content;

        return view('contents.frontend.static_page')->with($data);
    }

    public function terms() {
        $get_data = StaticPage::where('slug', 'terms')->first();

        $data['title'] = $get_data->title;
        $data['content'] = $get_data->content;

        return view('contents.frontend.static_page')->with($data);
    }

    public function faq() {
        $get_data = StaticPage::where('slug', 'faq')->first();

        $data['title'] = $get_data->title;
        $data['content'] = $get_data->content;

        return view('contents.frontend.static_page')->with($data);
    }

    public function shop() {
        return view('contents.frontend.shop');
    }

    public function search(Request $request) {
        $validator = Validator::make($request->all(), [
            'search' => 'required',
        ]);

        if ($validator->fails()) {
            session()->flash('status', trans("general.notification.something_wrong"));

            return redirect('/');
        }

        $search = $request->search;
        $products = $this->product->productForIndex()
            ->where('products.name', 'ilike', '%'.$search.'%')
            ->orderBy('created_at', 'asc')
            ->get();
        $wishes = $this->wish->wishForIndex()
            ->where('title', 'ilike', '%'.$search.'%')
            ->orderBy('created_at', 'asc')
            ->get();

        $data['title'] = $search;
        $data['products'] = $this->processGetRelated($products);
        $data['wishes'] = $this->processGetRelated($wishes);
        $data['users'] = $this->user->search($search)->get();

        return view('contents.frontend.search_result')->with($data);
    }
}
