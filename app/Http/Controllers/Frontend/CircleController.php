<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Exception;
use App\Models\Circle;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\StoreCircle;

class CircleController extends Controller
{
    public function __construct(Circle $circle, User $user)
    {
        $this->circle = $circle;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['circles'] = $this->circle->getAllByUserId();
        $data['user'] = null;

        return view('contents.frontend.circles.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['friends'] = user_info()->getFriends();

        return view('contents.frontend.circles.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCircle $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->circle->storeCircle($request);

            session()->flash('status', trans("general.notification.save_success"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("general.notification.save_success"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data['user'] = null;
        $data['circle'] = $this->circle->findCircleBySlug($slug);
        $data['circle_friends'] = $data['circle']->users()->get();

        return view('contents.frontend.circles.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data['circle'] = $this->circle->findCircleBySlug($slug);
        $data['friends'] = user_info()->getFriends();
        $data['circle_friends'] = $data['circle']->users()->get();

        return view('contents.frontend.circles.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCircle $request)
    {
        try {
            $this->circle->updateCircle($request);

            session()->flash('status', trans("general.notification.update_success"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("general.notification.update_success"),
            ], 200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $circle = $this->circle->find($request->id);

            if ($circle) {
                $this->circle->deleteCircle($circle->id);

                session()->flash('status', trans("general.notification.delete_success"));

                $result['result'] = 'Success';
                $result['message'] = trans("general.notification.delete_success");
            } else {
                session()->flash('status', trans("general.notification.data_not_found"));

                $result['result'] = 'Error';
                $result['message'] = trans("general.notification.data_not_found");
            }

            return response()->json($result, 200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }

    public function destroyFriend(Request $request)
    {
        try {
            $this->circle->deleteCircleUser($request->circle_id, $request->user_id);

            session()->flash('status', trans("general.notification.delete_success"));

            $result['result'] = 'Success';
            $result['message'] = trans("general.notification.delete_success");

            return response()->json($result, 200);
        } catch (Exception $e) {
            return errorException($e);
        }
    }
}
