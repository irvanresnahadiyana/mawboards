<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class FriendController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user->find(user_info('id'));

        $data['getAllFriendships'] = $user->getAllFriendships();
        $data['getPendingFriendships'] = $user->getPendingFriendships();
        $data['getAcceptedFriendships'] = $user->getAcceptedFriendships();
        $data['getDeniedFriendships'] = $user->getDeniedFriendships();
        $data['getBlockedFriendships'] = $user->getBlockedFriendships();
        $data['getFriendRequests'] = $user->getFriendRequests();
        $data['getFriendsCount'] = $user->getFriendsCount();

        $data['user'] = null;

        return view('contents.frontend.friends.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sender = $this->user->find($request->sender_user_id);
        $recipient = $this->user->find($request->recipient_user_id);
        // check recipient has blocked sender
        $check_sender_blocked = $recipient->hasBlocked($sender);
        if (!$check_sender_blocked) {
            // if ($request->type == 'befriend') {
                $sender->befriend($recipient);

                $message = trans("general.notification.request_friend_success");
            // }
        } else {
            $message = trans("general.notification.blocked");
        }

        session()->flash('status', $message);

        return response()->json([
            'result' => 'Success',
            'message' => $message,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $sender = $this->user->find($request->sender_user_id);
        $recipient = $this->user->find($request->recipient_user_id);
        if ($request->type == 'acceptFriendRequest') { // acceptFriendRequest
            $recipient->acceptFriendRequest($sender);

            $message = trans("general.notification.accept_friend_success");
        } else if ($request->type == 'denyFriendRequest') { //denyFriendRequest
            $recipient->denyFriendRequest($sender);

            $message = trans("general.notification.denied_friend_success");
        } else if ($request->type == 'blockFriend') { //blockFriend
            $recipient->blockFriend($sender);

            $message = trans("general.notification.block_friend_success");
        } else if ($request->type == 'unblockFriend') { //unblockFriend
            $sender->unblockFriend($recipient);

            $message = trans("general.notification.unblock_friend_success");
        }

        session()->flash('status', $message);

        return response()->json([
            'result' => 'Success',
            'message' => $message,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $sender = $this->user->find($request->sender_user_id);
        $recipient = $this->user->find($request->recipient_user_id);
        $sender->unfriend($recipient);
        if ($request->type == 'cancelFriendRequest') { // cancelFriendRequest
            $message = trans("general.notification.cancel_request_friend_success");
        } else if ($request->type == 'removeFriend') { // removeFriend
            $message = trans("general.notification.remove_friend_success");
        }

        session()->flash('status', $message);

        return response()->json([
            'result' => 'Success',
            'message' => $message,
        ], 200);
    }
}
