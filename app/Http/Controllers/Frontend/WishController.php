<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\StoreWish;
use App\Models\Wish;
use App\Models\User;
use App\Models\WishCategory;
use App\Models\Circle;
use App\Models\Like;
use App\Models\Product;

class WishController extends Controller
{
    public function __construct(Wish $wish, WishCategory $wish_category, Circle $circle, Like $like, Product $product, User $user)
    {
        $this->wish = $wish;
        $this->wish_category = $wish_category;
        $this->circle = $circle;
        $this->like = $like;
        $this->product = $product;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'My Wishboard';
        $data['wishboards'] = $this->wish->userWishboards();

        $data['user'] = false;
        $data['user_avatar'] = User::getDataAvatar();
        $data['wishes'] = $this->wish->getAllByUserId();
        $data['circle_total'] = count($this->circle->getAllByUserId());

        if (count($data['wishes']) > 0) {
            foreach ($data['wishes'] as $key => $value) {
                if (user_info()) {
                    $value->status_like = $value->likes()->where('user_id',user_info('id'))->first();
                }
                $value->people_likes = $value->likes;
            }
        }

        return view('contents.frontend.wishes.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($slug = null)
    {
        $wish_categories_selected = $this->wish_category->findBySlug($slug);

        if ($wish_categories_selected) {
            $data['wish_categories_selected'] = $wish_categories_selected;
        } else {
            $data['wish_categories_selected'] = null;
        }

        $data['user'] = false;
        $data['wish_categories'] = $this->wish_category->allWithColor();
        $data['circles'] = $this->circle->getAllByUserId();

        return view('contents.frontend.wishes.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWish $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->wish->storeWish($request);

            session()->flash('status', trans("general.notification.wish_published"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("general.notification.wish_published"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * Store wish/maw like created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function wishLike(Request $request) {
        DB::beginTransaction();
        try {
            $wishId = $request->id;
            $wishDetail = $request->group == 'App\Models\Wish' ? $this->wish->find($wishId) : $this->product->find($wishId);
            $checkLikeWish = $wishDetail->likes()->where('user_id',user_info('id'))->first();
            if ($checkLikeWish) {
                if ($checkLikeWish->status) {
                    $checkLikeWish->update(['status' => false]);
                    $wishDetail['status'] = 'unlike';
                } else {
                    $checkLikeWish->update(['status' => true]);
                    $wishDetail['status'] = 'like';
                }
            } else {
                $dataInsert = [
                    'user_id' => user_info('id'),
                    'status' => true,
                    'related_id' => $wishId,
                    'related_type' => $request->group
                ];

                $insert = Like::create($dataInsert);
            }

            $wishDetail['people_like'] = $wishDetail->likes()->where('status',true)->get();
            $wishDetail['wishID'] = '#wish-like-'.$wishId;

            $result['result'] = 'Success';
            $result['message'] = 'Data has been saved';
            $result['data'] = $wishDetail;

            DB::commit();
            return response()->json($result,200);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
                'msgTrans' => trans('general.notification.something_wrong'),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $slug)
    {

        $wishDetail = $this->wish->findBySlug($slug);
        $callback = @$request->callback == 'json' ? 'json' : 'html';
        if ($wishDetail) {

            if (@$request->comment_pagination) {
                $page = $request->page - 1;
                $comments = $wishDetail->comments()->orderBY('created_at','desc')->take(7)->skip($page * 7)->get();
                $result['result'] = 'success';
                if (count($comments) > 0) {
                    $result['finish'] = (count($comments) == 7 ? false : true);
                    $result['data'] = view('contents.frontend.wishes.comment')->with('comments',$comments)->render();
                } else {
                    $result['finish'] = true;
                    $result['data'] = [];
                }
                return response()->json($result,200);
            }

            $wishDetail->url = route('detail-wish-frontend',['slug' => $slug]);
            $wishDetail->image = get_file(@$wishDetail->images()->first()->image_standard);
            $wishDetail->avatar = link_to_avatar(@$wishDetail->user->images->first()->image_thumb);
            $wishDetail->people_likes = $wishDetail->likes()->where('status',true)->get();
            $wishDetail->status_like = false;
            if (user_info()) {
                $checkLikeWish = $wishDetail->likes()->where('user_id',user_info('id'))->first();
                $wishDetail->status_like = $checkLikeWish ? $checkLikeWish->status : false;
            }
            $wishDetail->people_comments = $wishDetail->comments()->orderBy('created_at','desc')->limit(7)->get();
            $wishDetail->total_comments = $wishDetail->comments->count();

            $wish_info['user'] = $wishDetail->user;
            $wish_info['user_id'] = $wishDetail->user_id;
            $wish_info['user_avatar'] = $wishDetail->avatar;
            $wish_info['user_name'] = $wishDetail->user_name;
            $wish_info['category'] = $wishDetail->wishCategory;
            $wish_info['data'] = $wishDetail->wishCategory->wishes()->orderBy('created_at','desc')->get();
            $wish_info['user_wishes'] = $wishDetail->user->wishes()->where('id','<>',$wishDetail->id)->inRandomOrder()->limit(6)->get();

            $wish_related = $this->wish->wishForIndex()->whereNotIn('wishes.id',[$wishDetail->id]);
            if ($wish_related->count() > 0) {
                $wish_related = $wish_related->inRandomOrder()->limit(6)->get();
            } else {
                $wish_related = $wish_related->get();
            }
            // ->inRandomOrder()->limit(15)->get();
            $this->processGetRelated($wish_related);
            // echo "<pre>";
            // print_r($wish_info); die();

            if ($callback == 'html') {
                return view('contents.frontend.wishes.detail')->with(['wish' => $wishDetail, 'wish_info' => $wish_info, 'wish_related' => $wish_related]);
            } else {
                $result['result'] = 'success';
                $result['content_detail'] = view('layouts.frontend.partials.modal_wish_contents.detail')->with(['wish' => $wishDetail])->render();
                $result['content_maw'] = view('layouts.frontend.partials.modal_wish_contents.maw')->with(['wish_info' => $wish_info])->render();
                $result['content_related'] = view('layouts.frontend.partials.modal_wish_contents.related_wish')->with(['wish_related' => $wish_related])->render();

                return response()->json($result,200);
            }
        } else {
            return redirect()->route('index');
        }
    }

    public function wishCategory($id = false, $slug = false)
    {
        $data['user'] = User::find($id);
        $data['category'] = $this->wish_category->findBySlug($slug);
        if ($data['category'] && $data['user']) {
            $wishes = $this->wish->wishForIndex()
                ->where('wish_categories.slug', $slug)
                ->where('wishes.user_id', $id)
                ->get();
            $data['wishes'] = $this->processGetRelated($wishes);
            $data['circle_total'] = $this->circle->getAllCircle()->where('user_id',$id)->count();
            // $data['category']->wishes()->where('user_id',$data['user']->id)->get();
            // echo "<pre>";
            // print_r($data); die();
            return view('contents.frontend.wishes.wish_category',$data);
        } else {
            return view('errors.503');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
