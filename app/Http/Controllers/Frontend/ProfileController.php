<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Exception;
use App\Models\User;
use App\Models\Wish;
use App\Models\Like;
use App\Models\Circle;
use App\Models\WishCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\UpdateProfile;
use App\Http\Controllers\Frontend\WishboardController as WishBoardCtrl;

class ProfileController extends Controller
{
    public function __construct(User $user, Wish $wish, Circle $circle, Like $like)
    {
        $this->user = $user;
        $this->wish = $wish;
        $this->circle = $circle;
        $this->like = $like;
        $this->wishBoardCtrl = new WishBoardCtrl(new Wish, new WishCategory, new User);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userId = null)
    {
        /* override profile .start here */
        $data = $this->wishBoardCtrl->index($userId, true);
        if ($data) {
            return view('contents.frontend.wishboards.index')->with($data);
        } else {
            session()->flash('status', trans("general.notification.data_not_found"));
            return redirect()->route('index');
        }
        /* override profile .end here */

        $data['news_total'] = 0;
        $data['message_total'] = 0;
        $data['maw_total'] = $this->wish->countWishUser(user_info('id'));
        $data['like_total'] = $this->like->countLikeUser(user_info('id'));
        $data['follower_total'] = 0;
        $data['following_total'] = 0;

        $page = @$request->page ? $request->page : 1;
        $callback = @$request->callback ? $request->callback : 'html';
        $wishes = $this->wish->wishOnProfileUser(user_info('id'))->orderBy('created_at','desc')->forPage($page,6)->get();

        // $result = collect();
        // $result = $products->merge($wishes);

        $data['wishes'] = $this->processGetRelated($wishes);
        if ($data['wishes']) {
            $data['wishes_random'] = $data['wishes']->random();
        } else {
            $data['wishes_random'] = null;
        }

        if (!$data['wishes']) {
            $data['finish'] = true;
        } else {
            $data['finish'] = false;
        }

        if ($callback == 'html') {
            return view('contents.frontend.profile.index')->with($data);
        } else {
            // $wishLists = view('contents.frontend.wish_list')->with($data)->render();
            $data['wishes'] = view('contents.frontend.wish_list')->with(['result' => $data['wishes']])->render();
            return json_encode($data);
        }

        return view('contents.frontend.profile.index')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('contents.frontend.profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfile $request)
    {
        DB::beginTransaction();
        try {
            DB::commit();
            $this->user->updateUser($request);

            // session()->flash('status', trans("general.notification.update_success"));

            return response()->json([
                'result' => 'Success',
                'message' => trans("general.notification.update_success"),
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return errorException($e);
        }
    }

    /**
     * Show wishboard by user id
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id)
    {
        $data['title'] = 'Mawboard';
        $data['user'] = $this->user->getUser($id);
        $data['user_avatar'] = $data['user']->images()->first();
        $data['maw_total'] = $this->wish->countWishUser($id);
        $data['wishes'] = $this->wish->getAllByUserId($id);

        if (count($data['wishes']) > 0) {
            foreach ($data['wishes'] as $key => $value) {
                if (user_info()) {
                    $value->status_like = $value->likes()->where('user_id',user_info('id'))->first();
                }
                $value->people_likes = $value->likes;
            }
        }

        $sender = user_info();
        $recipient = $data['user'];
        // check status between sender and recipient
        $check_friend_status = $sender->isFriendWith($recipient);
        if (!$check_friend_status) { // if not friend
            // check sender has sent friend request to recipient
            $check_sender_sent_request = $sender->hasSentFriendRequestTo($recipient);
            if (!$check_sender_sent_request) { // if sender not sent request
                $data['friend_status'] = ['status' => false, 'message' => ''];
            } else {
                $data['friend_status'] = ['status' => true, 'message' => 'Waiting Response'];
            }
        } else {
            $data['friend_status'] = ['status' => true, 'message' => 'isFriend'];
        }

        return view('contents.frontend.wishes.index')->with($data);
    }

    /**
     * [message description]
     * @return [type] [description]
     */
    public function message()
    {
        return view('contents.frontend.profile.message');
    }
}
