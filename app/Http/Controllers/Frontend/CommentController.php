<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\CommentRequest;

use DB;
use Exception;

use App\Models\Comment;
use App\Models\Wish;

class CommentController extends Controller
{
    public function __construct(Comment $comment, Wish $wish)
    {
        $this->comment = $comment;
        $this->wish = $wish;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        DB::beginTransaction();
        try {
            $insert = [
                'user_id' => user_info('id'),
                'comment' => $request->comment,
                'related_id' => $request->id,
                'related_type' => 'App\Models\Wish',
            ];

            DB::commit();
            $insertComment = $this->comment->create($insert);
            if ($insertComment) {
                
                // $data['comment'] = $insertComment;
                // $data['user'] = $insertComment->user;
                // $data['user']['avatar'] = link_to_avatar($insertComment->user->images()->first());
                $latestComment = $this->comment->getLatestComment($request->id,'App\Models\Wish');
                $detailWish =  $this->wish->find($request->id);
                $result['result'] = 'success';
                $result['message'] = 'comment has been saved';
                $result['data'] = view('contents.frontend.wishes.comment_list')->with(
                    [
                        'comments' => $latestComment->limit(7)->get(),
                        'total_comments' => $detailWish->comments->count(),
                        'wish' => $detailWish,
                    ])->render();
            } else {
                $result['result'] = 'warning';
                $result['message'] = trans('general.notification.something_wrong');
                $result['data'] = false;
            }

            return response()->json($result,200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
                'msgTrans' => trans('general.notification.something_wrong'),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ], 400);
        }
    }
}
