<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterMerchant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min: 8',
            'password_confirmation' => 'required|min: 8|same:password',
            'first_name' => 'required',
            'last_name' => 'required',
            'store_name' => 'required|unique:user_merchants,store_name',
            'store_address' => 'required',
            'phone_number' => 'required|numeric',
            'country' => 'required',
            'province' => 'required',
            'city' => 'required',
            'postal_code' => 'required|numeric',
            'terms' => 'required',
        ];
    }
}
