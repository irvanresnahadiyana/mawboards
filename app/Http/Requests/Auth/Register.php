<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min: 8',
            'password_confirmation' => 'required|min: 8|same:password',
            'gender' => 'required',
            'day' => 'required',
            'month' => 'required',
            'year' => 'required',
            // 'birthday' => 'required|date',
            'terms' => 'required',
        ];
    }
}
