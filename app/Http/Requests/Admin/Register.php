<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (@$this->request->get('id') != '' && @$this->request->get('password') != '') {
            $rules = [
                'password' => 'required|min: 8',
                'password_confirmation' => 'required|min: 8|same:password',
                'birthday' => 'required',
                'gender' => 'required',
                'first_name' => 'required',
            ];
        } elseif (@$this->request->get('id') != '' && @$this->request->get('password') == '') {
            $rules = [
                'birthday' => 'required',
                'gender' => 'required',
                'first_name' => 'required',
            ];
        } else {
            $rules = [
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min: 8',
                'password_confirmation' => 'required|min: 8|same:password',
                'birthday' => 'required',
                'gender' => 'required',
                'first_name' => 'required',
            ];            
        }

        return $rules;
    }
}
