<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreWish extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (@$this->request->get('id') != '') {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png,gif|max:1000',
                'title' => 'required',
                'category' => 'required',
            ];
        } else {
            $rules = [
                'user' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|max:1000',
                'title' => 'required',
                'category' => 'required',
            ];
        }
        return $rules;
    }
}
