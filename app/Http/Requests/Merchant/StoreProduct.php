<?php

namespace App\Http\Requests\Merchant;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (@$this->request->get('has_child') == 'yes') {
            $rules = [
                    'category' => 'required',
                    'category_child' => 'required',
                    'product_name' => 'required',
                ];
        } else {
            $rules = [
                    'category' => 'required',
                    'product_name' => 'required',
                ];
        }

        return $rules;
    }
}
