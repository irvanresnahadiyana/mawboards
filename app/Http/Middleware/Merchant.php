<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class Merchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if (!Sentinel::check() || !Sentinel::inRole('merchant')) {
          session()->flash('status', trans("auth.logged"));

          return redirect()->back();
      }

      return $next($request);
    }
}
