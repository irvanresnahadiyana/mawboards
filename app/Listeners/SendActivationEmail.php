<?php

namespace App\Listeners;

use App\Events\UserActivated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Mail;
use Activation;

class SendActivationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserActivated  $event
     * @return void
     */
    public function handle(UserActivated $event)
    {
        // Send email activation
        $user = User::find($event->user_id);

        Activation::removeExpired();
        $activation = (Activation::exists($user)) ? Activation::exists($user) : Activation::create($user);

        $email_content = '<a href="'.route('auth-activate', ['login' => $user->email, 'code' => $activation->code ]).'">Click here</a> to activate your account.';
        Mail::send('emails.general', ['title' => trans("auth.activate"), 'content' => $email_content], function ($message) use($user)
        {
            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $message->to($user->email);
            $message->subject(trans("auth.activate"));
        });
    }
}
