<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductCategoryChild extends Model
{
    use Sluggable;
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $table = 'product_categories_child';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'product_category_id',
        'name',
        'slug',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\ProductCategory','product_category_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','product_category_child_id');
    }
}
