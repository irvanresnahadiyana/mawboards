<?php

namespace App\Models;

use Event;
use Sentinel;
use DB;
use App\Models\GalleryImage;
use App\Models\UserMerchant;
use App\Events\UserActivated;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Cartalyst\Sentinel\Permissions\PermissibleInterface;
use Cartalyst\Sentinel\Permissions\PermissibleTrait;
use Cartalyst\Sentinel\Persistences\PersistableInterface;
use Cartalyst\Sentinel\Roles\RoleableInterface;
use Cartalyst\Sentinel\Roles\RoleInterface;
use Illuminate\Database\Eloquent\Model;
use Hootlex\Friendships\Traits\Friendable;
use Hashids\Hashids;

class User extends CartalystUser
{
    use Friendable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'permission', 'first_name', 'last_name','birthday','gender','live_in','bio',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function datatables()
    {
        return $this->leftJoin('role_users','role_users.user_id','=','users.id')
            ->join('roles','roles.id','=','role_users.role_id')
            ->select(['users.id','users.email','users.first_name','users.last_name','roles.name as role_name','users.created_at']);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','user_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\GalleryImage','related');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment','user_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like','user_id');
    }

    public function wishes()
    {
        return $this->hasMany('App\Models\Wish','user_id');
    }

    public static function getDataAvatar()
    {
        $user = user_info();

        if ($user) {
            return $user->images()->where('is_primary', true)->first();
        }
    }

    public function getFriendAvatar($user_id)
    {
        $user = $this->getUser($user_id);

        if ($user) {
            return $user->images()->where('is_primary', true)->first();
        }
    }

    public function getUser($id)
    {
        return $this->find($id);
    }

    public function getUsers()
    {
        return $this->join('role_users','role_users.user_id','=','users.id')
            ->select('users.id',\DB::raw("concat(users.first_name || ' ' || users.last_name) as name"))
            ->where('role_users.role_id',3)
            ->get();
    }

    public function registerUser($data)
    {
        $user = Sentinel::register($data->all());

        // Attach user to role
        if ($data->store_name) {
            $role = Sentinel::findRoleBySlug('merchant');
            $merchant = new UserMerchant;
            $merchant->user_id = $user->id;
            $merchant->store_name = $data->store_name;
            $merchant->store_address = $data->store_address;
            $merchant->phone_number = $data->phone_number;
            $merchant->country = $data->country;
            $merchant->province = $data->province;
            $merchant->city = $data->city;
            $merchant->postal_code = $data->postal_code;
            $merchant->save();
        } else {
            $role = Sentinel::findRoleBySlug('user');
            // Update other columns
            $update_user = $this->find($user->id);
            $update_user->gender = $data->gender;
            $update_user->birthday = $data->year.'-'.$data->month.'-'.$data->day;
            $update_user->save();
        }
        $role->users()->attach($user);

        // Send email activation use event
        Event::fire(new UserActivated($user->id));
    }

    public function updateUser($data)
    {
        $user = user_info();
        $credentials = [
            'first_name' => $data->first_name,
            'last_name' => $data->last_name,
        ];

        $update = Sentinel::update($user, $credentials);

        // update other column outside sentinel column
        if ($data->gender || $data->birthday || $data->live_in || $data->bio) {
            $update_user = $this->find($update->id);
            $update_user->gender = $data->gender;
            $update_user->birthday = $data->birthday;
            $update_user->live_in = $data->live_in;
            $update_user->bio = $data->bio;
            $update_user->save();
        }

        if ($data->avatar) {
            $user_images = $this->find($update->id)->images;
            foreach ($user_images as $image) {
                delete_file($image->image_standard);
                $image->delete();
            }

            $upload_image = upload_file($data->avatar, 'uploads/avatars/');

            $gallery = new GalleryImage;
            $gallery->image_standard = $upload_image['original'];
            $gallery->image_thumb = $upload_image['thumbnail'];
            $gallery->related_id = $user->id;
            $gallery->related_type = 'App\Models\User';
            $gallery->is_primary = true;
            $gallery->save();
        }
    }

    public static function boot()
    {
        parent::boot();

        User::deleting(function($user) {
            foreach ($user->images()->get() as $image) {
                delete_file($image->image_standard);
                $image->delete();
            }

            foreach ($user->products as $product) {
                $product->delete();
            }

            foreach ($user->comments as $comment) {
              $comment->delete();
            }

            foreach ($user->likes as $like) {
              $like->delete();
            }

            foreach ($user->wishes as $wish) {
              $wish->delete();
            }

            $user->merchant->delete();
        });
    }

    public function merchant()
    {
        return $this->hasOne('App\Models\UserMerchant','user_id');
    }

    public function search($search)
    {
        return $this
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('users.id', 'first_name', 'last_name', 'roles.slug as role_slug', 'roles.name as role_name')
            ->where('first_name', 'ilike', '%'.$search.'%')
            ->orWhere('last_name', 'ilike', '%'.$search.'%');
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('MySecretSalt');

        return $hashids->encode($this->getKey());
    }

    /**
     * Return uniqueId
     */
    public static function uniqueId($value)
    {
        $hashids = new Hashids('MySecretSalt');
        $hashids = $hashids->encode($value);

        if ($hashids) {
            return $hashids[0];
        }
    }

    /**
     * Return originalId
     */
    public static function originalId($value)
    {
        $hashids = new Hashids('MySecretSalt');
        $hashids = $hashids->decode($value);

        if ($hashids) {
            return $hashids[0];
        }
    }
}
