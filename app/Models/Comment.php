<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'comment', 'related_id','related_type'
    ];

    Public function related()
    {
        return $this->morphTo();
    }

    Public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    Public function getLatestComment($relatedId, $relatedType)
    {
        return $this->select('id','user_id','comment','related_id','related_type','created_at')
            ->where('related_id',$relatedId)
            ->where('related_type',$relatedType)
            ->orderBy('created_at','desc');
    }
}
