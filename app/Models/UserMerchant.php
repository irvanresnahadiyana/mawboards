<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMerchant extends Model
{
    protected $table = 'user_merchants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'store_name', 'store_address', 'phone_number', 'country', 'province', 'city', 'postal_code',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
