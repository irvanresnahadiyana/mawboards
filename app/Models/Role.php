<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Role extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $table = 'roles';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'slug',
    ];

    // public function products()
    // {
    //     return $this->hasMany('App\Models\User','role_id');
    // }

    public function datatables()
    {
        return $this->select(['id','name','slug','created_at','updated_at'])->get();
    }

    public function getDetail($id)
    {
        return $this->find($id);
    }
}
