<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CircleUser extends Model
{
    protected $table = 'circle_users';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id',
        'circle_id',
    ];
}
