<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishCircle extends Model
{
    protected $table = 'wishes_circles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wish_id', 'circle_id',
    ];

    public function wish()
    {
        return $this->belongsTo('App\Models\Wish', 'wish_id');
    }

    public function circle()
    {
        return $this->belongsTo('App\Models\Circle', 'circle_id');
    }
}
