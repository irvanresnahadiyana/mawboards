<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WholesalePrice extends Model
{
  protected $table = 'wholesales_prices';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'price',
    'quantity',
    'is_active',
  ];

  public function products()
  {
    return $this->belongsTo('App\Models\Product','product_id');
  }
}
