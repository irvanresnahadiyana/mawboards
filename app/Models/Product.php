<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Models\WholesalePrice;
use App\Models\GalleryImage;

use Sentinel;

class Product extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

  protected $table = 'products';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'description',
    'price',
    'wholesales_price_id',
    'product_category_id',
    'product_category_child_id',
    'stock',
    'weight',
    'length',
    'width',
    'height',
    'is_active',
    'slug'
  ];

  public function user()
  {
    return $this->belongsTo('App\Models\User','user_id');
  }

  public function images()
  {
    return $this->morphMany('App\Models\GalleryImage','related');
  }

  public function wholesales_prices()
  {
    return $this->hasMany('App\Models\WholesalePrice','product_id');
  }

  public function product_category()
  {
    return $this->belongsTo('App\Models\ProductCategory','product_category_id');
  }

  public function product_category_child()
  {
    return $this->belongsTo('App\Models\ProductCategoryChild','product_category_child_id');
  }

  public function comments()
  {
    return $this->morphMany('App\Models\Comment','related');
  }

  public function likes()
  {
    return $this->morphMany('App\Models\Like','related');
  }

  public function storeProduct($data)
  {
    $this->user_id = Sentinel::getUser()->id;
    $this->name = $data->product_name;
    $data->description ? $this->description = $data->description : $this->description = '';
    $data->price ? $this->price = $data->price : $this->price = 0;
    $data->category ? $this->product_category_id = $data->category : $this->product_category_id = null;
    $data->has_child == 'yes' ? $this->product_category_child_id = $data->category_child : $this->product_category_child_id = null;
    $data->stock ? $this->stock = $data->stock : $this->stock = 0;
    $data->weight ? $this->weight = $data->weight : $this->weight = 0;
    $data->length ? $this->length = $data->length : $this->length = 0;
    $data->width ? $this->width = $data->width : $this->width = 0;
    $data->height ? $this->height = $data->height : $this->height = 0;

    $data->is_active ? $this->is_active = true : $this->is_active = false;
    if ($this->save()) {
      
      // if (!empty($data->wholesale_price[0])){
      //   foreach ($data->wholesale_price as $key => $price) {
      //     $wholesalePrice = new WholesalePrice;
      //     $wholesalePrice->product_id = $this->id;
      //     $wholesalePrice->price = $price;

      //     if (!empty($data->quantity[$key])) {
      //       $wholesalePrice->quantity = $data->quantity[$key];
      //     }

      //     if (@$data->price_active[$key]) {
      //       $wholesalePrice->is_active = true;
      //     }

      //     $wholesalePrice->save();
      //   }
      // }

      if (count($data->product_image) > 0) {
        foreach ($data->product_image as $key => $image) {
          $uploadImage = upload_file($image, 'uploads/products/');

          $gallery = new GalleryImage;
          $gallery->image_standard = $uploadImage['original'];
          $gallery->image_thumb = $uploadImage['thumbnail'];
          $gallery->related_id = $this->id;
          $gallery->related_type = 'App\Models\Product';
          if (@$data->product_image_active == $key) {
            $gallery->is_primary = true;
          }
          $gallery->save();
        }
      }
    }
  }

  public function updateProduct($data)
  {
    $galleryImage = new GalleryImage;
    $update = Product::find($data->id_product);
    $update->name = $data->product_name;
    $data->description ? $update->description = $data->description : $update->description = '';
    $data->price ? $update->price = $data->price : $update->price = 0;
    $data->category ? $update->product_category_id = $data->category : $update->product_category_id = null;
    $data->has_child == 'yes' ? $update->product_category_child_id = $data->category_child : $update->product_category_child_id = null;
    $data->stock ? $update->stock = $data->stock : $update->stock = 0;
    $data->weight ? $update->weight = $data->weight : $update->weight = 0;
    $data->length ? $update->length = $data->length : $update->length = 0;
    $data->width ? $update->width = $data->width : $update->width = 0;
    $data->height ? $update->height = $data->height : $update->height = 0;

    $data->is_active ? $update->is_active = true : $update->is_active = false;

    if ($update->save()) {
      // if (!empty($data->wholesale_price[0])){
      //   WholesalePrice::where('product_id',$update->id)->delete();
      //   foreach ($data->wholesale_price as $key => $price) {
      //     $wholesalePrice = new WholesalePrice;
      //     $wholesalePrice->product_id = $update->id;
      //     $wholesalePrice->price = $price;

      //     if (!empty($data->quantity[$key])) {
      //       $wholesalePrice->quantity = $data->quantity[$key];
      //     }

      //     if (@$data->price_active[$key]) {
      //       $wholesalePrice->is_active = true;
      //     }

      //     $wholesalePrice->save();
      //   }
      // }

      GalleryImage::where('related_id',$update->id)->where('related_type','App\Models\Product')
        ->update(['is_primary' => false]);
      if (count(@$data->product_image) > 0) {
        
        foreach ($data->product_image as $key => $image) {
          if (@$data->product_image_old[$key]) {
            
            if (!empty($data->product_image_old[$key])) {
              delete_file($data->product_image_old[$key]);
            }

            $uploadImage = upload_file($image, 'uploads/products/');  
            $gallery = GalleryImage::find($data->product_image_old_id[$key]);
          } else {
            $uploadImage = upload_file($image, 'uploads/products/');
            $gallery = new GalleryImage;
          }
          $gallery->image_standard = $uploadImage['original'];
          $gallery->image_thumb = $uploadImage['thumbnail'];
          $gallery->related_id = $update->id;
          $gallery->related_type = 'App\Models\Product';
          if (@$data->product_image_active == $key) {
            $gallery->is_primary = true;
          }
          $gallery->save();

        }
        if (!$chkImagePrimary = $update->images->where('is_primary',true)->first()) {
          $galleryImage->updatePrimaryImageById($data->product_image_old_id[$data->product_image_active]);
        }
      } else {
        $galleryImage->updatePrimaryImageById($data->product_image_old_id[$data->product_image_active]);
      }
    }
  }

  public static function boot()
  {
    parent::boot();

    Product::deleting(function($products) {
      foreach ($products->images()->get() as $image) {
        delete_file($image->image_standard);
        $image->delete();
      }

      foreach($products->wholesales_prices as $wholesales_price){
        $wholesales_price->delete();
      }

      $products->comments()->delete();
      $products->likes()->delete();
    });
  }

  public function getAllProducts()
  {
    $results =  $this->join('product_categories','product_categories.id','=','products.product_category_id')
      ->select('products.id','products.name as product_name','products.description','products.price','products.stock','products.weight',
          'products.length','products.width','products.height','products.is_active','products.slug as product_slug',
          'product_categories.name as category_name','product_categories.slug as category_slug','products.updated_at')
      ->orderBy('products.updated_at','DESC')
      ->get();

    if (count($results) > 0) {
        foreach ($results as $val) {
            $val->image_primary = $val->images()->where('is_primary',true)->first();
            $val->total_like = $val->likes()->where('status',true)->count();
            $val->total_dislike = $val->likes()->where('status',false)->count();
            $val->total_comment = count($val->comments);
        }
    }

    return $results;
  }

  public function getProductsByUserId($userId)
  {
    $results =  $this->join('product_categories','product_categories.id','=','products.product_category_id')
      ->select('products.id','products.name as product_name','products.description','products.price','products.stock','products.weight',
          'products.length','products.width','products.height','products.is_active','products.slug as product_slug',
          'product_categories.name as category_name','product_categories.slug as category_slug','products.updated_at')
      ->where('products.user_id',$userId)
      ->orderBy('products.updated_at','DESC')
      ->get();

    if (count($results) > 0) {
        foreach ($results as $val) {
            $val->image_primary = $val->images()->where('is_primary',true)->first();
            $val->total_like = $val->likes()->where('status',true)->count();
            $val->total_dislike = $val->likes()->where('status',false)->count();
            $val->total_comment = count($val->comments);
        }
    }

    return $results;
  }

  public function datatable()
  {
    return $this->join('product_categories','product_categories.id','=','products.product_category_id')
      ->join('users','users.id','=','products.user_id')
      ->select(['products.id','products.name','product_categories.name as product_category_name',
        'products.slug',\DB::raw("(CONCAT(users.first_name,' ',users.last_name)) AS full_name"),
        'products.is_publish','products.created_at','products.updated_at'])->get();
  }

  public function productForIndex()
  {
    return $this
      ->join('users', 'products.user_id', '=', 'users.id')
      ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
      ->leftJoin('images as image_users', 'products.id', '=', 'image_users.related_id')
      ->join('user_merchants', 'products.user_id', '=', 'user_merchants.user_id')
      ->select(
        'products.id',
        'products.name as title',
        'products.slug',
        'products.created_at',
        'product_categories.name as category_name',
        'product_categories.slug as category_slug',
        \DB::raw('null as hex'),
        'users.id as user_id',
        \DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
        'image_users.image_thumb as image_user',
        'user_merchants.store_name'
      )
      ->where([
        ['products.is_publish', true],
        ['products.is_active', true],
        ['image_users.related_type', 'App\Models\Product'],
        ['image_users.is_primary', true],
      ]);
  }

  public function popularProduct()
  {
    return $this->join('product_categories','product_categories.id','=','products.product_category_id')
      ->join('users','users.id','=','products.user_id')
      ->select(
        'products.id',
        'products.user_id',
        \DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
        'products.name as title',
        'products.slug',
        'product_categories.name as category_name',
        'product_categories.slug as category_slug',
        'products.created_at',
        \DB::raw('null as hex'),'products.price',
        \DB::raw("(select count(*) from likes where related_id = products.id and related_type = 'App\Models\Product') as total_like"))
      ->where([
        ['products.is_publish', true],
        ['products.is_active', true],
      ])
      ->orderBy('total_like','desc');
  }
}
