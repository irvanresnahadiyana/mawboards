<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Wish extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $table = 'wishes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'wish_category_id', 'description','user_id','slug'
    ];

    public function wishCategory()
    {
        return $this->belongsTo('App\Models\WishCategory','wish_category_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\GalleryImage', 'related');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'related');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'related');
    }

    public function getWishImage($id)
    {
        $wish = $this->find($id);

        if ($wish) {
            return $wish->images()->where('is_primary', true)->first();
        }
    }

    public function getAll()
    {
        return $this
            ->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->join('images', 'wishes.id', '=', 'images.related_id')
            ->join('images as avatars', 'users.id', '=', 'avatars.related_id')
            ->select(
                'wishes.id',
                'wishes.slug',
                'wishes.title',
                'wishes.wish_category_id',
                'wish_categories.name as wish_category_name',
                'colors.hex',
                'users.id as user_id',
                'users.first_name',
                'users.last_name',
                'images.image_standard as wish_image',
                'images.image_thumb as wish_image_thumb',
                'avatars.image_thumb as avatar')
            ->orderBy('wishes.id', 'desc')
            ->paginate(6);
    }

    public function getAllByUserId($user_id = null)
    {
        if (!$user_id) {
            $user_id = user_info('id');
        }
        return $this
            ->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->join('images as image_wish', 'wishes.id', '=', 'image_wish.related_id')
            ->leftJoin('images as image_user', 'users.id', '=', 'image_user.related_id')
            ->select(
                'wishes.id',
                'wishes.slug',
                'wishes.title',
                'wishes.wish_category_id',
                'wish_categories.name as wish_category_name',
                'colors.hex',
                'users.id as user_id',
                'users.first_name',
                'users.last_name',
                'image_wish.image_standard as wish_image',
                'image_wish.image_thumb as wish_image_thumb',
                'image_user.image_thumb as avatar')
            ->where('wishes.user_id', $user_id)
            ->where('image_wish.related_type', 'App\Models\Wish')
            ->orderBy('wishes.id', 'desc')
            ->get();
    }

    public function countWishUser($user_id = null)
    {
        if (!$user_id) {
            $user_id = user_info('id');
        }
        return $this
            ->join('users', 'wishes.user_id', '=', 'users.id')
            ->where('wishes.user_id', $user_id)
            ->count();
    }

    public function storeWish($data, $type='')
    {
        if ($type == 'backend') {
            $user_id = $data->user;
        } else {
            $user_id = user_info('id');
        }
        $this->user_id = $user_id;
        $this->title = $data->title;
        $this->wish_category_id = $data->category;
        $this->description = $data->description;

        if ($this->save()) {
            if (@$data->image) {
                $upload_image = upload_file($data->image, 'uploads/wishes/');

                $gallery = new GalleryImage;
                $gallery->image_standard = $upload_image['original'];
                $gallery->image_thumb = $upload_image['thumbnail'];
                $gallery->related_id = $this->id;
                $gallery->related_type = 'App\Models\Wish';
                $gallery->is_primary = true;
                $gallery->save();
            }

            if ($data->circle) {
                foreach ($data->circle as $circle_id) {
                    $wish_circle = new WishCircle;
                    $wish_circle->wish_id = $this->id;
                    $wish_circle->circle_id = $circle_id;
                    $wish_circle->save();
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function updateWish($data,$type='')
    {
        $wishGet = $this->find($data->id);
        if ($wishGet) {
            $wishGet->title = $data->title;
            $wishGet->wish_category_id = $data->category;
            $wishGet->description = $data->description;

            if ($wishGet->save()) {
                if (@$data->image) {
                    $getImageWish = $wishGet->images->where('is_primary',true)->first();
                    if ($getImageWish) {
                        delete_file($getImageWish->image_standard);

                        $upload_image = upload_file($data->image, 'uploads/wishes/');

                        $getImageWish->image_standard = $upload_image['original'];
                        $getImageWish->image_thumb = $upload_image['thumbnail'];
                        $getImageWish->save();
                    } else {
                        $upload_image = upload_file($data->image, 'uploads/wishes/');

                        $gallery = new GalleryImage;
                        $gallery->image_standard = $upload_image['original'];
                        $gallery->image_thumb = $upload_image['thumbnail'];
                        $gallery->related_id = $wishGet->id;
                        $gallery->related_type = 'App\Models\Wish';
                        $gallery->is_primary = true;
                        $gallery->save();
                    }
                }

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function boot()
    {
        parent::boot();

        Wish::deleting(function($wishes) {
          foreach ($wishes->images()->get() as $image) {
            delete_file($image->image_standard);
            $image->delete();
          }
          $wishes->likes->delete();
        });
    }

    public function datatable()
    {
        return $this->join('users','users.id','=','wishes.user_id')
            ->join('wish_categories','wish_categories.id','=','wishes.wish_category_id')
            ->select(['wishes.id','users.first_name','users.last_name','wishes.title','wish_categories.name as category_name','wishes.created_at']);
    }

    public function wishForIndex()
    {
        return $this
            ->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->join('images as image_users', 'users.id', '=', 'image_users.related_id')
            ->select(
                'wishes.id',
                'wishes.title',
                'wishes.slug',
                'wishes.created_at',
                'wish_categories.name as category_name',
                'wish_categories.slug as category_slug',
                'colors.hex',
                'users.id as user_id',
                \DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
                'image_users.image_thumb as image_user'
            );
            /*->where([
                ['image_users.related_type', 'App\Models\User']
            ]);*/

        // return $this
        //     ->join('users', 'wishes.user_id', '=', 'users.id')
        //     ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
        //     ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
        //     ->join('images as image_users', 'users.id', '=', 'image_users.related_id')
        //     ->join('wishes_circles', 'wishes.id', '=', 'wishes_circles.wish_id')
        //     ->join('circles', 'wishes_circles.circle_id', '=', 'circles.id')
        //     ->join('circle_users', 'circles.id', '=', 'circle_users.circle_id')
        //     ->select(
        //         'wishes.id',
        //         'wishes.title',
        //         'wishes.slug',
        //         'wishes.created_at',
        //         'wish_categories.name as category_name',
        //         'wish_categories.slug as category_slug',
        //         'colors.hex',
        //         'users.id as user_id',
        //         \DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
        //         'image_users.image_thumb as image_user'
        //     )
        //     ->where([
        //         ['image_users.related_type', 'App\Models\User'],
        //         ['circle_users.user_id', user_info('id')]
        //     ])
        //     ->orWhere([
        //         ['wishes.user_id', user_info('id')]
        //     ])
        //     ->distinct();
    }

    public function wishOnProfileUser($user_id = null)
    {
        if (!$user_id) {
            $user_id = user_info('id');
        }
        return $this->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->select('wishes.id', 'wishes.user_id',\DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
                'wishes.title', 'wishes.slug','wish_categories.name as category_name','wish_categories.slug as category_slug','wishes.created_at',
                'colors.hex')
            ->where('wishes.user_id',$user_id);
    }

    public function findBySlug($slug)
    {
        return $this->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->select('wishes.id', 'wishes.user_id','wishes.wish_category_id',\DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
                'wishes.title', 'wishes.slug','wish_categories.name as category_name','wish_categories.slug as category_slug','wishes.created_at',
                'colors.hex')
            ->where('wishes.slug',$slug)
            ->first();
    }

    public function userWishboards($user_id = null)
    {
        if (!$user_id) {
            $user_id = user_info('id');
        }

        return $this
            ->leftJoin('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->select(
                'wish_categories.id',
                'wish_categories.name',
                'wish_categories.slug',
                'colors.id as color_id',
                'colors.name as color_name',
                'colors.hex as color_hex',
                \DB::raw('count(wish_categories.id) as total'))
            ->where('wishes.user_id', $user_id)
            ->where('wish_categories.user_id', $user_id)
            ->orWhere('wish_categories.user_id', null)
            ->groupBy('wish_categories.id', 'colors.id')
            ->get();
    }

    public function popularWish()
    {
        return $this->join('users', 'wishes.user_id', '=', 'users.id')
            ->join('wish_categories', 'wishes.wish_category_id', '=', 'wish_categories.id')
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->select('wishes.id', 'wishes.user_id',\DB::raw("concat(users.first_name || ' ' || users.last_name) as user_name"),
                'wishes.title', 'wishes.slug','wish_categories.name as category_name','wish_categories.slug as category_slug','wishes.created_at',
                'colors.hex', \DB::raw("(select count(*) from comments where related_id = wishes.id and related_type = 'App\Models\Wish') as total_comment")
                ,\DB::raw("(select count(*) from likes where related_id = wishes.id and related_type = 'App\Models\Wish') as total_like"))
            ->orderBy('total_comment','desc')
            ->orderBy('total_like','desc');
    }
}
