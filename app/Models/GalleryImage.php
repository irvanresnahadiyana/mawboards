<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
  protected $table = 'images';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'image_standard', 'image_thumb', 'is_primary','related_id','related_type',
  ];

  public function related()
  {
    return $this->morphTo();
  }

  public function updatePrimaryImageById($id)
  {
    return GalleryImage::where('id',$id)->update(['is_primary' => true]);
  }

  public function deleteImageById($id)
  {
    $galleyImage = GalleryImage::find($id);
    
    $galleyImage->delete();

    return true;
  }

  public static function boot()
  {
    parent::boot();

    GalleryImage::deleting(function($image) {
      delete_file($image->image_standard);      
    });
  }
}
