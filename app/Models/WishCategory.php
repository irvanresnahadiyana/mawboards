<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class WishCategory extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $table = 'wish_categories';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'color_id',
        'slug'
    ];

    public function storeWishCategory($data)
    {
        $this->name = $data->name;
        $this->color_id = 6;
        $this->user_id = user_info('id');
        $this->save();

        return true;
    }

    public function wishes()
    {
        return $this->hasMany('App\Models\Wish', 'wish_category_id');
    }

    public function color()
    {
        return $this->hasOne('App\Models\Color','id');
    }

    public function datatables()
    {
        return $this->join('colors','colors.id','=','wish_categories.color_id')
            ->select(['wish_categories.id','wish_categories.name','colors.name as color_name','wish_categories.slug',
                'wish_categories.created_at','wish_categories.updated_at']);
    }

    public function allWithColor()
    {
        return $this
            ->join('colors', 'colors.id', '=', 'wish_categories.color_id')
            ->select('wish_categories.id', 'wish_categories.name', 'colors.name as color_name', 'colors.hex')
            ->where('user_id', user_info('id'))
            ->orWhere('user_id', null)
            ->get();
    }

    public function findBySlug($slug)
    {
        return $this->where('slug', $slug)->first();
    }

    public function userWishboards($user_id)
    {
        return $this
            ->join('colors', 'wish_categories.color_id', '=', 'colors.id')
            ->leftJoin('wishes', 'wish_categories.id', '=', 'wishes.wish_category_id')
            ->select(
                'wish_categories.id',
                'wish_categories.name',
                'wish_categories.slug',
                'colors.id as color_id',
                'colors.name as color_name',
                'colors.hex as color_hex',
                \DB::raw('count(wishes.id) as total'))
            ->where('wish_categories.user_id', $user_id)
            ->orWhere('wish_categories.user_id', null)
            ->where('wishes.user_id', $user_id)
            ->groupBy('wish_categories.id', 'colors.id')
            ->orderBy('wish_categories.id', 'asc')
            ->get();
    }

}
