<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'hex'
    ];

    public function wishCategory()
    {
        return $this->hasOne('App\Models\WishCategory','color_id');
    }

    public function datatables()
    {
        return $this->select(['id','name','hex','created_at']);
    }
}
