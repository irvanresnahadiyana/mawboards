<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductCategory extends Model
{
    use Sluggable;
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $table = 'product_categories';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'is_parent',
        'slug',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product','product_category_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\ProductCategoryChild','product_category_id');
    }

    public function datatables()
    {
        return $this->select(['product_categories.id','product_categories.name as parent',
                'product_categories.slug as parent_slug','product_categories.is_parent',
                'product_categories.created_at','product_categories.updated_at']);
    }

    public function getDetail($id)
    {
        return $this->find($id);
    }

    public static function boot()
    {
        parent::boot();

        ProductCategory::deleting(function($productCategory) {
            if (count($productCategory->childs) > 0) {
                foreach ($productCategory->childs as $key => $child) {
                    $child->delete();
                }
            }
        });
    }

    public function findBySlug($slug) 
    {
        return $this->where('slug',$slug)->first();
    }
}
