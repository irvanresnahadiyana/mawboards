<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Circle extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['name', 'user_id'],
                'separator' => '-'
            ]
        ];
    }

    protected $table = 'circles';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id',
        'name',
        'type',
    ];

     /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'circle_users');
    }

    public function wishes()
    {
        return $this->belongsToMany('App\Models\Wish', 'wishes_circles');
    }

    public function getAllByUserId()
    {
        return $this
            ->select('id', 'user_id', 'name', 'type', 'slug')
            ->where('user_id', user_info('id'))
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    public function getAllCircle()
    {
        return $this
            ->select('id', 'user_id', 'name', 'type', 'slug');
    }

    public function findCircleBySlug($slug)
    {
        return $this->findBySlug($slug);
    }

    public function storeCircle($data)
    {
        $this->user_id = user_info('id');
        $this->name = $data->name;
        $this->type = 'publish';
        $save = $this->save();

        if ($save && $data->friends) {
            foreach ($data->friends as $friend) {
                // $this->users()->attach($friend);
                $circle_user = new CircleUser;
                $circle_user->user_id = $friend;
                $circle_user->circle_id = $this->id;
                $circle_user->save();
            }
        }
    }

    public function updateCircle($data)
    {
        $circle = $this->find($data->id);
        $circle->user_id = user_info('id');
        $circle->name = $data->name;
        $circle->type = 'publish';
        $save = $circle->save();

        if ($save && $data->friends) {
            $circle->users()->detach();
            foreach ($data->friends as $friend) {
                $circle_user = new CircleUser;
                $circle_user->user_id = $friend;
                $circle_user->circle_id = $circle->id;
                $circle_user->save();
            }
        }
    }

    public function deleteCircle($id)
    {
        $circle = $this->find($id);
        // Remove all friend from circle will be deleted
        $circle->users()->detach();
        // Delete circle
        $circle->delete();
    }

    public function deleteCircleUser($circle_id, $user_id)
    {
        $circle = $this->find($circle_id);
        // Remove friend from circle by user_id
        $circle->users()->detach($user_id);
    }
}
