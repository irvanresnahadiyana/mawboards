<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    protected $fillable = [
        'title',
        'content',
    ];

    public function updateData($param, $id) {
        $data = $this::find($id);
        $data->content = $param['content'];

        if ($data->save()) {
            return $data;
        } else {
            return false;
        }
    }
}
