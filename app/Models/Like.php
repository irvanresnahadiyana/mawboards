<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'status', 'related_id','related_type'
    ];

    Public function related()
    {
        return $this->morphTo();
    }

    Public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function countLikeUser($user_id = null)
    {
        if (!$user_id) {
            $user_id = user_info('id');
        }
        return $this
            ->where('likes.user_id', $user_id)
            ->count();
    }
}
