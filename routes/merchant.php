<?php

/*
|--------------------------------------------------------------------------
| Merchant Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'merchant_auth'], function () {
	Route::get('dashboard', array('as' => 'merchant-dashboard', 'uses' => 'DashboardController@index'));
	Route::get('profile', array('as' => 'profile', 'uses' => 'ProfileController@index'));
	Route::post('update-profile', array('as' => 'update-profile', 'uses' => 'ProfileController@updateProfile'));

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', array('as' => 'merchant-product', 'uses' => 'ProductController@index'));
        Route::get('create', array('as' => 'merchant-product-create', 'uses' => 'ProductController@create'));
        Route::get('{id}/edit', array('as' => 'merchant-product-edit', 'uses' => 'ProductController@edit'));
        Route::post('store', array('as' => 'merchant-product-store', 'uses' => 'ProductController@storeProduct'));
        Route::post('update', array('as' => 'merchant-product-update', 'uses' => 'ProductController@updateProduct'));
        Route::post('delete', array('as' => 'merchant-product-delete', 'uses' => 'ProductController@deleteProduct'));
        Route::post('delete-image', array('as' => 'merchant-product-delete-image', 'uses' => 'ProductController@deleteImageProduct'));
    });

    Route::group(['prefix' => 'product-category'], function(){
        Route::get('get-child', array('as' => 'merchant-product-category-get-child', 'uses' => 'ProductCategoryController@getCategoryChild'));
    });


});
