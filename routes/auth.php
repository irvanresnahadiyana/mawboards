<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'sentinel_guest'], function () {
    // Login
    Route::get('login', array('as' => 'auth-login', 'uses' => 'AuthController@login'));
    Route::post('login-post', array('as' => 'auth-login-post', 'uses' => 'AuthController@loginPost'));

    // Socialite login
    Route::get('socialite/redirect/{provider}', array('as' => 'auth-redirect-socialite', 'uses' => 'AuthController@socialiteRedirect'));
    
    Route::get('socialite/login/{provider}', array('as' => 'auth-login-socialite', 'uses' => 'AuthController@socialiteLogin'));

    // Register
    Route::get('register', array('as' => 'auth-register', 'uses' => 'AuthController@register'));
    Route::post('register-post', array('as' => 'auth-register-post', 'uses' => 'AuthController@registerPost'));
    Route::post('register-post-merchant', array('as' => 'auth-register-post-merchant', 'uses' => 'AuthController@registerMerchantPost'));

    // Activation
    Route::get('activate', array('as' => 'auth-activate', 'uses' => 'AuthController@activate'));

    // Forgot password
    Route::post('forgot-post', array('as' => 'auth-forgot-post', 'uses' => 'AuthController@forgotPost'));

    // Change Password
    Route::get('change-password', array('as' => 'auth-change-password', 'uses' => 'AuthController@changePassword'));
});

Route::post('change-password-post', array('as' => 'auth-change-password-post', 'uses' => 'AuthController@changePasswordPost'));
Route::get('reminder-token-get', array('as' => 'auth-reminder-token-get', 'uses' => 'AuthController@reminderTokenGet'));

Route::group(['middleware' => 'sentinel_auth'], function () {
    // Logout
    Route::get('logout', array('as' => 'auth-logout', 'uses' => 'AuthController@logout'));
});