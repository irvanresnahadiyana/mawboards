<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'admin_auth'], function () {
	Route::get('dashboard', array('as' => 'admin-dashboard', 'uses' => 'DashboardController@index'));
	Route::get('profile', array('as' => 'profile', 'uses' => 'ProfileController@index'));
	Route::post('update-profile', array('as' => 'update-profile', 'uses' => 'ProfileController@updateProfile'));

	Route::get('setting-frontend-static-pages', array('as' => 'setting-frontend-static-pages', 'uses' => 'StaticPageController@index'));
	Route::post('update-setting-frontend-static-pages', array('as' => 'update-setting-frontend-static-pages', 'uses' => 'StaticPageController@updateData'));

	Route::group(['prefix' => 'product'], function(){

		Route::get('/', array('as' => 'admin-product', 'uses' => 'ProductController@index'));
		Route::get('datatable', array('as' => 'admin-product-datatable', 'uses' => 'ProductController@datatables'));
		Route::get('create', array('as' => 'admin-product-create', 'uses' => 'ProductController@create'));
		Route::get('{id}/edit', array('as' => 'admin-product-edit', 'uses' => 'ProductController@edit'));
		Route::post('store', array('as' => 'admin-product-store', 'uses' => 'ProductController@storeProduct'));
		Route::post('update', array('as' => 'admin-product-update', 'uses' => 'ProductController@updateProduct'));
		Route::post('delete', array('as' => 'admin-product-delete', 'uses' => 'ProductController@deleteProduct'));
		Route::post('action-publish-unpublish', array('as' => 'admin-product-publish-unpublish', 'uses' => 'ProductController@actionPublishUnpublish'));
		Route::post('delete-image', array('as' => 'admin-product-delete-image', 'uses' => 'ProductController@deleteImageProduct'));

		Route::group(['prefix' => 'product-category'], function(){
			Route::get('/', array('as' => 'admin-product-category', 'uses' => 'ProductCategoryController@index'));
			Route::get('datatable', array('as' => 'admin-product-category-datatable', 'uses' => 'ProductCategoryController@datatable'));
			Route::get('create', array('as' => 'admin-product-category-create', 'uses' => 'ProductCategoryController@create'));
			Route::get('{id}/edit', array('as' => 'admin-product-category-edit', 'uses' => 'ProductCategoryController@edit'));
			Route::post('store', array('as' => 'admin-product-category-store', 'uses' => 'ProductCategoryController@store'));
			Route::post('update', array('as' => 'admin-product-category-update', 'uses' => 'ProductCategoryController@update'));
			Route::post('delete', array('as' => 'admin-product-category-delete', 'uses' => 'ProductCategoryController@destroy'));
			Route::get('get-child', array('as' => 'admin-product-category-get-child', 'uses' => 'ProductCategoryController@getCategoryChild'));
		});

		Route::group(['prefix' => 'product-category-child'], function(){
			Route::post('delete', array('as' => 'admin-product-category-child-delete', 'uses' => 'ProductCategoryChildController@deleteChildCategory'));
		});
	});

	Route::group(['prefix' => 'wish'], function(){

		Route::get('/', array('as' => 'admin-wish', 'uses' => 'WishController@index'));
		Route::get('datatable', array('as' => 'admin-wish-datatable', 'uses' => 'WishController@datatables'));
		Route::get('create', array('as' => 'admin-wish-create', 'uses' => 'WishController@create'));
		Route::get('{id}/edit', array('as' => 'admin-wish-edit', 'uses' => 'WishController@edit'));
		Route::post('store', array('as' => 'admin-wish-store', 'uses' => 'WishController@store'));
		Route::post('update', array('as' => 'admin-wish-update', 'uses' => 'WishController@update'));
		Route::post('delete', array('as' => 'admin-wish-delete', 'uses' => 'WishController@destroy'));

		Route::group(['prefix' => 'wish-category'], function(){
			Route::get('/', array('as' => 'admin-wish-category', 'uses' => 'WishCategoryController@index'));
			Route::get('datatable', array('as' => 'admin-wish-category-datatable', 'uses' => 'WishCategoryController@datatable'));
			Route::get('create', array('as' => 'admin-wish-category-create', 'uses' => 'WishCategoryController@create'));
			Route::get('{id}/edit', array('as' => 'admin-wish-category-edit', 'uses' => 'WishCategoryController@edit'));
			Route::post('store', array('as' => 'admin-wish-category-store', 'uses' => 'WishCategoryController@store'));
			Route::post('update', array('as' => 'admin-wish-category-update', 'uses' => 'WishCategoryController@update'));
			Route::post('delete', array('as' => 'admin-wish-category-delete', 'uses' => 'WishCategoryController@destroy'));
		});

		Route::group(['prefix' => 'wish-color'], function(){
			Route::get('/', array('as' => 'admin-wish-color', 'uses' => 'WishColorController@index'));
			Route::get('datatable', array('as' => 'admin-wish-color-datatable', 'uses' => 'WishColorController@datatable'));
			Route::get('create', array('as' => 'admin-wish-color-create', 'uses' => 'WishColorController@create'));
			Route::get('{id}/edit', array('as' => 'admin-wish-color-edit', 'uses' => 'WishColorController@edit'));
			Route::post('store', array('as' => 'admin-wish-color-store', 'uses' => 'WishColorController@store'));
			Route::post('update', array('as' => 'admin-wish-color-update', 'uses' => 'WishColorController@update'));
			Route::post('delete', array('as' => 'admin-wish-color-delete', 'uses' => 'WishColorController@destroy'));
		});
	});

	Route::group(['prefix' => 'role'], function(){
		Route::get('/', array('as' => 'admin-role', 'uses' => 'RoleController@index'));
		Route::get('datatable', array('as' => 'admin-role-datatable', 'uses' => 'RoleController@datatable'));
		Route::get('create', array('as' => 'admin-role-create', 'uses' => 'RoleController@create'));
		Route::get('{id}/edit', array('as' => 'admin-role-edit', 'uses' => 'RoleController@edit'));
		Route::post('store', array('as' => 'admin-role-store', 'uses' => 'RoleController@store'));
		Route::post('update', array('as' => 'admin-role-update', 'uses' => 'RoleController@update'));
		Route::post('delete', array('as' => 'admin-role-delete', 'uses' => 'RoleController@destroy'));
	});

	Route::group(['prefix' => 'user'], function(){
		Route::get('/', array('as' => 'admin-user', 'uses' => 'UserController@index'));
		Route::get('datatable', array('as' => 'admin-user-datatable', 'uses' => 'UserController@datatable'));
		Route::get('create', array('as' => 'admin-user-create', 'uses' => 'UserController@create'));
		Route::get('{id}/edit', array('as' => 'admin-user-edit', 'uses' => 'UserController@edit'));
		Route::post('store', array('as' => 'admin-user-store', 'uses' => 'UserController@store'));
		Route::post('update', array('as' => 'admin-user-update', 'uses' => 'UserController@update'));
		Route::post('delete', array('as' => 'admin-user-delete', 'uses' => 'UserController@destroy'));

		// Register
    Route::post('register-post', array('as' => 'admin-user-register', 'uses' => 'UserController@registerPost'));
	});

});
