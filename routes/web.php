<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));
Route::get('get-wishes', array('as' => 'get-wishes', 'uses' => 'HomeController@getAllWishes'));
Route::get('about', array('as' => 'about', 'uses' => 'HomeController@aboutUs'));
Route::get('terms', array('as' => 'terms', 'uses' => 'HomeController@terms'));
Route::get('faq', array('as' => 'faq', 'uses' => 'HomeController@faq'));
Route::get('shop', array('as' => 'shop', 'uses' => 'HomeController@shop'));
Route::post('search', array('as' => 'search', 'uses' => 'HomeController@search'));

Route::group(['prefix' => 'maw'], function() {
    Route::get('{slug?}', array('as' => 'detail-wish-frontend', 'uses' => 'WishController@detail'));
    Route::get('category/{id}/{slug}', array('as' => 'wish-category-frontend', 'uses' => 'WishController@wishCategory'));
});

Route::group(['middleware' => 'sentinel_auth'], function () {
    Route::group(['prefix' => 'profile'], function() {
        Route::get('index/{id?}', array('as' => 'index-profile-frontend', 'uses' => 'ProfileController@index'));
        Route::get('edit', array('as' => 'edit-profile-frontend', 'uses' => 'ProfileController@edit'));
        Route::post('update', array('as' => 'update-profile-frontend', 'uses' => 'ProfileController@update'));
        Route::get('show/{id}', array('as' => 'show-profile-frontend', 'uses' => 'ProfileController@show'));
        Route::get('message', array('as' => 'profile-message', 'uses' => 'ProfileController@message'));
    });

    Route::group(['prefix' => 'friend'], function() {
        Route::get('index', array('as' => 'index-friend-frontend', 'uses' => 'FriendController@index'));
        Route::post('store', array('as' => 'store-friend-frontend', 'uses' => 'FriendController@store'));
        Route::post('update', array('as' => 'update-friend-frontend', 'uses' => 'FriendController@update'));
        Route::post('destroy', array('as' => 'destroy-friend-frontend', 'uses' => 'FriendController@destroy'));
    });

    Route::group(['prefix' => 'circle'], function() {
        Route::get('index', array('as' => 'index-circle-frontend', 'uses' => 'CircleController@index'));
        Route::get('create', array('as' => 'create-circle-frontend', 'uses' => 'CircleController@create'));
        Route::post('store', array('as' => 'store-circle-frontend', 'uses' => 'CircleController@store'));
        Route::get('show/{slug}', array('as' => 'show-circle-frontend', 'uses' => 'CircleController@show'));
        Route::get('edit/{slug}', array('as' => 'edit-circle-frontend', 'uses' => 'CircleController@edit'));
        Route::post('update', array('as' => 'update-circle-frontend', 'uses' => 'CircleController@update'));
        Route::post('destroy', array('as' => 'destroy-circle-frontend', 'uses' => 'CircleController@destroy'));
        Route::post('destroy-circle-friend', array('as' => 'destroy-circle-friend-frontend', 'uses' => 'CircleController@destroyFriend'));
    });

    Route::group(['prefix' => 'wish'], function() {
        Route::get('index', array('as' => 'index-wish-frontend', 'uses' => 'WishController@index'));
        Route::get('create', array('as' => 'create-wish-frontend', 'uses' => 'WishController@create'));
        Route::get('create/{slug}', array('as' => 'create-wish-wishboard-frontend', 'uses' => 'WishController@create'));
        Route::post('store', array('as' => 'store-wish-frontend', 'uses' => 'WishController@store'));
        Route::post('bookmark', array('as' => 'bookmark-wish-frontend', 'uses' => 'WishController@wishBookmark'));
        Route::post('like', array('as' => 'like-wish-frontend', 'uses' => 'WishController@wishLike'));
    });

    Route::group(['prefix' => 'wishboard'], function() {
        Route::get('index', array('as' => 'index-wishboard-frontend', 'uses' => 'WishboardController@index'));
        Route::post('store', array('as' => 'store-wishboard-frontend', 'uses' => 'WishboardController@store'));
        Route::get('show/{slug}', array('as' => 'show-wishboard-frontend', 'uses' => 'WishboardController@show'));
        Route::get('{id}', array('as' => 'user-wishboard-frontend', 'uses' => 'WishboardController@index'));
    });

    Route::post('store-comment', array('as' => 'store-comment-frontend', 'uses' => 'CommentController@store'));
});
